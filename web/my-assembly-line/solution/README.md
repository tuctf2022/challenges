# Solution
The assembly line has a very obvious exported function named `get_secret_flag`. 
By reading this function you can extract the bytes `\x55\x56\x44\x55\x47\x7b\x32\x60\x67\x31\x73\x37\x31\x38\x60\x38\x31\x60\x71\x73\x32\x6f\x38\x60\x6e\x7a\x60\x67\x6d\x35\x37\x60\x33\x39\x36\x33\x31\x7d`
By converting to ASCII you get the text `UVDUG{2`g1s718`81`qs2o8`nz`gm57`39631}`
With simple deduction, you can shift all alphanumeric characters down by 1 to get the flag.