# Flag generator

flag = "TUCTF{1_f0r607_70_pr1n7_my_fl46_28520}"

# Caesar cipher by 1
caesar_cipher = ""
for c in flag:
    if c == "{":
        caesar_cipher += "{"
    elif c == "}":
        caesar_cipher += "}"
    else:
        caesar_cipher += chr(ord(c) + 1)

# Print as C array
print("char caesar_cipher[] = \"", end="")
for c in caesar_cipher:
    print("\\x" + hex(ord(c))[2:], end="")
print("\";")