#include <stdio.h>
#include <emscripten/emscripten.h>
#include <string.h>
#include <fstream>
#include <iostream>

EMSCRIPTEN_KEEPALIVE void display_flag( )
{
    /* Open ../flag.txt */
    std::ifstream flag_file( "./flag.txt" );
    std::string flag;
    std::getline( flag_file, flag );

    /* Display flag with js alert */
    EM_ASM( {
        alert( UTF8ToString( $0 ) );
    }, flag.c_str( ) );
}

EMSCRIPTEN_KEEPALIVE std::uint8_t* get_secret_flag( )
{
    /* Malloc new str */
    std::uint8_t* str = ( std::uint8_t* )malloc( 0x100 );
    char caesar_cipher[] = "\x55\x56\x44\x55\x47\x7b\x32\x60\x67\x31\x73\x37\x31\x38\x60\x38\x31\x60\x71\x73\x32\x6f\x38\x60\x6e\x7a\x60\x67\x6d\x35\x37\x60\x33\x39\x36\x33\x31\x7d";
    memcpy( str, caesar_cipher, sizeof( caesar_cipher ) );
    return str;
}

int main()
{
    int p= 48;

    printf("Hello, World!\n");
    printf("Pointer: 0x%p\n", &display_flag);
    return 0;
}

extern "C" EMSCRIPTEN_KEEPALIVE const char* check_password(char* passwd) 
{
    /* Get first argument, input password */
    if(strcmp(passwd, ("password")) == 0) {
        display_flag();
        return ("Password is correct!");
    } else {
        return ("Password is incorrect!");
    }
}
