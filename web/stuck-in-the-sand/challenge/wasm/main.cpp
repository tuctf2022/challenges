#include <stdio.h>
#include <emscripten/emscripten.h>
#include <string.h>
#include <fstream>
#include <iostream>

EMSCRIPTEN_KEEPALIVE std::string get_flag_dir( )
{
    std::string file_dir = "./files";
    std::string hidden_dir = "/hidden";
    std::string top_secret_dir = "/top_secret";
    std::string too_far_dir = "/too_far";
    return file_dir + hidden_dir + top_secret_dir + too_far_dir;
}

EMSCRIPTEN_KEEPALIVE std::string get_flag_name( )
{
    char xor_cipher[] = "\x6c\x7a\x7c\x6d\x7a\x6b\x40\x7b\x7e\x6b\x7e\x31\x7b\x7e\x6b";
    /* Xor string by 0x1F */
    for( std::size_t i = 0; i < sizeof( xor_cipher ); i++ )
    {
        xor_cipher[ i ] ^= 0x1F;
    }
    return std::string( xor_cipher );
}


EMSCRIPTEN_KEEPALIVE void read_flag_file( )
{
    /* Open ../flag.txt */
    std::ifstream flag_file( get_flag_dir( ) + "/" + get_flag_name( ) );
    std::string flag;
    std::getline( flag_file, flag );

    /* Display flag with js alert */
    EM_ASM( {
        alert( UTF8ToString( $0 ) );
    }, flag.c_str( ) );
}

EMSCRIPTEN_KEEPALIVE std::uint8_t* get_xor_keys( )
{
    std::uint8_t* ret = (std::uint8_t*)malloc(10);
    ret[0] = 0x39;
    ret[1] = 0x10;
    ret[2] = 0x30;
    ret[3] = 0x76;
    ret[4] = 0xF2;
    ret[5] = 0x23;
    ret[6] = 0xAA;
    return ret;
}

int main()
{
    int p= 48;
    printf("Enter the sandbox\n");
    return 0;
}
