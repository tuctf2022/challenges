# Flag generator
import os

file_name = "secret_data_new.dat"
# Xor by 0x13
xor_cipher = ""
for c in file_name:
    xor_cipher += chr(ord(c) ^ 0x1f)

#print("char xor_cipher[] = \"", end="")
#for c in xor_cipher:
#    print("\\x" + hex(ord(c))[2:], end="")

flag = "TUCTF{y0u_607_0u7_0f_my_54ndb0x_247591}"

# Open and print 'secret_data.dat' as bytes
with open("secret_data_new.dat", "rb") as f:
    print("char secret_data[] = \"", end="")
    for c in f.read():
        print("\\x" + hex(c)[2:], end="")
    print("\";")

# \x6d\x45\x73\x22\xb4\x58\xd3\x9\x65\x6f\x40\xc2\x14\xf5\x9\x65\x7\x29\xc2\x45\xf5\x54\x69\x6f\x43\xc6\x4d\xce\x5b\x20\x48\x29\xc0\x17\x9d\xc\x29\x1\xb
flag2_enc = [ 0x6d, 0x45, 0x73, 0x22, 0xb4, 0x58, 0xd3, 0x9, 0x65, 0x6f, 0x40, 0xc2, 0x14, 0xf5, 0x9, 0x65, 0x7, 0x29, 0xc2, 0x45, 0xf5, 0x54, 0x69, 0x6f, 0x43, 0xc6, 0x4d, 0xce, 0x5b, 0x20, 0x48, 0x29, 0xc0, 0x17, 0x9d, 0xc, 0x29, 0x1, 0xb ]

# 6D 45 73 22 B4 58 D3 09 65 6F 40 C2 14 F5 09 65 07 29 C2 45 F5 54 69 6F 43 C6 4D CE 5B 20 48 29 C0 17 9D 0C 29 01 0B
flag3_enc = [0x6D, 0x45, 0x73, 0x22, 0xB4, 0x58, 0xD3, 0x09, 0x65, 0x6F, 0x40, 0xC2, 0x14, 0xF5, 0x09, 0x65, 0x07, 0x29, 0xC2, 0x45, 0xF5, 0x54, 0x69, 0x6F, 0x43, 0xC6, 0x4D, 0xCE, 0x5B, 0x20, 0x48, 0x29, 0xC0, 0x17, 0x9D, 0x0C, 0x29, 0x01, 0x0B]

# 6D 45 73 22 58 09 65 6F 40 14 09 65 07 29 45 54 69 6F 43 4D 5B 20 48 29 17 0C 29 01 0B
flag_enc = [ 0x6D, 0x45, 0x73, 0x22, 0x58, 0x09, 0x65, 0x6F, 0x40, 0x14, 0x09, 0x65, 0x07, 0x29, 0x45, 0x54, 0x69, 0x6F, 0x43, 0x4D, 0x5B, 0x20, 0x48, 0x29, 0x17, 0x0C, 0x29, 0x01, 0x0B ]

# Xor key table
xor_keys = [0x39, 0x10, 0x30, 0x76, 0xF2, 0x23, 0xAA]
# Xor encryption
xor_cipher = []
for i in range(len(flag3_enc)):
    xor_cipher.append((flag3_enc[i]) ^ xor_keys[i % len(xor_keys)])

# Print data as hex
for b in xor_cipher:
    print(chr(b), end=" ")

exit()

# Write to secret_data.dat in script directory
with open(os.path.join(os.path.dirname(__file__), file_name), "wb") as f:
    f.write(bytes(xor_cipher))

print("\n\n\n")

#print("char xor_cipher[] = \"", end="")
#for c in xor_cipher:
#    print("\\x" + hex(ord(c))[2:], end="")
#print("\";")


# Open secret_data.dat in current script directory and read as bytes
with open(os.path.join(os.path.dirname(__file__), file_name), "rb") as f:
    data = f.read()
    # Print data as hex
    for b in data:
        print(hex(b), end=" ")
    flag_dec = ""
    # Decrypt data using xor table
    for i in range(len(data)):
        flag_dec += chr((data[i]) ^ xor_keys [i % len(xor_keys)])
    # Print decrypted data
    print(flag_dec)
exit()
# Decrypt xor_cipher
flag_dec = ""
for i in range(len(xor_cipher)):
    flag_dec += chr(ord(xor_cipher[i]) ^ xor_keys [i % len(xor_keys)])
print(flag_dec)
exit()

# Write to secret_data.dat in script directory
current_dir = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(current_dir, "secret_data.dat"), "wb") as f:
    # Get bytes of xor_cipher, ignore error if not ascii
    f.write(xor_cipher.encode("ascii", "ignore"))