Challenge deals with modifying HTTP Header information

Step 1) Change the time using the Date header so that it fits when the site is operable 
        If the time is between Midnight and noon, it will need to be changed to be between noon and midnight and other way around
       
Step 2) Change the IP address to a Tulsa IP address (X-Forwarded-For header) 

Step 3) Can find that IE8 released March 2009 (Wanted to use a browser that no one would likely be randomly using) and change User-Agent header to the IE8 user agent

Step 4) Add any email that ends in @TUCTF.com using the From header (ex. From: Noah@TUCTF.com)

Step 5) Flag on screen 
