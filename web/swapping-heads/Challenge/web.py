import tornado.template
import tornado.ioloop
import tornado.web
import datetime

TEMPLATE = '''
<html>
    <head>
        <title>
            {{temp}}
        </title>
    </head>
    <body>
	replace
    </body>
</html>
'''

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        text = '<center><strong> Can you trick my site into thinking you are from Tulsa, Oklahoma? </strong></center>'
        text2 = '<center><strong> Sorry! This site is only available from noon to midnight! </strong></center>'
        text3 = '<center><strong> No can do! Where is your email for this site?  </strong></center>'
        text4 = '<center><strong> Wait a minute! That browser is not from March 2009! </strong></center>'
        completed = 0 #0 if nothing done, 1 if date modified, 2 if date and ip modified, 3 if date ip and browser agent  modified, 4 if date ip email and browser agent modified - give flag at 4  
        timezone = 0 #should be 0 if currently midnight to noon, 1 if currently noon to midnight
        curtime = str(datetime.datetime.now())
        curtime = curtime[curtime.index(' '):curtime.index(':')]
        if(int(curtime)>=12):
             text2 = text2.replace('noon to midnight','midnight to noon')
             timezone = 1
        try:
             ip = self.request.headers['X-Forwarded-For']
        except:
             ip = 'no input'
        try:
             date = self.request.headers['Date']
        except:
             date = ''
        try:
             browser = self.request.headers['User-Agent']
        except:
             browser = ''
        try:
             email = self.request.headers['From']
        except:
             email = ''
        try:
          if(date != ''):
                  bdate = int(date[date.index(':')-2:date.index(':')])
                  if(bdate<=12):
                       bdate = 1
                  else:
                       bdate = 0
          else:
               bdate = 2
        except:
             bdate = 2 #incorrect date format
			
        ips = '''65.82.32 65.16.100 66.148.231 12.106.198 12.106.199 67.77.195 68.15.192 68.15.193 68.15.194 24.119.227 65.210.56 68.14.160 68.14.161 12.212.88 
               12.18.214 23.113.56 23.113.57 63.101.19 68.0.80 68.0.81 68.0.82 64.192.32 68.91.9 68.91.10 45.28.145 45.28.146 45.28.147 64.216.187 64.200.205 
               23.232.154 23.232.155 68.13.243 68.13.244 68.13.245 68.13.246 68.13.247 68.13.248 68.13.249 68.13.250 68.13.251 68.13.252 68.13.253 12.5.227 
               12.5.228 12.5.229 12.5.230 12.5.231 64.219.33 64.200.136 64.200.137 12.170.58 12.170.59 24.254.74 24.254.75 24.254.76 24.254.77 68.143.177 
               12.149.176 12.149.177 12.149.178 12.149.179 12.133.108 12.133.109 12.133.110 12.133.111 12.31.192 12.31.193 12.31.194 12.31.195 12.31.196 
               23.122.53 23.122.54 23.122.55 66.139.36 66.139.37 12.106.196 68.13.240 68.13.241 45.30.57 45.30.58 45.30.59 45.30.60 45.30.61 63.114.183
               12.159.117 23.120.186 23.120.187 12.209.227 66.20.211 68.13.229 68.13.230 68.13.231 68.13.232 68.13.233 68.13.234 68.13.235 68.0.66 12.57.149 
               68.16.213 68.0.84 68.0.85 68.0.86 68.152.66 12.189.48 12.189.49 12.189.50 12.5.234 12.5.235 12.5.236 12.5.237 12.5.238 64.149.54 66.49.19 
               12.18.212 66.141.196 68.0.71 68.0.72 66.194.172 63.241.191 65.115.193 65.67.203 65.67.204 64.218.184 64.218.185 12.206.140 12.206.141 12.206.142 12.206.143
               12.39.117 64.11.252 68.109.249 63.156.72 67.28.91 65.70.64 66.21.102 67.65.97 66.137.58 66.137.59 63.80.22 12.170.56 66.21.245 66.21.246 12.130.205 
               64.216.177 64.216.178 64.216.179 64.216.180 68.153.76 68.91.139 68.91.140 65.4.30 66.55.249 66.55.250 63.25.155 12.206.136 12.206.137 12.206.138 
               65.12.29 66.139.43 66.139.44 66.139.45 66.139.40 65.10.89 65.64.37 12.212.190 64.217.111 64.200.32 64.200.33 64.200.34 64.200.35 64.200.36 66.148.208 
               66.148.209 66.191.36 63.91.179 63.91.180 65.70.85 32.170.36 12.39.115 67.217.202 64.149.44 65.64.40 40.128.79 63.165.44 66.139.34 68.15.204 68.15.205 68.15.206 ''' 
        #that took so long to type and I bet there was a much better solution
        if(ip!='no input'):
               try:
                    ipmod = ip
                    ipmod = ipmod.replace('.','',2)
                    ipmod = ipmod.index('.') + 2
                    ip = ip[0:ipmod]
               except:
                    ip = 'incorrect format'
		
        browserc = 'Mozilla/4.0' in browser and 'MSIE 8.0' in browser and 'Trident/4.0' in browser

        if(bdate==timezone and 'tuctf.com' in email.lower() and browserc):
             completed = 4
        elif(bdate==timezone  and browserc): 
             completed = 3
        #elif(ip in ips and bdate==timezone):
        #    completed = 2
        elif(bdate==timezone):
             completed = 2

        if(completed==0): #need modify date
             template = TEMPLATE.replace('replace',text2)
             t = tornado.template.Template(template)
             self.write(t.generate(temp=text2))
        elif(completed==1): #need modify ip (X-Forwarded-For header) Removed this check
             template = TEMPLATE.replace('replace',text)
             t = tornado.template.Template(template)
             self.write(t.generate(temp=text))
        elif(completed ==2): #need modify browser agent (User-Agent header)
             template = TEMPLATE.replace('replace', text4)
             t = tornado.template.Template(template)
             self.write(t.generate(temp=text4))
        elif(completed==3): #need modify email to have TUCTF.com domain (From header) 
             template = TEMPLATE.replace('replace',text3)
             t = tornado.template.Template(template)
             self.write(t.generate(temp=text3))
        elif(completed==4):
             flag = 'TUCTF{s0_m@ny_h11p_h3@d3r5}'
             template = TEMPLATE.replace('replace',flag)
             t = tornado.template.Template(template)
             self.write(t.generate(temp=flag))
#        print('\n' + ip)
#        print('\n' + browser)
#        print('\n' + email)

application = tornado.web.Application([
   (r"/", MainHandler),
], debug = True, static_path=None, template_path=None)

if __name__ == '__main__':
    application.listen(8100)
    print('Server started on port 8100')
    tornado.ioloop.IOLoop.instance().start()
