#include <stdio.h>
#include <emscripten/emscripten.h>
#include <string.h>
#include <fstream>
#include <iostream>


std::string error_string = std::string("[ ") + __FILE__ + std::string( " ] exception caught!" ); 

__attribute__((noinline))
EMSCRIPTEN_KEEPALIVE char* get_flag( )
{
    /* Wow, epic compiler bug! */
    char flag[64] = "TUCTF{my_c0mp1l3r_0p71m1z35_m3_0u7!}";
   
   return flag;
}

int main()
{
    printf("Flag: %s\n", get_flag());
    return 0;
}
