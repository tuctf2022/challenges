# Solution
The soltion to `broken-source` leverages the possible misconfiguration of the developer who forgot to remove his C++ source code from the web server.
The `get_flag` function prints nothing to console, showing the the function is indeed broken.

By looking at the strings in the .wasm file, you can notice a `.\new_project_for_ctf_v28095.cpp` string.
By navigating to `/wasm/new_project_for_ctf_v28095.cpp` you can see the source code of the C++ program.
By looking at the source code you can see why `get_flag` returns nothing and you can get the flag.

```cpp
__attribute__((noinline))
EMSCRIPTEN_KEEPALIVE char* get_flag( )
{
    /* Wow, epic compiler bug! */
    char flag[64] = "TUCTF{my_c0mp1l3r_0p71m1z35_m3_0u7!}";
   
   return flag;
}

```
