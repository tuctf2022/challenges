import tornado.template
import tornado.ioloop
import tornado.web
import os

TEMPLATE = '''
<html>
 <head> 
 <title>
         {{ name }} 
 </title>
 </head>
 <body>
         Hello input! My friend Joe entrusted me with his website template - hope I dont mess anything up, Tornado sure is confusing
  </body>
</html>
'''
class MainHandler(tornado.web.RequestHandler):

        def get(self):
                self.set_cookie('are_you_joe','no')
                name = self.get_argument('name', '')
                name = check(name)
                template_data = TEMPLATE.replace("input",name)
                t = tornado.template.Template(template_data)
                self.write(t.generate(name=name))
                self.write('<br/><br/>By the way, whats your name? I dont think you told me')
                self.write('<form action="/namecheck" method="POST">')
                self.write('</br></br><label for="name" > Input Name Here: </label>')
                self.write('<input type="text" id = "name"  name = "name" />')
                self.write('<input type="submit" id="loginbtn" name="login" value="Say hi to me!" class="button"></form>')

class NameHandler(tornado.web.RequestHandler):
        def post(self):
                name = self.get_argument('name','')
                name = check(name) #To prevent harmful remote code execution while still allowing the premesis of the challenge to remain the same, a input check method was implemented to estimate the output of the command without ever executing anything (Needs more testing but rm did not work, limited commands available)
                template_data = TEMPLATE.replace("input",name)
                t = tornado.template.Template(template_data)
                self.write(t.generate(name=name))
                self.write('<p style="color:white"> Joe has 12957293 cookies - so forgetful, making me put it on the webpage</p>')

class Page2Handler(tornado.web.RequestHandler):
        def get(self):
                if (self.get_cookie("are_you_joe") != '12957293'):
                        riddle = 'Bob has 1027524 cookies. Alice has 2 cookies. How many cookies does Joe have?'
                        template_data = TEMPLATE.replace('Hello input! My friend Joe entrusted me with his website template - hope I dont mess anything up, Tornado sure is confusing',riddle)
                        t = tornado.template.Template(template_data)
                        self.write(t.generate(name=riddle))
                else:
                        welcome = 'Welcome! Here is your super secure data: TUCTF{t0rnad0_1snt_v3ry_s3cur3}'
                        template_data = TEMPLATE.replace('Hello input! My friend Joe entrusted me with his website template - hope I dont mess anything up, Tornado sure is confusing',welcome)
                        t = tornado.template.Template(template_data)
                        self.write(t.generate(name=welcome))

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/parttwooftheproblem", Page2Handler),
    (r"/namecheck", NameHandler),
], debug=True, static_path=None, template_path=None)

def check(inp):
        inp = inp.lower()
        inp = inp.replace(' ','')
        if('{' in inp):
                if('import' in inp and 'os' in inp):
                        synt =[0,0,0,0,0,0]
                        for i in inp:
                                if(i=='{'):
                                        synt[0]+=1
                                elif(i=='}'):
                                        synt[1]+=1
                                elif(i=='('):
                                        synt[2]+=1
                                elif(i==')'):
                                        synt[3]+=1
                                elif(i=='"'):
                                        synt[4]+=1
                                elif(i=="'"):
                                        synt[5]+=1
                        if(not(synt[0]==synt[1]) or not(synt[2]==synt[3]) or not(synt[4]%2 == 0) or not(synt[5]%2 == 0)):
                                inp = 'Syntax Error!'
                                return inp
                        start = inp.index('os.')
                        end = inp.index('(')
                        method = inp[start+3:end]
                        if('{{os.' in inp and method == 'popen'):
                                if('"ls' in inp or "'ls" in inp):
                                        inp = 'lookatme.txt'
                                elif('"cat' in inp or "'cat" in inp):
                                        if('lookatme.txt' in inp):
                                                inp = 'Z28gdG8= L3BhcnR0d29vZnRoZXByb2JsZW0='
                                        else:
                                                inp = 'Invalid File. Check usage of command or file name.'
                                elif('"help"' in inp or "'help'" in inp):
                                        inp = "Available commands: ls (list), cat (read file), help"
                                else:
                                        inp = "Invalid command. Use help to view available commands."
                        elif('{{os.' in inp and method == 'getcwd'):
                                inp = '/home/tornado'
                        elif('{{os.' in inp and method == 'listdir'):
                                inp = 'lookatme.txt'
                        elif('{{os.' in inp and method == 'read'):
                                file = inp.index('read')+5
                                if(inp[file+1:file+13] == 'lookatme.txt'):
                                        inp = 'Z28gdG8= L3BhcnR0d29vZnRoZXByb2JsZW0='
                                else:
                                        inp = 'Invalid file'
                        else:
                                inp = 'module os has no attribute ' + method
                elif('import' in inp):
                        inp = 'Could not get requested module'
        return inp


if __name__ == '__main__':
        application.listen(8000)
        print("Server started on port 8000")
        tornado.ioloop.IOLoop.instance().start()
