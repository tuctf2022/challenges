/* Import express */
const express = require( 'express' );
/* Import body-parser */
const bodyParser = require( 'body-parser' );
/* Include fs */
const fs = require( 'fs' );

const app = express( );

const challenge = require( './challenge.json' );
const port = challenge.port;

app.use( bodyParser.json( ) );

app.get( '/' , ( req , res ) => {
    let html = "";
    html += "<center><h1>Welcome To Vertical Traversal!</h1></center>" ;
    html += "<center><h3>Try to find the flag!</h3></center>" ;
    html += "<center><a href='/get/bee.txt'>bee.txt</a> | <a href='/get/emoji.txt'>emoji.txt</a></center>" ;
    /* Send html */
    res.send( html );
} );

app.get( '/get/:filename' , ( req , res ) => {
    const filename = req.params.filename.replace( /\.\./g , '.' );
    /* try */
    try {
        fs.readFile( "./files/" + filename , ( err , data ) => {
            if ( err ) {
                res.status( 404 ).send( 'File not found: ' + filename );
            } else {
                /* Prepend html tags */
                let html = "<center><h1>" + filename + "</h1></center>" ;
                html += "<center><pre>" + data + "</pre></center>" ;
                /* Send html */
                res.send( html );
            }
        } );
    } catch ( e ) {
        res.status( 404 ).send( 'File not found: ' + filename );
    }
} );

app.listen( port , ( ) => {
    console.log( `Server started on port ${ port }` );
} );

