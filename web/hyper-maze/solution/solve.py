import os

cwd = os.path.dirname( os.path.realpath( __file__ ) )
page_dir = cwd + "/../challenge/pages/"

def find_next_page( html ):
    # Get all hrefs in the html
    hrefs = []
    for i in range( len( html ) ):
        if "href=\"" in html[ i ]:
            hrefs.append( html[ i ].split( "href=\"" )[ -1 ].split( "\"" )[ 0 ] )
    for href in hrefs:
        if href.endswith( ".html" ):
            return href
    return None

def load_html_file( name ):
    f = open( page_dir + name, "r" )
    html = f.readlines( )
    f.close( )
    return html

if __name__ == "__main__":
    # Load the first page
    html = load_html_file( "page_aesthetician100.html" )
    # Iterate through all pages
    while True:
        # Find the next page
        next_page = find_next_page( html )
        # Debug print
        print( next_page )
        if next_page is None:
            # No next page found
            break
        # Load the next page
        html = load_html_file( next_page )
    # Print the last page
    print( "".join( html ) )