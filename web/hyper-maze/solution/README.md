# Hyper Maze Solution

The maze consists of 100 randomly generated html pages which scorch the eyes upon viewing. 
Each page contains a link to the next page in the maze. The maze is solved by following the links to the end.
This can be done by hand, but it is much easier to use a script to do it for you.

A simple script to follow all '.html' hrefs will work great.

## Solution
```python
if __name__ == "__main__":
    # Load the first page
    html = load_html_file( "page_aesthetician100.html" )
    # Iterate through all pages
    while True:
        # Find the next page
        next_page = find_next_page( html )
        # Debug print
        print( next_page )
        if next_page is None:
            # No next page found
            break
        # Load the next page
        html = load_html_file( next_page )
    # Print the last page
    print( "".join( html ) )
```

## Solution Output
```html
3xtr4_s3cr3t_fl4g_429850252068.html
None
<html>
<head>
    <title>Hyper Maze Secret Flag</title>
    <link rel="stylesheet" href="style.css">
</head>
<center>
    <h1>
        Congrats on reaching the end of my maze!
    </h1>
    <h3>
        Here's your flag: <b>TUCTF{y0u_50lv3d_my_hyp3r73x7_m4z3_38157}</b>
    </h3>
</center>
</html>
```