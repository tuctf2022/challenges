import json
import random
import os

# Get current script directory
cwd = os.path.dirname( os.path.realpath( __file__ ) )

def load_words():
    with open( cwd + '/words_alpha.txt' ) as word_file:
        valid_words = ( word_file.read( ).split( ) )
    return valid_words

all_words = load_words( )
num_words = len( all_words )
def get_random_word( ):
    # Return a random word from `all_words` using index
    return all_words[ random.randint( 0, num_words - 1 ) ]

html_tags = [
    "a", "b", "p", "div", "span", "h1", "h2", "h3", "h4", "h5", "h6", "i", "em", "strong", "pre", "code", "blockquote", "ul", "ol", "li", "dl", "dt", "dd", "table", "tr", "td", "th", "thead", "tbody", "tfoot", "caption", "col", "colgroup", "form", "input", "textarea", "button", "select", "option", "optgroup", "label", "fieldset", "legend", "iframe", "img", "map", "area", "audio", "video", "canvas", "script", "noscript", "del", "ins", "cite", "abbr", "acronym", "address", "bdo", "big", "br", "button", "cite", "code", "dfn", "em", "i", "img", "input", "kbd", "label", "q", "samp", "select", "small", "span", "strong", "sub", "sup", "textarea", "tt", "var"
]

html_attritubes = [
    "accept", "accept-charset", "accesskey", "action", "align", "alt", "async", "autocomplete", "autofocus", "autoplay", "bgcolor", "border", "buffered", "challenge", "charset", "checked", "cite", "class", "code", "codebase", "color", "cols", "colspan", "content", "contenteditable", "contextmenu", "controls", "coords", "crossorigin", "data", "datetime", "default", "defer", "dir", "dirname", "disabled", "download", "draggable", "dropzone", "enctype", "for", "form", "formaction", "headers", "height", "hidden", "high", "href", "hreflang", "http-equiv", "icon", "id", "ismap", "itemprop", "keytype", "kind", "label", "lang", "language", "list", "loop", "low", "max", "maxlength", "media", "method", "min", "multiple", "muted", "name", "novalidate", "open", "optimum", "pattern", "ping", "placeholder", "poster", "preload", "radiogroup", "readonly", "rel", "required", "reversed", "rows", "rowspan", "sandbox", "scope", "scoped", "seamless", "selected", "shape", "size", "sizes", "span", "spellcheck", "src", "srcdoc", "srclang", "srcset", "start", "step", "style", "summary", "tabindex", "target", "title", "type", "usemap", "value", "width", "wrap"
]

css_tags = [
    "background", "background-attachment", "background-color", "background-image", "background-position", "background-repeat", "border", "border-bottom", "border-bottom-color", "border-bottom-style", "border-bottom-width", "border-collapse", "border-color", "border-left", "border-left-color", "border-left-style", "border-left-width", "border-right", "border-right-color", "border-right-style", "border-right-width", "border-spacing", "border-style", "border-top", "border-top-color", "border-top-style", "border-top-width", "border-width", "bottom", "caption-side", "clear", "clip", "color", "content", "counter-increment", "counter-reset", "cue", "cue-after", "cue-before", "cursor", "direction", "display", "elevation", "empty-cells", "float", "font", "font-family", "font-size", "font-style", "font-variant", "font-weight", "height", "left", "letter-spacing", "line-height", "list-style", "list-style-image", "list-style-position", "list-style-type", "margin", "margin-bottom", "margin-left", "margin-right", "margin-top", "max-height", "max-width", "min-height", "min-width", "orphans", "outline", "outline-color", "outline-style", "outline-width", "overflow", "padding", "padding-bottom", "padding-left", "padding-right", "padding-top", "page", "page-break-after", "page-break-before", "page-break-inside", "pause", "pause-after", "pause-before", "pitch", "pitch-range", "play-during", "position", "quotes", "richness", "right", "speak", "speak-header", "speak-numeral", "speak-punctuation", "speech-rate", "stress", "table-layout", "text-align", "text-decoration", "text-indent", "text-shadow", "text-transform", "top", "unicode-bidi", "vertical-align", "visibility", "voice-family", "volume", "white-space", "widows", "width", "word-spacing", "z-index"
]

html_text_tags = [
    "a", "area", "base", "link"
]

def get_random_html_tag( ):
    # Return a random HTML tag from `html_tags`
    return html_tags[ random.randint( 0, len( html_tags ) - 1 ) ]

def get_random_html_text_tag( ):
    # Return a random HTML text tag from `html_text_tags`
    return html_text_tags[ random.randint( 0, len( html_text_tags ) - 1 ) ]

def get_random_html_attribute( ):
    # Return a random HTML attribute from `html_attritubes`
    return html_attritubes[ random.randint( 0, len( html_attritubes ) - 1 ) ]

def get_random_css_tag( ):
    # Return a random CSS tag from `css_tags`
    return css_tags[ random.randint( 0, len( css_tags ) - 1 ) ]

def generate_hidden_link( destination ):
    # Generate a hidden link to `destination`
    # Generate a random HTML tag
    tag = get_random_html_text_tag( )
    # Generate random number of attributes
    num_attrs = random.randint( 0, 10 )
    attrs = ""
    for j in range( num_attrs ):
        attrs += " " + get_random_html_attribute( ) + "=\"" + get_random_word( ) + "\""
    # Generate the hidden link
    return "\t\t<" + tag + attrs + " href=\"" + destination + "\">" + get_random_word( ) + "</" + tag + ">\n"

def generate_normal_link( destination ):
    # Generate a normal link to `destination`
    return "\t\t<a href=\"" + destination + "\">" + get_random_word( ) + "</a>" + "\n"

def get_random_hex_str( ):
    # Return a random hex string, example: "#FFAABBCC"
    return "\t\t#" + "".join( [ random.choice( "0123456789ABCDEF" ) for i in range( 6 ) ] )

current_iter = 0
def generate_html_page( target_page ):
    # Create the html header
    html = "<!DOCTYPE html>\n"
    html += "<html>\n"
    html += "\t<head>\n"
    html += "\t\t<title>" + get_random_word( ) + str(current_iter) + "</title>\n"
    html += "\t<style>\n"
    # Hide all 'a' tags
    html += """
           a:link {color: black;}
       a:visited {color: black;}
       a:hover {color: black;}
       a:active {color: black;}
    """
    # Iterate all html tags
    for tag in html_tags:
        # Generate random number of CSS tags
        num_css_tags = random.randint( 0, 10 )
        css = ""
        for j in range( num_css_tags ):
            css += get_random_css_tag( ) + ": " + str( random.randint( 1, 100 ) ) + "; "
            # Generate random background color
            if random.randint( 0, 1 ) == 1:
                css += "background-color: " + get_random_hex_str( ) + "; "
        # Generate the CSS for the tag
        # Add the CSS to the html
        html += "\t\t" + tag + " { " + css + " }\n"
    html += "\t</style>\n"
    html += "\t</head>\n"
    html += "\t<body>\n"
    
    # Generate a random number of random HTML tags
    num_tags = random.randint( 100, 1000 )
    # Generate a random number to place the hidden link
    hidden_link_index = random.randint( 0, num_tags - 1 )
    print( "Hidden link index: " + str( hidden_link_index ) )
    for i in range( num_tags ):
        if i == hidden_link_index:
            # Generate a hidden link to the next page or normal link to the next page
            should_generate_hidden_link = random.randint( 0, 2 )
            if should_generate_hidden_link == 0:
                html += generate_hidden_link( target_page  )
            elif should_generate_hidden_link == 1:
                html += generate_normal_link( target_page )
            else:
                # Generate a random html tag and attributes, add the hidden link to a random attribute
                tag = get_random_html_tag( )
                num_attrs = random.randint( 0, 10 )
                attrs = ""
                for j in range( num_attrs ):
                    attrs += " " + get_random_html_attribute( ) + "=\"" + get_random_word( ) + "\""
                # Generate the hidden link
                html += "<" + tag + attrs + " " + get_random_html_attribute( ) + "=\"" + generate_hidden_link( target_page ) + "\">" + get_random_word( ) + "</" + tag + ">"
            continue
        tag = get_random_html_tag( )
        # Generate random number of attributes
        num_attrs = random.randint( 0, 10 )
        attrs = ""
        for j in range( num_attrs ):
            attrs += " " + get_random_html_attribute( ) + "=\"" + str( random.randint( 0,10 ) ) + "\""
        html += "\t\t<" + tag + attrs + ">" + get_random_word( ) + "</" + tag + ">\n"

    # Close the html body and footer
    html += "\t</body>\n"
    html += "</html>\n"

    # Return the generated HTML
    return html

def generate_html_file( target_page ):
    # Generate a random HTML file
    global current_iter
    current_iter += 1
    filename = "page_" + get_random_word( ) + str( current_iter ) + ".html"
    # Save file into ./pages/
    f = open( cwd + "/pages/" + filename, "w" )
    f.write( generate_html_page( target_page ) )
    f.close( )
    return filename

def generate_html_files( num_files ):
    target = "3xtr4_s3cr3t_fl4g_429850252068.html"
    # Generate `num_files` random HTML files
    for i in range( num_files ):
        target = generate_html_file( target )

def reset_html_files( ):
    # Reset the HTML files
    global current_iter
    current_iter = 0
    # Delete all files in ./pages/
    for filename in os.listdir( cwd + "/pages/" ):
        # Skip "3xtr4_s3cr3t_fl4g_429850252068.html"
        if filename == "3xtr4_s3cr3t_fl4g_429850252068.html":
            continue
        os.remove( cwd + "/pages/" + filename )

if __name__ == "__main__":
    # Reset the HTML files
    reset_html_files( )
    # Generate 100 random HTML files
    generate_html_files( 100 )