# TUCTF 22
## Folder Formats
Category -> Challenge Name -> Challenge Files

Each challenge should consist of a README.md file with the challenge description. Split each challenge folder into two subfolders, one for the challenge, and another for the solution.