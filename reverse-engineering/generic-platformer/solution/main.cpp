#include <Windows.h>
#include <iostream>
#include <cstdint>
#include <polyhook2/Detour/x64Detour.hpp>
#include <polyhook2/CapstoneDisassembler.hpp>

std::uintptr_t load_scene_address   = 0x0099F840;
std::uintptr_t set_gravity_address  = 0x00C55930;
std::uintptr_t unityplayer = (std::uintptr_t)LoadLibraryA("UnityPlayer.dll");

class MonoString
{
private:
    char pad[0x10];
public:
    std::int32_t length;
    wchar_t chars[0];
public:
    std::wstring to_wstring( )
    {
        return std::wstring( chars, length );
    }

    std::string to_string( )
    {
        std::wstring wstr = to_wstring( );
        std::string str( wstr.begin( ), wstr.end( ) );
    }

    void set_wstring( std::wstring wstr )
    {
        length = wstr.length( );
        memcpy( chars, wstr.c_str( ), length * 2 );
    }
};

struct Vector2
{
    float x,y;
};

PVOID original_load_scene = nullptr;

void __fastcall hook_load_scene( MonoString* scene_name, int a2, PVOID a3, char a4 )
{
    /* Change to level 3 */
	scene_name->chars[6] = L'3';
    /* Call original function */
    return ((void( __fastcall* )( MonoString*, int, PVOID, char ))original_load_scene)( scene_name, a2, a3, a4 );
}

void initialize( )
{
    /* Initialize console */
    AllocConsole( );
    freopen_s( (FILE**)stdout, "CONOUT$", "w", stdout );
    /* Initialize detour */
    PLH::CapstoneDisassembler dis( PLH::Mode::x64 );
    PLH::x64Detour detour( (uint64_t)(unityplayer + load_scene_address), (uint64_t)hook_load_scene, (uint64_t*)&original_load_scene, dis );
    if ( detour.hook( ) )
    {
        std::cout << "Successfully hooked load_scene!" << std::endl;
    }
    else
    {
        std::cout << "Failed to hook load_scene!" << std::endl;
    }
    
	float y_gravity = -9.81f;
    /* Set gravity to 0.1f */
    Vector2 gravity = { 0.0f, y_gravity };
    reinterpret_cast<void( __fastcall* )( Vector2* )>(unityplayer + set_gravity_address)( &gravity );

    /* Busy wait */
    while ( true )
    {
		/* If we press up arrow, increase gravity */
		if (GetAsyncKeyState(VK_UP))
		{
			gravity.y += 0.1f;
			reinterpret_cast<void(__fastcall*)(Vector2*)>(unityplayer + set_gravity_address)(&gravity);
		}
		/* If we press down arrow, decrease gravity */
		if (GetAsyncKeyState(VK_DOWN))
		{
			gravity.y -= 0.1f;
            reinterpret_cast<void(__fastcall*)(Vector2*)>(unityplayer + set_gravity_address)(&gravity);
		}
		
        Sleep( 10 );
    }
}

/* DllMain */
BOOL WINAPI DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpReserved )
{
    if ( dwReason == DLL_PROCESS_ATTACH )
    {
        DisableThreadLibraryCalls( hModule );
        CreateThread( nullptr, 0, (LPTHREAD_START_ROUTINE)initialize, nullptr, 0, nullptr );
    }
    return TRUE;
}