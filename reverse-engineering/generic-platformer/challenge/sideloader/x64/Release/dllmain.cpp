#include <Windows.h>

#pragma region Proxy
struct sideloader_dll {
	HMODULE dll;
} sideloader;

extern "C" {
	FARPROC PA = 0;
	int runASM();

}

void setupFunctions() {
}
#pragma endregion

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
		char path[MAX_PATH];
		GetWindowsDirectory(path, sizeof(path));

		// Example: "\\System32\\version.dll"
		strcat_s(path, "\\add manual path\\sideloader.dll");
		sideloader.dll = LoadLibrary(path);
		setupFunctions();

		// Add here your code, I recommend you to create a thread
		break;
	case DLL_PROCESS_DETACH:
		FreeLibrary(sideloader.dll);
		break;
	}
	return 1;
}
