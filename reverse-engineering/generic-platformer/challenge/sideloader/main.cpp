#include <Windows.h>
#include <iostream>
#include <cstdint>
#include <polyhook2/Detour/x64Detour.hpp>
#include <polyhook2/CapstoneDisassembler.hpp>

std::uintptr_t load_metadata_address = 0x00142340;
//std::uintptr_t game_assembly = (std::uintptr_t)LoadLibraryA("msvcbin.dll");
std::uintptr_t game_assembly = 0;

/* Xor Key Table */
std::uint8_t key_table[] = 
{
    0x38, 0x25, 0xAB, 0x73,
    0x3A, 0x2D, 0x9B, 0x6B,
    0x3C, 0x35, 0x8B, 0x63,
    0x3E, 0x3D, 0x7B, 0x5B,
    0x40, 0x45, 0x6B, 0x53,
    0x42, 0x4D, 0x5B, 0x4B,
    0xDC, 0x55, 0x4B, 0x43,
    0xDE, 0x5D, 0x3B, 0x3B,
};

void xor_encrypt( std::uint8_t* data, std::size_t size )
{
    for ( std::size_t i = 0; i < size; i++ )
    {
        data[ i ] ^= key_table[ i % sizeof( key_table ) ];
    }
}

/* constexpr fnv1a hashing */
constexpr std::uint64_t fnv1a_64(const char* str, std::size_t count) {
    std::uint64_t hash = 0xcbf29ce484222325;
    for (std::size_t i = 0; i < count; ++i) {
        hash ^= str[i];
        hash *= 0x100000001b3;
    }
    return hash;
}

std::size_t get_length_of_metadata( std::uint8_t* data )
{
    std::size_t size = 0;
    /* End signature: `61 06 00 00 63 06 00 00 62 06 00 00 06 0D 00 00` (hex) */
    while ( data[ size ] != 0x61 || data[ size + 1 ] != 0x06 || data[ size + 2 ] != 0x00 || data[ size + 3 ] != 0x00 ||
            data[ size + 4 ] != 0x63 || data[ size + 5 ] != 0x06 || data[ size + 6 ] != 0x00 || data[ size + 7 ] != 0x00 ||
            data[ size + 8 ] != 0x62 || data[ size + 9 ] != 0x06 || data[ size + 10 ] != 0x00 || data[ size + 11 ] != 0x00 ||
            data[ size + 12 ] != 0x06 || data[ size + 13 ] != 0x0D || data[ size + 14 ] != 0x00 || data[ size + 15 ] != 0x00 )
    {
        size++;
    }
    return size + 16;
}

PVOID original_load_metadata = nullptr;
/* Our custom callback for metadata loading */
std::uint8_t* __fastcall hook_load_metadata( const char* filename )
{
    /* Ensure filename is `global-metadata.dat` */
    if (fnv1a_64(filename, strlen(filename)) == fnv1a_64("global-metadata.dat", 19)) 
    {
        /* Call original */
        std::uint8_t* result = PLH::FnCast(original_load_metadata, hook_load_metadata)( filename );
        /* Get size */
        std::size_t size = get_length_of_metadata( result );
        /* Debug messagebox */
        MessageBoxA( nullptr, std::to_string( size ).c_str(), "Size", MB_OK );
        /* Decrypt */
        xor_encrypt( (std::uint8_t*)result, size );
		
        /* Return now decrypted metadata */
        return result;
    }

	/* Call original */
	return PLH::FnCast( original_load_metadata, hook_load_metadata )( filename );
}

bool initialize_metadata_decryption_routine( )
{
    PLH::CapstoneDisassembler dis(PLH::Mode::x64);
    PLH::x64Detour detour((uint64_t)(game_assembly + load_metadata_address), (uint64_t)hook_load_metadata, (uint64_t*)&original_load_metadata, dis);
    detour.hook( );

    /* Busy wait to keep thread alive */
    while (true)
    {
        Sleep( 1000 );
    }
}


// Explained in p. 2 below
void NTAPI tls_callback(PVOID DllHandle, DWORD dwReason, PVOID)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        MessageBoxA(NULL, "Call", NULL, NULL);
        //CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)initialize_metadata_decryption_routine, NULL, NULL, NULL);
    }
}

#ifdef _WIN64
#pragma comment (linker, "/INCLUDE:_tls_used")  // See p. 1 below
#pragma comment (linker, "/INCLUDE:tls_callback_func")  // See p. 3 below
#else
#pragma comment (linker, "/INCLUDE:__tls_used")  // See p. 1 below
#pragma comment (linker, "/INCLUDE:_tls_callback_func")  // See p. 3 below
#endif

// Explained in p. 3 below
#ifdef _WIN64
#pragma const_seg(".CRT$XLF")
EXTERN_C const
#else
#pragma data_seg(".CRT$XLF")
EXTERN_C
#endif
PIMAGE_TLS_CALLBACK tls_callback_func = tls_callback;
#ifdef _WIN64
#pragma const_seg()
#else
#pragma data_seg()
#endif //_WIN64

static int loader = (CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)initialize_metadata_decryption_routine, NULL, NULL, NULL), 0);

/* DllMain */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}