#include <Windows.h>
#include <iostream>
#include <cstdint>
#include <polyhook2/Detour/x64Detour.hpp>
#include <polyhook2/CapstoneDisassembler.hpp>

#pragma region Proxy
struct GameAssembly_dll {
	HMODULE dll;
	FARPROC oCloseZStream;
	FARPROC oCreateZStream;
	FARPROC oDllCanUnloadNow;
	FARPROC oDllGetActivationFactory;
	FARPROC oFlush;
	FARPROC oReadZStream;
	FARPROC oUnityPalGetLocalTimeZoneData;
	FARPROC oUnityPalGetTimeZoneDataForID;
	FARPROC oUnityPalTimeZoneInfoGetTimeZoneIDs;
	FARPROC oUseUnityPalForTimeZoneInformation;
	FARPROC oWriteZStream;
	FARPROC oil2cpp_add_internal_call;
	FARPROC oil2cpp_alloc;
	FARPROC oil2cpp_allocation_granularity;
	FARPROC oil2cpp_array_class_get;
	FARPROC oil2cpp_array_element_size;
	FARPROC oil2cpp_array_get_byte_length;
	FARPROC oil2cpp_array_length;
	FARPROC oil2cpp_array_new;
	FARPROC oil2cpp_array_new_full;
	FARPROC oil2cpp_array_new_specific;
	FARPROC oil2cpp_array_object_header_size;
	FARPROC oil2cpp_assembly_get_image;
	FARPROC oil2cpp_bounded_array_class_get;
	FARPROC oil2cpp_capture_memory_snapshot;
	FARPROC oil2cpp_class_array_element_size;
	FARPROC oil2cpp_class_enum_basetype;
	FARPROC oil2cpp_class_for_each;
	FARPROC oil2cpp_class_from_il2cpp_type;
	FARPROC oil2cpp_class_from_name;
	FARPROC oil2cpp_class_from_system_type;
	FARPROC oil2cpp_class_from_type;
	FARPROC oil2cpp_class_get_assemblyname;
	FARPROC oil2cpp_class_get_bitmap;
	FARPROC oil2cpp_class_get_bitmap_size;
	FARPROC oil2cpp_class_get_data_size;
	FARPROC oil2cpp_class_get_declaring_type;
	FARPROC oil2cpp_class_get_element_class;
	FARPROC oil2cpp_class_get_events;
	FARPROC oil2cpp_class_get_field_from_name;
	FARPROC oil2cpp_class_get_fields;
	FARPROC oil2cpp_class_get_flags;
	FARPROC oil2cpp_class_get_image;
	FARPROC oil2cpp_class_get_interfaces;
	FARPROC oil2cpp_class_get_method_from_name;
	FARPROC oil2cpp_class_get_methods;
	FARPROC oil2cpp_class_get_name;
	FARPROC oil2cpp_class_get_namespace;
	FARPROC oil2cpp_class_get_nested_types;
	FARPROC oil2cpp_class_get_parent;
	FARPROC oil2cpp_class_get_properties;
	FARPROC oil2cpp_class_get_property_from_name;
	FARPROC oil2cpp_class_get_rank;
	FARPROC oil2cpp_class_get_static_field_data;
	FARPROC oil2cpp_class_get_type;
	FARPROC oil2cpp_class_get_type_token;
	FARPROC oil2cpp_class_get_userdata_offset;
	FARPROC oil2cpp_class_has_attribute;
	FARPROC oil2cpp_class_has_parent;
	FARPROC oil2cpp_class_has_references;
	FARPROC oil2cpp_class_instance_size;
	FARPROC oil2cpp_class_is_abstract;
	FARPROC oil2cpp_class_is_assignable_from;
	FARPROC oil2cpp_class_is_blittable;
	FARPROC oil2cpp_class_is_enum;
	FARPROC oil2cpp_class_is_generic;
	FARPROC oil2cpp_class_is_inflated;
	FARPROC oil2cpp_class_is_interface;
	FARPROC oil2cpp_class_is_subclass_of;
	FARPROC oil2cpp_class_is_valuetype;
	FARPROC oil2cpp_class_num_fields;
	FARPROC oil2cpp_class_set_userdata;
	FARPROC oil2cpp_class_value_size;
	FARPROC oil2cpp_current_thread_get_frame_at;
	FARPROC oil2cpp_current_thread_get_stack_depth;
	FARPROC oil2cpp_current_thread_get_top_frame;
	FARPROC oil2cpp_current_thread_walk_frame_stack;
	FARPROC oil2cpp_custom_attrs_construct;
	FARPROC oil2cpp_custom_attrs_free;
	FARPROC oil2cpp_custom_attrs_from_class;
	FARPROC oil2cpp_custom_attrs_from_method;
	FARPROC oil2cpp_custom_attrs_get_attr;
	FARPROC oil2cpp_custom_attrs_has_attr;
	FARPROC oil2cpp_debug_get_method_info;
	FARPROC oil2cpp_debugger_set_agent_options;
	FARPROC oil2cpp_domain_assembly_open;
	FARPROC oil2cpp_domain_get;
	FARPROC oil2cpp_domain_get_assemblies;
	FARPROC oil2cpp_exception_from_name_msg;
	FARPROC oil2cpp_field_get_flags;
	FARPROC oil2cpp_field_get_name;
	FARPROC oil2cpp_field_get_offset;
	FARPROC oil2cpp_field_get_parent;
	FARPROC oil2cpp_field_get_type;
	FARPROC oil2cpp_field_get_value;
	FARPROC oil2cpp_field_get_value_object;
	FARPROC oil2cpp_field_has_attribute;
	FARPROC oil2cpp_field_is_literal;
	FARPROC oil2cpp_field_set_value;
	FARPROC oil2cpp_field_set_value_object;
	FARPROC oil2cpp_field_static_get_value;
	FARPROC oil2cpp_field_static_set_value;
	FARPROC oil2cpp_format_exception;
	FARPROC oil2cpp_format_stack_trace;
	FARPROC oil2cpp_free;
	FARPROC oil2cpp_free_captured_memory_snapshot;
	FARPROC oil2cpp_gc_collect;
	FARPROC oil2cpp_gc_collect_a_little;
	FARPROC oil2cpp_gc_disable;
	FARPROC oil2cpp_gc_enable;
	FARPROC oil2cpp_gc_foreach_heap;
	FARPROC oil2cpp_gc_get_heap_size;
	FARPROC oil2cpp_gc_get_max_time_slice_ns;
	FARPROC oil2cpp_gc_get_used_size;
	FARPROC oil2cpp_gc_has_strict_wbarriers;
	FARPROC oil2cpp_gc_is_disabled;
	FARPROC oil2cpp_gc_is_incremental;
	FARPROC oil2cpp_gc_set_external_allocation_tracker;
	FARPROC oil2cpp_gc_set_external_wbarrier_tracker;
	FARPROC oil2cpp_gc_set_max_time_slice_ns;
	FARPROC oil2cpp_gc_set_mode;
	FARPROC oil2cpp_gc_start_incremental_collection;
	FARPROC oil2cpp_gc_wbarrier_set_field;
	FARPROC oil2cpp_gchandle_foreach_get_target;
	FARPROC oil2cpp_gchandle_free;
	FARPROC oil2cpp_gchandle_get_target;
	FARPROC oil2cpp_gchandle_new;
	FARPROC oil2cpp_gchandle_new_weakref;
	FARPROC oil2cpp_get_corlib;
	FARPROC oil2cpp_get_exception_argument_null;
	FARPROC oil2cpp_image_get_assembly;
	FARPROC oil2cpp_image_get_class;
	FARPROC oil2cpp_image_get_class_count;
	FARPROC oil2cpp_image_get_entry_point;
	FARPROC oil2cpp_image_get_filename;
	FARPROC oil2cpp_image_get_name;
	FARPROC oil2cpp_init;
	FARPROC oil2cpp_init_utf16;
	FARPROC oil2cpp_is_debugger_attached;
	FARPROC oil2cpp_is_vm_thread;
	FARPROC oil2cpp_method_get_class;
	FARPROC oil2cpp_method_get_declaring_type;
	FARPROC oil2cpp_method_get_flags;
	FARPROC oil2cpp_method_get_from_reflection;
	FARPROC oil2cpp_method_get_name;
	FARPROC oil2cpp_method_get_object;
	FARPROC oil2cpp_method_get_param;
	FARPROC oil2cpp_method_get_param_count;
	FARPROC oil2cpp_method_get_param_name;
	FARPROC oil2cpp_method_get_return_type;
	FARPROC oil2cpp_method_get_token;
	FARPROC oil2cpp_method_has_attribute;
	FARPROC oil2cpp_method_is_generic;
	FARPROC oil2cpp_method_is_inflated;
	FARPROC oil2cpp_method_is_instance;
	FARPROC oil2cpp_monitor_enter;
	FARPROC oil2cpp_monitor_exit;
	FARPROC oil2cpp_monitor_pulse;
	FARPROC oil2cpp_monitor_pulse_all;
	FARPROC oil2cpp_monitor_try_enter;
	FARPROC oil2cpp_monitor_try_wait;
	FARPROC oil2cpp_monitor_wait;
	FARPROC oil2cpp_native_stack_trace;
	FARPROC oil2cpp_object_get_class;
	FARPROC oil2cpp_object_get_size;
	FARPROC oil2cpp_object_get_virtual_method;
	FARPROC oil2cpp_object_header_size;
	FARPROC oil2cpp_object_new;
	FARPROC oil2cpp_object_unbox;
	FARPROC oil2cpp_offset_of_array_bounds_in_array_object_header;
	FARPROC oil2cpp_offset_of_array_length_in_array_object_header;
	FARPROC oil2cpp_override_stack_backtrace;
	FARPROC oil2cpp_profiler_install;
	FARPROC oil2cpp_profiler_install_allocation;
	FARPROC oil2cpp_profiler_install_enter_leave;
	FARPROC oil2cpp_profiler_install_fileio;
	FARPROC oil2cpp_profiler_install_gc;
	FARPROC oil2cpp_profiler_install_thread;
	FARPROC oil2cpp_profiler_set_events;
	FARPROC oil2cpp_property_get_flags;
	FARPROC oil2cpp_property_get_get_method;
	FARPROC oil2cpp_property_get_name;
	FARPROC oil2cpp_property_get_parent;
	FARPROC oil2cpp_property_get_set_method;
	FARPROC oil2cpp_raise_exception;
	FARPROC oil2cpp_register_debugger_agent_transport;
	FARPROC oil2cpp_register_log_callback;
	FARPROC oil2cpp_resolve_icall;
	FARPROC oil2cpp_runtime_class_init;
	FARPROC oil2cpp_runtime_invoke;
	FARPROC oil2cpp_runtime_invoke_convert_args;
	FARPROC oil2cpp_runtime_object_init;
	FARPROC oil2cpp_runtime_object_init_exception;
	FARPROC oil2cpp_runtime_unhandled_exception_policy_set;
	FARPROC oil2cpp_set_commandline_arguments;
	FARPROC oil2cpp_set_commandline_arguments_utf16;
	FARPROC oil2cpp_set_config;
	FARPROC oil2cpp_set_config_dir;
	FARPROC oil2cpp_set_config_utf16;
	FARPROC oil2cpp_set_data_dir;
	FARPROC oil2cpp_set_default_thread_affinity;
	FARPROC oil2cpp_set_find_plugin_callback;
	FARPROC oil2cpp_set_memory_callbacks;
	FARPROC oil2cpp_set_temp_dir;
	FARPROC oil2cpp_shutdown;
	FARPROC oil2cpp_start_gc_world;
	FARPROC oil2cpp_stats_dump_to_file;
	FARPROC oil2cpp_stats_get_value;
	FARPROC oil2cpp_stop_gc_world;
	FARPROC oil2cpp_string_chars;
	FARPROC oil2cpp_string_intern;
	FARPROC oil2cpp_string_is_interned;
	FARPROC oil2cpp_string_length;
	FARPROC oil2cpp_string_new;
	FARPROC oil2cpp_string_new_len;
	FARPROC oil2cpp_string_new_utf16;
	FARPROC oil2cpp_string_new_wrapper;
	FARPROC oil2cpp_thread_attach;
	FARPROC oil2cpp_thread_current;
	FARPROC oil2cpp_thread_detach;
	FARPROC oil2cpp_thread_get_all_attached_threads;
	FARPROC oil2cpp_thread_get_frame_at;
	FARPROC oil2cpp_thread_get_stack_depth;
	FARPROC oil2cpp_thread_get_top_frame;
	FARPROC oil2cpp_thread_walk_frame_stack;
	FARPROC oil2cpp_type_equals;
	FARPROC oil2cpp_type_get_assembly_qualified_name;
	FARPROC oil2cpp_type_get_attrs;
	FARPROC oil2cpp_type_get_class_or_element_class;
	FARPROC oil2cpp_type_get_name;
	FARPROC oil2cpp_type_get_name_chunked;
	FARPROC oil2cpp_type_get_object;
	FARPROC oil2cpp_type_get_type;
	FARPROC oil2cpp_type_is_byref;
	FARPROC oil2cpp_type_is_pointer_type;
	FARPROC oil2cpp_type_is_static;
	FARPROC oil2cpp_unhandled_exception;
	FARPROC oil2cpp_unity_install_unitytls_interface;
	FARPROC oil2cpp_unity_liveness_calculation_begin;
	FARPROC oil2cpp_unity_liveness_calculation_end;
	FARPROC oil2cpp_unity_liveness_calculation_from_root;
	FARPROC oil2cpp_unity_liveness_calculation_from_statics;
	FARPROC oil2cpp_value_box;
} GameAssembly;

extern "C" {
	FARPROC PA = 0;
	int runASM();

	void fCloseZStream() { PA = GameAssembly.oCloseZStream; runASM(); }
	void fCreateZStream() { PA = GameAssembly.oCreateZStream; runASM(); }
	void fDllCanUnloadNow() { PA = GameAssembly.oDllCanUnloadNow; runASM(); }
	void fDllGetActivationFactory() { PA = GameAssembly.oDllGetActivationFactory; runASM(); }
	void fFlush() { PA = GameAssembly.oFlush; runASM(); }
	void fReadZStream() { PA = GameAssembly.oReadZStream; runASM(); }
	void fUnityPalGetLocalTimeZoneData() { PA = GameAssembly.oUnityPalGetLocalTimeZoneData; runASM(); }
	void fUnityPalGetTimeZoneDataForID() { PA = GameAssembly.oUnityPalGetTimeZoneDataForID; runASM(); }
	void fUnityPalTimeZoneInfoGetTimeZoneIDs() { PA = GameAssembly.oUnityPalTimeZoneInfoGetTimeZoneIDs; runASM(); }
	void fUseUnityPalForTimeZoneInformation() { PA = GameAssembly.oUseUnityPalForTimeZoneInformation; runASM(); }
	void fWriteZStream() { PA = GameAssembly.oWriteZStream; runASM(); }
	void fil2cpp_add_internal_call() { PA = GameAssembly.oil2cpp_add_internal_call; runASM(); }
	void fil2cpp_alloc() { PA = GameAssembly.oil2cpp_alloc; runASM(); }
	void fil2cpp_allocation_granularity() { PA = GameAssembly.oil2cpp_allocation_granularity; runASM(); }
	void fil2cpp_array_class_get() { PA = GameAssembly.oil2cpp_array_class_get; runASM(); }
	void fil2cpp_array_element_size() { PA = GameAssembly.oil2cpp_array_element_size; runASM(); }
	void fil2cpp_array_get_byte_length() { PA = GameAssembly.oil2cpp_array_get_byte_length; runASM(); }
	void fil2cpp_array_length() { PA = GameAssembly.oil2cpp_array_length; runASM(); }
	void fil2cpp_array_new() { PA = GameAssembly.oil2cpp_array_new; runASM(); }
	void fil2cpp_array_new_full() { PA = GameAssembly.oil2cpp_array_new_full; runASM(); }
	void fil2cpp_array_new_specific() { PA = GameAssembly.oil2cpp_array_new_specific; runASM(); }
	void fil2cpp_array_object_header_size() { PA = GameAssembly.oil2cpp_array_object_header_size; runASM(); }
	void fil2cpp_assembly_get_image() { PA = GameAssembly.oil2cpp_assembly_get_image; runASM(); }
	void fil2cpp_bounded_array_class_get() { PA = GameAssembly.oil2cpp_bounded_array_class_get; runASM(); }
	void fil2cpp_capture_memory_snapshot() { PA = GameAssembly.oil2cpp_capture_memory_snapshot; runASM(); }
	void fil2cpp_class_array_element_size() { PA = GameAssembly.oil2cpp_class_array_element_size; runASM(); }
	void fil2cpp_class_enum_basetype() { PA = GameAssembly.oil2cpp_class_enum_basetype; runASM(); }
	void fil2cpp_class_for_each() { PA = GameAssembly.oil2cpp_class_for_each; runASM(); }
	void fil2cpp_class_from_il2cpp_type() { PA = GameAssembly.oil2cpp_class_from_il2cpp_type; runASM(); }
	void fil2cpp_class_from_name() { PA = GameAssembly.oil2cpp_class_from_name; runASM(); }
	void fil2cpp_class_from_system_type() { PA = GameAssembly.oil2cpp_class_from_system_type; runASM(); }
	void fil2cpp_class_from_type() { PA = GameAssembly.oil2cpp_class_from_type; runASM(); }
	void fil2cpp_class_get_assemblyname() { PA = GameAssembly.oil2cpp_class_get_assemblyname; runASM(); }
	void fil2cpp_class_get_bitmap() { PA = GameAssembly.oil2cpp_class_get_bitmap; runASM(); }
	void fil2cpp_class_get_bitmap_size() { PA = GameAssembly.oil2cpp_class_get_bitmap_size; runASM(); }
	void fil2cpp_class_get_data_size() { PA = GameAssembly.oil2cpp_class_get_data_size; runASM(); }
	void fil2cpp_class_get_declaring_type() { PA = GameAssembly.oil2cpp_class_get_declaring_type; runASM(); }
	void fil2cpp_class_get_element_class() { PA = GameAssembly.oil2cpp_class_get_element_class; runASM(); }
	void fil2cpp_class_get_events() { PA = GameAssembly.oil2cpp_class_get_events; runASM(); }
	void fil2cpp_class_get_field_from_name() { PA = GameAssembly.oil2cpp_class_get_field_from_name; runASM(); }
	void fil2cpp_class_get_fields() { PA = GameAssembly.oil2cpp_class_get_fields; runASM(); }
	void fil2cpp_class_get_flags() { PA = GameAssembly.oil2cpp_class_get_flags; runASM(); }
	void fil2cpp_class_get_image() { PA = GameAssembly.oil2cpp_class_get_image; runASM(); }
	void fil2cpp_class_get_interfaces() { PA = GameAssembly.oil2cpp_class_get_interfaces; runASM(); }
	void fil2cpp_class_get_method_from_name() { PA = GameAssembly.oil2cpp_class_get_method_from_name; runASM(); }
	void fil2cpp_class_get_methods() { PA = GameAssembly.oil2cpp_class_get_methods; runASM(); }
	void fil2cpp_class_get_name() { PA = GameAssembly.oil2cpp_class_get_name; runASM(); }
	void fil2cpp_class_get_namespace() { PA = GameAssembly.oil2cpp_class_get_namespace; runASM(); }
	void fil2cpp_class_get_nested_types() { PA = GameAssembly.oil2cpp_class_get_nested_types; runASM(); }
	void fil2cpp_class_get_parent() { PA = GameAssembly.oil2cpp_class_get_parent; runASM(); }
	void fil2cpp_class_get_properties() { PA = GameAssembly.oil2cpp_class_get_properties; runASM(); }
	void fil2cpp_class_get_property_from_name() { PA = GameAssembly.oil2cpp_class_get_property_from_name; runASM(); }
	void fil2cpp_class_get_rank() { PA = GameAssembly.oil2cpp_class_get_rank; runASM(); }
	void fil2cpp_class_get_static_field_data() { PA = GameAssembly.oil2cpp_class_get_static_field_data; runASM(); }
	void fil2cpp_class_get_type() { PA = GameAssembly.oil2cpp_class_get_type; runASM(); }
	void fil2cpp_class_get_type_token() { PA = GameAssembly.oil2cpp_class_get_type_token; runASM(); }
	void fil2cpp_class_get_userdata_offset() { PA = GameAssembly.oil2cpp_class_get_userdata_offset; runASM(); }
	void fil2cpp_class_has_attribute() { PA = GameAssembly.oil2cpp_class_has_attribute; runASM(); }
	void fil2cpp_class_has_parent() { PA = GameAssembly.oil2cpp_class_has_parent; runASM(); }
	void fil2cpp_class_has_references() { PA = GameAssembly.oil2cpp_class_has_references; runASM(); }
	void fil2cpp_class_instance_size() { PA = GameAssembly.oil2cpp_class_instance_size; runASM(); }
	void fil2cpp_class_is_abstract() { PA = GameAssembly.oil2cpp_class_is_abstract; runASM(); }
	void fil2cpp_class_is_assignable_from() { PA = GameAssembly.oil2cpp_class_is_assignable_from; runASM(); }
	void fil2cpp_class_is_blittable() { PA = GameAssembly.oil2cpp_class_is_blittable; runASM(); }
	void fil2cpp_class_is_enum() { PA = GameAssembly.oil2cpp_class_is_enum; runASM(); }
	void fil2cpp_class_is_generic() { PA = GameAssembly.oil2cpp_class_is_generic; runASM(); }
	void fil2cpp_class_is_inflated() { PA = GameAssembly.oil2cpp_class_is_inflated; runASM(); }
	void fil2cpp_class_is_interface() { PA = GameAssembly.oil2cpp_class_is_interface; runASM(); }
	void fil2cpp_class_is_subclass_of() { PA = GameAssembly.oil2cpp_class_is_subclass_of; runASM(); }
	void fil2cpp_class_is_valuetype() { PA = GameAssembly.oil2cpp_class_is_valuetype; runASM(); }
	void fil2cpp_class_num_fields() { PA = GameAssembly.oil2cpp_class_num_fields; runASM(); }
	void fil2cpp_class_set_userdata() { PA = GameAssembly.oil2cpp_class_set_userdata; runASM(); }
	void fil2cpp_class_value_size() { PA = GameAssembly.oil2cpp_class_value_size; runASM(); }
	void fil2cpp_current_thread_get_frame_at() { PA = GameAssembly.oil2cpp_current_thread_get_frame_at; runASM(); }
	void fil2cpp_current_thread_get_stack_depth() { PA = GameAssembly.oil2cpp_current_thread_get_stack_depth; runASM(); }
	void fil2cpp_current_thread_get_top_frame() { PA = GameAssembly.oil2cpp_current_thread_get_top_frame; runASM(); }
	void fil2cpp_current_thread_walk_frame_stack() { PA = GameAssembly.oil2cpp_current_thread_walk_frame_stack; runASM(); }
	void fil2cpp_custom_attrs_construct() { PA = GameAssembly.oil2cpp_custom_attrs_construct; runASM(); }
	void fil2cpp_custom_attrs_free() { PA = GameAssembly.oil2cpp_custom_attrs_free; runASM(); }
	void fil2cpp_custom_attrs_from_class() { PA = GameAssembly.oil2cpp_custom_attrs_from_class; runASM(); }
	void fil2cpp_custom_attrs_from_method() { PA = GameAssembly.oil2cpp_custom_attrs_from_method; runASM(); }
	void fil2cpp_custom_attrs_get_attr() { PA = GameAssembly.oil2cpp_custom_attrs_get_attr; runASM(); }
	void fil2cpp_custom_attrs_has_attr() { PA = GameAssembly.oil2cpp_custom_attrs_has_attr; runASM(); }
	void fil2cpp_debug_get_method_info() { PA = GameAssembly.oil2cpp_debug_get_method_info; runASM(); }
	void fil2cpp_debugger_set_agent_options() { PA = GameAssembly.oil2cpp_debugger_set_agent_options; runASM(); }
	void fil2cpp_domain_assembly_open() { PA = GameAssembly.oil2cpp_domain_assembly_open; runASM(); }
	void fil2cpp_domain_get() { PA = GameAssembly.oil2cpp_domain_get; runASM(); }
	void fil2cpp_domain_get_assemblies() { PA = GameAssembly.oil2cpp_domain_get_assemblies; runASM(); }
	void fil2cpp_exception_from_name_msg() { PA = GameAssembly.oil2cpp_exception_from_name_msg; runASM(); }
	void fil2cpp_field_get_flags() { PA = GameAssembly.oil2cpp_field_get_flags; runASM(); }
	void fil2cpp_field_get_name() { PA = GameAssembly.oil2cpp_field_get_name; runASM(); }
	void fil2cpp_field_get_offset() { PA = GameAssembly.oil2cpp_field_get_offset; runASM(); }
	void fil2cpp_field_get_parent() { PA = GameAssembly.oil2cpp_field_get_parent; runASM(); }
	void fil2cpp_field_get_type() { PA = GameAssembly.oil2cpp_field_get_type; runASM(); }
	void fil2cpp_field_get_value() { PA = GameAssembly.oil2cpp_field_get_value; runASM(); }
	void fil2cpp_field_get_value_object() { PA = GameAssembly.oil2cpp_field_get_value_object; runASM(); }
	void fil2cpp_field_has_attribute() { PA = GameAssembly.oil2cpp_field_has_attribute; runASM(); }
	void fil2cpp_field_is_literal() { PA = GameAssembly.oil2cpp_field_is_literal; runASM(); }
	void fil2cpp_field_set_value() { PA = GameAssembly.oil2cpp_field_set_value; runASM(); }
	void fil2cpp_field_set_value_object() { PA = GameAssembly.oil2cpp_field_set_value_object; runASM(); }
	void fil2cpp_field_static_get_value() { PA = GameAssembly.oil2cpp_field_static_get_value; runASM(); }
	void fil2cpp_field_static_set_value() { PA = GameAssembly.oil2cpp_field_static_set_value; runASM(); }
	void fil2cpp_format_exception() { PA = GameAssembly.oil2cpp_format_exception; runASM(); }
	void fil2cpp_format_stack_trace() { PA = GameAssembly.oil2cpp_format_stack_trace; runASM(); }
	void fil2cpp_free() { PA = GameAssembly.oil2cpp_free; runASM(); }
	void fil2cpp_free_captured_memory_snapshot() { PA = GameAssembly.oil2cpp_free_captured_memory_snapshot; runASM(); }
	void fil2cpp_gc_collect() { PA = GameAssembly.oil2cpp_gc_collect; runASM(); }
	void fil2cpp_gc_collect_a_little() { PA = GameAssembly.oil2cpp_gc_collect_a_little; runASM(); }
	void fil2cpp_gc_disable() { PA = GameAssembly.oil2cpp_gc_disable; runASM(); }
	void fil2cpp_gc_enable() { PA = GameAssembly.oil2cpp_gc_enable; runASM(); }
	void fil2cpp_gc_foreach_heap() { PA = GameAssembly.oil2cpp_gc_foreach_heap; runASM(); }
	void fil2cpp_gc_get_heap_size() { PA = GameAssembly.oil2cpp_gc_get_heap_size; runASM(); }
	void fil2cpp_gc_get_max_time_slice_ns() { PA = GameAssembly.oil2cpp_gc_get_max_time_slice_ns; runASM(); }
	void fil2cpp_gc_get_used_size() { PA = GameAssembly.oil2cpp_gc_get_used_size; runASM(); }
	void fil2cpp_gc_has_strict_wbarriers() { PA = GameAssembly.oil2cpp_gc_has_strict_wbarriers; runASM(); }
	void fil2cpp_gc_is_disabled() { PA = GameAssembly.oil2cpp_gc_is_disabled; runASM(); }
	void fil2cpp_gc_is_incremental() { PA = GameAssembly.oil2cpp_gc_is_incremental; runASM(); }
	void fil2cpp_gc_set_external_allocation_tracker() { PA = GameAssembly.oil2cpp_gc_set_external_allocation_tracker; runASM(); }
	void fil2cpp_gc_set_external_wbarrier_tracker() { PA = GameAssembly.oil2cpp_gc_set_external_wbarrier_tracker; runASM(); }
	void fil2cpp_gc_set_max_time_slice_ns() { PA = GameAssembly.oil2cpp_gc_set_max_time_slice_ns; runASM(); }
	void fil2cpp_gc_set_mode() { PA = GameAssembly.oil2cpp_gc_set_mode; runASM(); }
	void fil2cpp_gc_start_incremental_collection() { PA = GameAssembly.oil2cpp_gc_start_incremental_collection; runASM(); }
	void fil2cpp_gc_wbarrier_set_field() { PA = GameAssembly.oil2cpp_gc_wbarrier_set_field; runASM(); }
	void fil2cpp_gchandle_foreach_get_target() { PA = GameAssembly.oil2cpp_gchandle_foreach_get_target; runASM(); }
	void fil2cpp_gchandle_free() { PA = GameAssembly.oil2cpp_gchandle_free; runASM(); }
	void fil2cpp_gchandle_get_target() { PA = GameAssembly.oil2cpp_gchandle_get_target; runASM(); }
	void fil2cpp_gchandle_new() { PA = GameAssembly.oil2cpp_gchandle_new; runASM(); }
	void fil2cpp_gchandle_new_weakref() { PA = GameAssembly.oil2cpp_gchandle_new_weakref; runASM(); }
	void fil2cpp_get_corlib() { PA = GameAssembly.oil2cpp_get_corlib; runASM(); }
	void fil2cpp_get_exception_argument_null() { PA = GameAssembly.oil2cpp_get_exception_argument_null; runASM(); }
	void fil2cpp_image_get_assembly() { PA = GameAssembly.oil2cpp_image_get_assembly; runASM(); }
	void fil2cpp_image_get_class() { PA = GameAssembly.oil2cpp_image_get_class; runASM(); }
	void fil2cpp_image_get_class_count() { PA = GameAssembly.oil2cpp_image_get_class_count; runASM(); }
	void fil2cpp_image_get_entry_point() { PA = GameAssembly.oil2cpp_image_get_entry_point; runASM(); }
	void fil2cpp_image_get_filename() { PA = GameAssembly.oil2cpp_image_get_filename; runASM(); }
	void fil2cpp_image_get_name() { PA = GameAssembly.oil2cpp_image_get_name; runASM(); }
	void fil2cpp_init() { PA = GameAssembly.oil2cpp_init; runASM(); }
	void fil2cpp_init_utf16() { PA = GameAssembly.oil2cpp_init_utf16; runASM(); }
	void fil2cpp_is_debugger_attached() { PA = GameAssembly.oil2cpp_is_debugger_attached; runASM(); }
	void fil2cpp_is_vm_thread() { PA = GameAssembly.oil2cpp_is_vm_thread; runASM(); }
	void fil2cpp_method_get_class() { PA = GameAssembly.oil2cpp_method_get_class; runASM(); }
	void fil2cpp_method_get_declaring_type() { PA = GameAssembly.oil2cpp_method_get_declaring_type; runASM(); }
	void fil2cpp_method_get_flags() { PA = GameAssembly.oil2cpp_method_get_flags; runASM(); }
	void fil2cpp_method_get_from_reflection() { PA = GameAssembly.oil2cpp_method_get_from_reflection; runASM(); }
	void fil2cpp_method_get_name() { PA = GameAssembly.oil2cpp_method_get_name; runASM(); }
	void fil2cpp_method_get_object() { PA = GameAssembly.oil2cpp_method_get_object; runASM(); }
	void fil2cpp_method_get_param() { PA = GameAssembly.oil2cpp_method_get_param; runASM(); }
	void fil2cpp_method_get_param_count() { PA = GameAssembly.oil2cpp_method_get_param_count; runASM(); }
	void fil2cpp_method_get_param_name() { PA = GameAssembly.oil2cpp_method_get_param_name; runASM(); }
	void fil2cpp_method_get_return_type() { PA = GameAssembly.oil2cpp_method_get_return_type; runASM(); }
	void fil2cpp_method_get_token() { PA = GameAssembly.oil2cpp_method_get_token; runASM(); }
	void fil2cpp_method_has_attribute() { PA = GameAssembly.oil2cpp_method_has_attribute; runASM(); }
	void fil2cpp_method_is_generic() { PA = GameAssembly.oil2cpp_method_is_generic; runASM(); }
	void fil2cpp_method_is_inflated() { PA = GameAssembly.oil2cpp_method_is_inflated; runASM(); }
	void fil2cpp_method_is_instance() { PA = GameAssembly.oil2cpp_method_is_instance; runASM(); }
	void fil2cpp_monitor_enter() { PA = GameAssembly.oil2cpp_monitor_enter; runASM(); }
	void fil2cpp_monitor_exit() { PA = GameAssembly.oil2cpp_monitor_exit; runASM(); }
	void fil2cpp_monitor_pulse() { PA = GameAssembly.oil2cpp_monitor_pulse; runASM(); }
	void fil2cpp_monitor_pulse_all() { PA = GameAssembly.oil2cpp_monitor_pulse_all; runASM(); }
	void fil2cpp_monitor_try_enter() { PA = GameAssembly.oil2cpp_monitor_try_enter; runASM(); }
	void fil2cpp_monitor_try_wait() { PA = GameAssembly.oil2cpp_monitor_try_wait; runASM(); }
	void fil2cpp_monitor_wait() { PA = GameAssembly.oil2cpp_monitor_wait; runASM(); }
	void fil2cpp_native_stack_trace() { PA = GameAssembly.oil2cpp_native_stack_trace; runASM(); }
	void fil2cpp_object_get_class() { PA = GameAssembly.oil2cpp_object_get_class; runASM(); }
	void fil2cpp_object_get_size() { PA = GameAssembly.oil2cpp_object_get_size; runASM(); }
	void fil2cpp_object_get_virtual_method() { PA = GameAssembly.oil2cpp_object_get_virtual_method; runASM(); }
	void fil2cpp_object_header_size() { PA = GameAssembly.oil2cpp_object_header_size; runASM(); }
	void fil2cpp_object_new() { PA = GameAssembly.oil2cpp_object_new; runASM(); }
	void fil2cpp_object_unbox() { PA = GameAssembly.oil2cpp_object_unbox; runASM(); }
	void fil2cpp_offset_of_array_bounds_in_array_object_header() { PA = GameAssembly.oil2cpp_offset_of_array_bounds_in_array_object_header; runASM(); }
	void fil2cpp_offset_of_array_length_in_array_object_header() { PA = GameAssembly.oil2cpp_offset_of_array_length_in_array_object_header; runASM(); }
	void fil2cpp_override_stack_backtrace() { PA = GameAssembly.oil2cpp_override_stack_backtrace; runASM(); }
	void fil2cpp_profiler_install() { PA = GameAssembly.oil2cpp_profiler_install; runASM(); }
	void fil2cpp_profiler_install_allocation() { PA = GameAssembly.oil2cpp_profiler_install_allocation; runASM(); }
	void fil2cpp_profiler_install_enter_leave() { PA = GameAssembly.oil2cpp_profiler_install_enter_leave; runASM(); }
	void fil2cpp_profiler_install_fileio() { PA = GameAssembly.oil2cpp_profiler_install_fileio; runASM(); }
	void fil2cpp_profiler_install_gc() { PA = GameAssembly.oil2cpp_profiler_install_gc; runASM(); }
	void fil2cpp_profiler_install_thread() { PA = GameAssembly.oil2cpp_profiler_install_thread; runASM(); }
	void fil2cpp_profiler_set_events() { PA = GameAssembly.oil2cpp_profiler_set_events; runASM(); }
	void fil2cpp_property_get_flags() { PA = GameAssembly.oil2cpp_property_get_flags; runASM(); }
	void fil2cpp_property_get_get_method() { PA = GameAssembly.oil2cpp_property_get_get_method; runASM(); }
	void fil2cpp_property_get_name() { PA = GameAssembly.oil2cpp_property_get_name; runASM(); }
	void fil2cpp_property_get_parent() { PA = GameAssembly.oil2cpp_property_get_parent; runASM(); }
	void fil2cpp_property_get_set_method() { PA = GameAssembly.oil2cpp_property_get_set_method; runASM(); }
	void fil2cpp_raise_exception() { PA = GameAssembly.oil2cpp_raise_exception; runASM(); }
	void fil2cpp_register_debugger_agent_transport() { PA = GameAssembly.oil2cpp_register_debugger_agent_transport; runASM(); }
	void fil2cpp_register_log_callback() { PA = GameAssembly.oil2cpp_register_log_callback; runASM(); }
	void fil2cpp_resolve_icall() { PA = GameAssembly.oil2cpp_resolve_icall; runASM(); }
	void fil2cpp_runtime_class_init() { PA = GameAssembly.oil2cpp_runtime_class_init; runASM(); }
	void fil2cpp_runtime_invoke() { PA = GameAssembly.oil2cpp_runtime_invoke; runASM(); }
	void fil2cpp_runtime_invoke_convert_args() { PA = GameAssembly.oil2cpp_runtime_invoke_convert_args; runASM(); }
	void fil2cpp_runtime_object_init() { PA = GameAssembly.oil2cpp_runtime_object_init; runASM(); }
	void fil2cpp_runtime_object_init_exception() { PA = GameAssembly.oil2cpp_runtime_object_init_exception; runASM(); }
	void fil2cpp_runtime_unhandled_exception_policy_set() { PA = GameAssembly.oil2cpp_runtime_unhandled_exception_policy_set; runASM(); }
	void fil2cpp_set_commandline_arguments() { PA = GameAssembly.oil2cpp_set_commandline_arguments; runASM(); }
	void fil2cpp_set_commandline_arguments_utf16() { PA = GameAssembly.oil2cpp_set_commandline_arguments_utf16; runASM(); }
	void fil2cpp_set_config() { PA = GameAssembly.oil2cpp_set_config; runASM(); }
	void fil2cpp_set_config_dir() { PA = GameAssembly.oil2cpp_set_config_dir; runASM(); }
	void fil2cpp_set_config_utf16() { PA = GameAssembly.oil2cpp_set_config_utf16; runASM(); }
	void fil2cpp_set_data_dir() { PA = GameAssembly.oil2cpp_set_data_dir; runASM(); }
	void fil2cpp_set_default_thread_affinity() { PA = GameAssembly.oil2cpp_set_default_thread_affinity; runASM(); }
	void fil2cpp_set_find_plugin_callback() { PA = GameAssembly.oil2cpp_set_find_plugin_callback; runASM(); }
	void fil2cpp_set_memory_callbacks() { PA = GameAssembly.oil2cpp_set_memory_callbacks; runASM(); }
	void fil2cpp_set_temp_dir() { PA = GameAssembly.oil2cpp_set_temp_dir; runASM(); }
	void fil2cpp_shutdown() { PA = GameAssembly.oil2cpp_shutdown; runASM(); }
	void fil2cpp_start_gc_world() { PA = GameAssembly.oil2cpp_start_gc_world; runASM(); }
	void fil2cpp_stats_dump_to_file() { PA = GameAssembly.oil2cpp_stats_dump_to_file; runASM(); }
	void fil2cpp_stats_get_value() { PA = GameAssembly.oil2cpp_stats_get_value; runASM(); }
	void fil2cpp_stop_gc_world() { PA = GameAssembly.oil2cpp_stop_gc_world; runASM(); }
	void fil2cpp_string_chars() { PA = GameAssembly.oil2cpp_string_chars; runASM(); }
	void fil2cpp_string_intern() { PA = GameAssembly.oil2cpp_string_intern; runASM(); }
	void fil2cpp_string_is_interned() { PA = GameAssembly.oil2cpp_string_is_interned; runASM(); }
	void fil2cpp_string_length() { PA = GameAssembly.oil2cpp_string_length; runASM(); }
	void fil2cpp_string_new() { PA = GameAssembly.oil2cpp_string_new; runASM(); }
	void fil2cpp_string_new_len() { PA = GameAssembly.oil2cpp_string_new_len; runASM(); }
	void fil2cpp_string_new_utf16() { PA = GameAssembly.oil2cpp_string_new_utf16; runASM(); }
	void fil2cpp_string_new_wrapper() { PA = GameAssembly.oil2cpp_string_new_wrapper; runASM(); }
	void fil2cpp_thread_attach() { PA = GameAssembly.oil2cpp_thread_attach; runASM(); }
	void fil2cpp_thread_current() { PA = GameAssembly.oil2cpp_thread_current; runASM(); }
	void fil2cpp_thread_detach() { PA = GameAssembly.oil2cpp_thread_detach; runASM(); }
	void fil2cpp_thread_get_all_attached_threads() { PA = GameAssembly.oil2cpp_thread_get_all_attached_threads; runASM(); }
	void fil2cpp_thread_get_frame_at() { PA = GameAssembly.oil2cpp_thread_get_frame_at; runASM(); }
	void fil2cpp_thread_get_stack_depth() { PA = GameAssembly.oil2cpp_thread_get_stack_depth; runASM(); }
	void fil2cpp_thread_get_top_frame() { PA = GameAssembly.oil2cpp_thread_get_top_frame; runASM(); }
	void fil2cpp_thread_walk_frame_stack() { PA = GameAssembly.oil2cpp_thread_walk_frame_stack; runASM(); }
	void fil2cpp_type_equals() { PA = GameAssembly.oil2cpp_type_equals; runASM(); }
	void fil2cpp_type_get_assembly_qualified_name() { PA = GameAssembly.oil2cpp_type_get_assembly_qualified_name; runASM(); }
	void fil2cpp_type_get_attrs() { PA = GameAssembly.oil2cpp_type_get_attrs; runASM(); }
	void fil2cpp_type_get_class_or_element_class() { PA = GameAssembly.oil2cpp_type_get_class_or_element_class; runASM(); }
	void fil2cpp_type_get_name() { PA = GameAssembly.oil2cpp_type_get_name; runASM(); }
	void fil2cpp_type_get_name_chunked() { PA = GameAssembly.oil2cpp_type_get_name_chunked; runASM(); }
	void fil2cpp_type_get_object() { PA = GameAssembly.oil2cpp_type_get_object; runASM(); }
	void fil2cpp_type_get_type() { PA = GameAssembly.oil2cpp_type_get_type; runASM(); }
	void fil2cpp_type_is_byref() { PA = GameAssembly.oil2cpp_type_is_byref; runASM(); }
	void fil2cpp_type_is_pointer_type() { PA = GameAssembly.oil2cpp_type_is_pointer_type; runASM(); }
	void fil2cpp_type_is_static() { PA = GameAssembly.oil2cpp_type_is_static; runASM(); }
	void fil2cpp_unhandled_exception() { PA = GameAssembly.oil2cpp_unhandled_exception; runASM(); }
	void fil2cpp_unity_install_unitytls_interface() { PA = GameAssembly.oil2cpp_unity_install_unitytls_interface; runASM(); }
	void fil2cpp_unity_liveness_calculation_begin() { PA = GameAssembly.oil2cpp_unity_liveness_calculation_begin; runASM(); }
	void fil2cpp_unity_liveness_calculation_end() { PA = GameAssembly.oil2cpp_unity_liveness_calculation_end; runASM(); }
	void fil2cpp_unity_liveness_calculation_from_root() { PA = GameAssembly.oil2cpp_unity_liveness_calculation_from_root; runASM(); }
	void fil2cpp_unity_liveness_calculation_from_statics() { PA = GameAssembly.oil2cpp_unity_liveness_calculation_from_statics; runASM(); }
	void fil2cpp_value_box() { PA = GameAssembly.oil2cpp_value_box; runASM(); }
}

void setupFunctions() {
	GameAssembly.oCloseZStream = GetProcAddress(GameAssembly.dll, "CloseZStream");
	GameAssembly.oCreateZStream = GetProcAddress(GameAssembly.dll, "CreateZStream");
	GameAssembly.oDllCanUnloadNow = GetProcAddress(GameAssembly.dll, "DllCanUnloadNow");
	GameAssembly.oDllGetActivationFactory = GetProcAddress(GameAssembly.dll, "DllGetActivationFactory");
	GameAssembly.oFlush = GetProcAddress(GameAssembly.dll, "Flush");
	GameAssembly.oReadZStream = GetProcAddress(GameAssembly.dll, "ReadZStream");
	GameAssembly.oUnityPalGetLocalTimeZoneData = GetProcAddress(GameAssembly.dll, "UnityPalGetLocalTimeZoneData");
	GameAssembly.oUnityPalGetTimeZoneDataForID = GetProcAddress(GameAssembly.dll, "UnityPalGetTimeZoneDataForID");
	GameAssembly.oUnityPalTimeZoneInfoGetTimeZoneIDs = GetProcAddress(GameAssembly.dll, "UnityPalTimeZoneInfoGetTimeZoneIDs");
	GameAssembly.oUseUnityPalForTimeZoneInformation = GetProcAddress(GameAssembly.dll, "UseUnityPalForTimeZoneInformation");
	GameAssembly.oWriteZStream = GetProcAddress(GameAssembly.dll, "WriteZStream");
	GameAssembly.oil2cpp_add_internal_call = GetProcAddress(GameAssembly.dll, "il2cpp_add_internal_call");
	GameAssembly.oil2cpp_alloc = GetProcAddress(GameAssembly.dll, "il2cpp_alloc");
	GameAssembly.oil2cpp_allocation_granularity = GetProcAddress(GameAssembly.dll, "il2cpp_allocation_granularity");
	GameAssembly.oil2cpp_array_class_get = GetProcAddress(GameAssembly.dll, "il2cpp_array_class_get");
	GameAssembly.oil2cpp_array_element_size = GetProcAddress(GameAssembly.dll, "il2cpp_array_element_size");
	GameAssembly.oil2cpp_array_get_byte_length = GetProcAddress(GameAssembly.dll, "il2cpp_array_get_byte_length");
	GameAssembly.oil2cpp_array_length = GetProcAddress(GameAssembly.dll, "il2cpp_array_length");
	GameAssembly.oil2cpp_array_new = GetProcAddress(GameAssembly.dll, "il2cpp_array_new");
	GameAssembly.oil2cpp_array_new_full = GetProcAddress(GameAssembly.dll, "il2cpp_array_new_full");
	GameAssembly.oil2cpp_array_new_specific = GetProcAddress(GameAssembly.dll, "il2cpp_array_new_specific");
	GameAssembly.oil2cpp_array_object_header_size = GetProcAddress(GameAssembly.dll, "il2cpp_array_object_header_size");
	GameAssembly.oil2cpp_assembly_get_image = GetProcAddress(GameAssembly.dll, "il2cpp_assembly_get_image");
	GameAssembly.oil2cpp_bounded_array_class_get = GetProcAddress(GameAssembly.dll, "il2cpp_bounded_array_class_get");
	GameAssembly.oil2cpp_capture_memory_snapshot = GetProcAddress(GameAssembly.dll, "il2cpp_capture_memory_snapshot");
	GameAssembly.oil2cpp_class_array_element_size = GetProcAddress(GameAssembly.dll, "il2cpp_class_array_element_size");
	GameAssembly.oil2cpp_class_enum_basetype = GetProcAddress(GameAssembly.dll, "il2cpp_class_enum_basetype");
	GameAssembly.oil2cpp_class_for_each = GetProcAddress(GameAssembly.dll, "il2cpp_class_for_each");
	GameAssembly.oil2cpp_class_from_il2cpp_type = GetProcAddress(GameAssembly.dll, "il2cpp_class_from_il2cpp_type");
	GameAssembly.oil2cpp_class_from_name = GetProcAddress(GameAssembly.dll, "il2cpp_class_from_name");
	GameAssembly.oil2cpp_class_from_system_type = GetProcAddress(GameAssembly.dll, "il2cpp_class_from_system_type");
	GameAssembly.oil2cpp_class_from_type = GetProcAddress(GameAssembly.dll, "il2cpp_class_from_type");
	GameAssembly.oil2cpp_class_get_assemblyname = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_assemblyname");
	GameAssembly.oil2cpp_class_get_bitmap = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_bitmap");
	GameAssembly.oil2cpp_class_get_bitmap_size = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_bitmap_size");
	GameAssembly.oil2cpp_class_get_data_size = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_data_size");
	GameAssembly.oil2cpp_class_get_declaring_type = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_declaring_type");
	GameAssembly.oil2cpp_class_get_element_class = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_element_class");
	GameAssembly.oil2cpp_class_get_events = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_events");
	GameAssembly.oil2cpp_class_get_field_from_name = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_field_from_name");
	GameAssembly.oil2cpp_class_get_fields = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_fields");
	GameAssembly.oil2cpp_class_get_flags = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_flags");
	GameAssembly.oil2cpp_class_get_image = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_image");
	GameAssembly.oil2cpp_class_get_interfaces = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_interfaces");
	GameAssembly.oil2cpp_class_get_method_from_name = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_method_from_name");
	GameAssembly.oil2cpp_class_get_methods = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_methods");
	GameAssembly.oil2cpp_class_get_name = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_name");
	GameAssembly.oil2cpp_class_get_namespace = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_namespace");
	GameAssembly.oil2cpp_class_get_nested_types = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_nested_types");
	GameAssembly.oil2cpp_class_get_parent = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_parent");
	GameAssembly.oil2cpp_class_get_properties = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_properties");
	GameAssembly.oil2cpp_class_get_property_from_name = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_property_from_name");
	GameAssembly.oil2cpp_class_get_rank = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_rank");
	GameAssembly.oil2cpp_class_get_static_field_data = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_static_field_data");
	GameAssembly.oil2cpp_class_get_type = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_type");
	GameAssembly.oil2cpp_class_get_type_token = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_type_token");
	GameAssembly.oil2cpp_class_get_userdata_offset = GetProcAddress(GameAssembly.dll, "il2cpp_class_get_userdata_offset");
	GameAssembly.oil2cpp_class_has_attribute = GetProcAddress(GameAssembly.dll, "il2cpp_class_has_attribute");
	GameAssembly.oil2cpp_class_has_parent = GetProcAddress(GameAssembly.dll, "il2cpp_class_has_parent");
	GameAssembly.oil2cpp_class_has_references = GetProcAddress(GameAssembly.dll, "il2cpp_class_has_references");
	GameAssembly.oil2cpp_class_instance_size = GetProcAddress(GameAssembly.dll, "il2cpp_class_instance_size");
	GameAssembly.oil2cpp_class_is_abstract = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_abstract");
	GameAssembly.oil2cpp_class_is_assignable_from = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_assignable_from");
	GameAssembly.oil2cpp_class_is_blittable = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_blittable");
	GameAssembly.oil2cpp_class_is_enum = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_enum");
	GameAssembly.oil2cpp_class_is_generic = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_generic");
	GameAssembly.oil2cpp_class_is_inflated = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_inflated");
	GameAssembly.oil2cpp_class_is_interface = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_interface");
	GameAssembly.oil2cpp_class_is_subclass_of = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_subclass_of");
	GameAssembly.oil2cpp_class_is_valuetype = GetProcAddress(GameAssembly.dll, "il2cpp_class_is_valuetype");
	GameAssembly.oil2cpp_class_num_fields = GetProcAddress(GameAssembly.dll, "il2cpp_class_num_fields");
	GameAssembly.oil2cpp_class_set_userdata = GetProcAddress(GameAssembly.dll, "il2cpp_class_set_userdata");
	GameAssembly.oil2cpp_class_value_size = GetProcAddress(GameAssembly.dll, "il2cpp_class_value_size");
	GameAssembly.oil2cpp_current_thread_get_frame_at = GetProcAddress(GameAssembly.dll, "il2cpp_current_thread_get_frame_at");
	GameAssembly.oil2cpp_current_thread_get_stack_depth = GetProcAddress(GameAssembly.dll, "il2cpp_current_thread_get_stack_depth");
	GameAssembly.oil2cpp_current_thread_get_top_frame = GetProcAddress(GameAssembly.dll, "il2cpp_current_thread_get_top_frame");
	GameAssembly.oil2cpp_current_thread_walk_frame_stack = GetProcAddress(GameAssembly.dll, "il2cpp_current_thread_walk_frame_stack");
	GameAssembly.oil2cpp_custom_attrs_construct = GetProcAddress(GameAssembly.dll, "il2cpp_custom_attrs_construct");
	GameAssembly.oil2cpp_custom_attrs_free = GetProcAddress(GameAssembly.dll, "il2cpp_custom_attrs_free");
	GameAssembly.oil2cpp_custom_attrs_from_class = GetProcAddress(GameAssembly.dll, "il2cpp_custom_attrs_from_class");
	GameAssembly.oil2cpp_custom_attrs_from_method = GetProcAddress(GameAssembly.dll, "il2cpp_custom_attrs_from_method");
	GameAssembly.oil2cpp_custom_attrs_get_attr = GetProcAddress(GameAssembly.dll, "il2cpp_custom_attrs_get_attr");
	GameAssembly.oil2cpp_custom_attrs_has_attr = GetProcAddress(GameAssembly.dll, "il2cpp_custom_attrs_has_attr");
	GameAssembly.oil2cpp_debug_get_method_info = GetProcAddress(GameAssembly.dll, "il2cpp_debug_get_method_info");
	GameAssembly.oil2cpp_debugger_set_agent_options = GetProcAddress(GameAssembly.dll, "il2cpp_debugger_set_agent_options");
	GameAssembly.oil2cpp_domain_assembly_open = GetProcAddress(GameAssembly.dll, "il2cpp_domain_assembly_open");
	GameAssembly.oil2cpp_domain_get = GetProcAddress(GameAssembly.dll, "il2cpp_domain_get");
	GameAssembly.oil2cpp_domain_get_assemblies = GetProcAddress(GameAssembly.dll, "il2cpp_domain_get_assemblies");
	GameAssembly.oil2cpp_exception_from_name_msg = GetProcAddress(GameAssembly.dll, "il2cpp_exception_from_name_msg");
	GameAssembly.oil2cpp_field_get_flags = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_flags");
	GameAssembly.oil2cpp_field_get_name = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_name");
	GameAssembly.oil2cpp_field_get_offset = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_offset");
	GameAssembly.oil2cpp_field_get_parent = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_parent");
	GameAssembly.oil2cpp_field_get_type = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_type");
	GameAssembly.oil2cpp_field_get_value = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_value");
	GameAssembly.oil2cpp_field_get_value_object = GetProcAddress(GameAssembly.dll, "il2cpp_field_get_value_object");
	GameAssembly.oil2cpp_field_has_attribute = GetProcAddress(GameAssembly.dll, "il2cpp_field_has_attribute");
	GameAssembly.oil2cpp_field_is_literal = GetProcAddress(GameAssembly.dll, "il2cpp_field_is_literal");
	GameAssembly.oil2cpp_field_set_value = GetProcAddress(GameAssembly.dll, "il2cpp_field_set_value");
	GameAssembly.oil2cpp_field_set_value_object = GetProcAddress(GameAssembly.dll, "il2cpp_field_set_value_object");
	GameAssembly.oil2cpp_field_static_get_value = GetProcAddress(GameAssembly.dll, "il2cpp_field_static_get_value");
	GameAssembly.oil2cpp_field_static_set_value = GetProcAddress(GameAssembly.dll, "il2cpp_field_static_set_value");
	GameAssembly.oil2cpp_format_exception = GetProcAddress(GameAssembly.dll, "il2cpp_format_exception");
	GameAssembly.oil2cpp_format_stack_trace = GetProcAddress(GameAssembly.dll, "il2cpp_format_stack_trace");
	GameAssembly.oil2cpp_free = GetProcAddress(GameAssembly.dll, "il2cpp_free");
	GameAssembly.oil2cpp_free_captured_memory_snapshot = GetProcAddress(GameAssembly.dll, "il2cpp_free_captured_memory_snapshot");
	GameAssembly.oil2cpp_gc_collect = GetProcAddress(GameAssembly.dll, "il2cpp_gc_collect");
	GameAssembly.oil2cpp_gc_collect_a_little = GetProcAddress(GameAssembly.dll, "il2cpp_gc_collect_a_little");
	GameAssembly.oil2cpp_gc_disable = GetProcAddress(GameAssembly.dll, "il2cpp_gc_disable");
	GameAssembly.oil2cpp_gc_enable = GetProcAddress(GameAssembly.dll, "il2cpp_gc_enable");
	GameAssembly.oil2cpp_gc_foreach_heap = GetProcAddress(GameAssembly.dll, "il2cpp_gc_foreach_heap");
	GameAssembly.oil2cpp_gc_get_heap_size = GetProcAddress(GameAssembly.dll, "il2cpp_gc_get_heap_size");
	GameAssembly.oil2cpp_gc_get_max_time_slice_ns = GetProcAddress(GameAssembly.dll, "il2cpp_gc_get_max_time_slice_ns");
	GameAssembly.oil2cpp_gc_get_used_size = GetProcAddress(GameAssembly.dll, "il2cpp_gc_get_used_size");
	GameAssembly.oil2cpp_gc_has_strict_wbarriers = GetProcAddress(GameAssembly.dll, "il2cpp_gc_has_strict_wbarriers");
	GameAssembly.oil2cpp_gc_is_disabled = GetProcAddress(GameAssembly.dll, "il2cpp_gc_is_disabled");
	GameAssembly.oil2cpp_gc_is_incremental = GetProcAddress(GameAssembly.dll, "il2cpp_gc_is_incremental");
	GameAssembly.oil2cpp_gc_set_external_allocation_tracker = GetProcAddress(GameAssembly.dll, "il2cpp_gc_set_external_allocation_tracker");
	GameAssembly.oil2cpp_gc_set_external_wbarrier_tracker = GetProcAddress(GameAssembly.dll, "il2cpp_gc_set_external_wbarrier_tracker");
	GameAssembly.oil2cpp_gc_set_max_time_slice_ns = GetProcAddress(GameAssembly.dll, "il2cpp_gc_set_max_time_slice_ns");
	GameAssembly.oil2cpp_gc_set_mode = GetProcAddress(GameAssembly.dll, "il2cpp_gc_set_mode");
	GameAssembly.oil2cpp_gc_start_incremental_collection = GetProcAddress(GameAssembly.dll, "il2cpp_gc_start_incremental_collection");
	GameAssembly.oil2cpp_gc_wbarrier_set_field = GetProcAddress(GameAssembly.dll, "il2cpp_gc_wbarrier_set_field");
	GameAssembly.oil2cpp_gchandle_foreach_get_target = GetProcAddress(GameAssembly.dll, "il2cpp_gchandle_foreach_get_target");
	GameAssembly.oil2cpp_gchandle_free = GetProcAddress(GameAssembly.dll, "il2cpp_gchandle_free");
	GameAssembly.oil2cpp_gchandle_get_target = GetProcAddress(GameAssembly.dll, "il2cpp_gchandle_get_target");
	GameAssembly.oil2cpp_gchandle_new = GetProcAddress(GameAssembly.dll, "il2cpp_gchandle_new");
	GameAssembly.oil2cpp_gchandle_new_weakref = GetProcAddress(GameAssembly.dll, "il2cpp_gchandle_new_weakref");
	GameAssembly.oil2cpp_get_corlib = GetProcAddress(GameAssembly.dll, "il2cpp_get_corlib");
	GameAssembly.oil2cpp_get_exception_argument_null = GetProcAddress(GameAssembly.dll, "il2cpp_get_exception_argument_null");
	GameAssembly.oil2cpp_image_get_assembly = GetProcAddress(GameAssembly.dll, "il2cpp_image_get_assembly");
	GameAssembly.oil2cpp_image_get_class = GetProcAddress(GameAssembly.dll, "il2cpp_image_get_class");
	GameAssembly.oil2cpp_image_get_class_count = GetProcAddress(GameAssembly.dll, "il2cpp_image_get_class_count");
	GameAssembly.oil2cpp_image_get_entry_point = GetProcAddress(GameAssembly.dll, "il2cpp_image_get_entry_point");
	GameAssembly.oil2cpp_image_get_filename = GetProcAddress(GameAssembly.dll, "il2cpp_image_get_filename");
	GameAssembly.oil2cpp_image_get_name = GetProcAddress(GameAssembly.dll, "il2cpp_image_get_name");
	GameAssembly.oil2cpp_init = GetProcAddress(GameAssembly.dll, "il2cpp_init");
	GameAssembly.oil2cpp_init_utf16 = GetProcAddress(GameAssembly.dll, "il2cpp_init_utf16");
	GameAssembly.oil2cpp_is_debugger_attached = GetProcAddress(GameAssembly.dll, "il2cpp_is_debugger_attached");
	GameAssembly.oil2cpp_is_vm_thread = GetProcAddress(GameAssembly.dll, "il2cpp_is_vm_thread");
	GameAssembly.oil2cpp_method_get_class = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_class");
	GameAssembly.oil2cpp_method_get_declaring_type = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_declaring_type");
	GameAssembly.oil2cpp_method_get_flags = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_flags");
	GameAssembly.oil2cpp_method_get_from_reflection = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_from_reflection");
	GameAssembly.oil2cpp_method_get_name = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_name");
	GameAssembly.oil2cpp_method_get_object = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_object");
	GameAssembly.oil2cpp_method_get_param = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_param");
	GameAssembly.oil2cpp_method_get_param_count = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_param_count");
	GameAssembly.oil2cpp_method_get_param_name = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_param_name");
	GameAssembly.oil2cpp_method_get_return_type = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_return_type");
	GameAssembly.oil2cpp_method_get_token = GetProcAddress(GameAssembly.dll, "il2cpp_method_get_token");
	GameAssembly.oil2cpp_method_has_attribute = GetProcAddress(GameAssembly.dll, "il2cpp_method_has_attribute");
	GameAssembly.oil2cpp_method_is_generic = GetProcAddress(GameAssembly.dll, "il2cpp_method_is_generic");
	GameAssembly.oil2cpp_method_is_inflated = GetProcAddress(GameAssembly.dll, "il2cpp_method_is_inflated");
	GameAssembly.oil2cpp_method_is_instance = GetProcAddress(GameAssembly.dll, "il2cpp_method_is_instance");
	GameAssembly.oil2cpp_monitor_enter = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_enter");
	GameAssembly.oil2cpp_monitor_exit = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_exit");
	GameAssembly.oil2cpp_monitor_pulse = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_pulse");
	GameAssembly.oil2cpp_monitor_pulse_all = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_pulse_all");
	GameAssembly.oil2cpp_monitor_try_enter = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_try_enter");
	GameAssembly.oil2cpp_monitor_try_wait = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_try_wait");
	GameAssembly.oil2cpp_monitor_wait = GetProcAddress(GameAssembly.dll, "il2cpp_monitor_wait");
	GameAssembly.oil2cpp_native_stack_trace = GetProcAddress(GameAssembly.dll, "il2cpp_native_stack_trace");
	GameAssembly.oil2cpp_object_get_class = GetProcAddress(GameAssembly.dll, "il2cpp_object_get_class");
	GameAssembly.oil2cpp_object_get_size = GetProcAddress(GameAssembly.dll, "il2cpp_object_get_size");
	GameAssembly.oil2cpp_object_get_virtual_method = GetProcAddress(GameAssembly.dll, "il2cpp_object_get_virtual_method");
	GameAssembly.oil2cpp_object_header_size = GetProcAddress(GameAssembly.dll, "il2cpp_object_header_size");
	GameAssembly.oil2cpp_object_new = GetProcAddress(GameAssembly.dll, "il2cpp_object_new");
	GameAssembly.oil2cpp_object_unbox = GetProcAddress(GameAssembly.dll, "il2cpp_object_unbox");
	GameAssembly.oil2cpp_offset_of_array_bounds_in_array_object_header = GetProcAddress(GameAssembly.dll, "il2cpp_offset_of_array_bounds_in_array_object_header");
	GameAssembly.oil2cpp_offset_of_array_length_in_array_object_header = GetProcAddress(GameAssembly.dll, "il2cpp_offset_of_array_length_in_array_object_header");
	GameAssembly.oil2cpp_override_stack_backtrace = GetProcAddress(GameAssembly.dll, "il2cpp_override_stack_backtrace");
	GameAssembly.oil2cpp_profiler_install = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_install");
	GameAssembly.oil2cpp_profiler_install_allocation = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_install_allocation");
	GameAssembly.oil2cpp_profiler_install_enter_leave = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_install_enter_leave");
	GameAssembly.oil2cpp_profiler_install_fileio = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_install_fileio");
	GameAssembly.oil2cpp_profiler_install_gc = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_install_gc");
	GameAssembly.oil2cpp_profiler_install_thread = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_install_thread");
	GameAssembly.oil2cpp_profiler_set_events = GetProcAddress(GameAssembly.dll, "il2cpp_profiler_set_events");
	GameAssembly.oil2cpp_property_get_flags = GetProcAddress(GameAssembly.dll, "il2cpp_property_get_flags");
	GameAssembly.oil2cpp_property_get_get_method = GetProcAddress(GameAssembly.dll, "il2cpp_property_get_get_method");
	GameAssembly.oil2cpp_property_get_name = GetProcAddress(GameAssembly.dll, "il2cpp_property_get_name");
	GameAssembly.oil2cpp_property_get_parent = GetProcAddress(GameAssembly.dll, "il2cpp_property_get_parent");
	GameAssembly.oil2cpp_property_get_set_method = GetProcAddress(GameAssembly.dll, "il2cpp_property_get_set_method");
	GameAssembly.oil2cpp_raise_exception = GetProcAddress(GameAssembly.dll, "il2cpp_raise_exception");
	GameAssembly.oil2cpp_register_debugger_agent_transport = GetProcAddress(GameAssembly.dll, "il2cpp_register_debugger_agent_transport");
	GameAssembly.oil2cpp_register_log_callback = GetProcAddress(GameAssembly.dll, "il2cpp_register_log_callback");
	GameAssembly.oil2cpp_resolve_icall = GetProcAddress(GameAssembly.dll, "il2cpp_resolve_icall");
	GameAssembly.oil2cpp_runtime_class_init = GetProcAddress(GameAssembly.dll, "il2cpp_runtime_class_init");
	GameAssembly.oil2cpp_runtime_invoke = GetProcAddress(GameAssembly.dll, "il2cpp_runtime_invoke");
	GameAssembly.oil2cpp_runtime_invoke_convert_args = GetProcAddress(GameAssembly.dll, "il2cpp_runtime_invoke_convert_args");
	GameAssembly.oil2cpp_runtime_object_init = GetProcAddress(GameAssembly.dll, "il2cpp_runtime_object_init");
	GameAssembly.oil2cpp_runtime_object_init_exception = GetProcAddress(GameAssembly.dll, "il2cpp_runtime_object_init_exception");
	GameAssembly.oil2cpp_runtime_unhandled_exception_policy_set = GetProcAddress(GameAssembly.dll, "il2cpp_runtime_unhandled_exception_policy_set");
	GameAssembly.oil2cpp_set_commandline_arguments = GetProcAddress(GameAssembly.dll, "il2cpp_set_commandline_arguments");
	GameAssembly.oil2cpp_set_commandline_arguments_utf16 = GetProcAddress(GameAssembly.dll, "il2cpp_set_commandline_arguments_utf16");
	GameAssembly.oil2cpp_set_config = GetProcAddress(GameAssembly.dll, "il2cpp_set_config");
	GameAssembly.oil2cpp_set_config_dir = GetProcAddress(GameAssembly.dll, "il2cpp_set_config_dir");
	GameAssembly.oil2cpp_set_config_utf16 = GetProcAddress(GameAssembly.dll, "il2cpp_set_config_utf16");
	GameAssembly.oil2cpp_set_data_dir = GetProcAddress(GameAssembly.dll, "il2cpp_set_data_dir");
	GameAssembly.oil2cpp_set_default_thread_affinity = GetProcAddress(GameAssembly.dll, "il2cpp_set_default_thread_affinity");
	GameAssembly.oil2cpp_set_find_plugin_callback = GetProcAddress(GameAssembly.dll, "il2cpp_set_find_plugin_callback");
	GameAssembly.oil2cpp_set_memory_callbacks = GetProcAddress(GameAssembly.dll, "il2cpp_set_memory_callbacks");
	GameAssembly.oil2cpp_set_temp_dir = GetProcAddress(GameAssembly.dll, "il2cpp_set_temp_dir");
	GameAssembly.oil2cpp_shutdown = GetProcAddress(GameAssembly.dll, "il2cpp_shutdown");
	GameAssembly.oil2cpp_start_gc_world = GetProcAddress(GameAssembly.dll, "il2cpp_start_gc_world");
	GameAssembly.oil2cpp_stats_dump_to_file = GetProcAddress(GameAssembly.dll, "il2cpp_stats_dump_to_file");
	GameAssembly.oil2cpp_stats_get_value = GetProcAddress(GameAssembly.dll, "il2cpp_stats_get_value");
	GameAssembly.oil2cpp_stop_gc_world = GetProcAddress(GameAssembly.dll, "il2cpp_stop_gc_world");
	GameAssembly.oil2cpp_string_chars = GetProcAddress(GameAssembly.dll, "il2cpp_string_chars");
	GameAssembly.oil2cpp_string_intern = GetProcAddress(GameAssembly.dll, "il2cpp_string_intern");
	GameAssembly.oil2cpp_string_is_interned = GetProcAddress(GameAssembly.dll, "il2cpp_string_is_interned");
	GameAssembly.oil2cpp_string_length = GetProcAddress(GameAssembly.dll, "il2cpp_string_length");
	GameAssembly.oil2cpp_string_new = GetProcAddress(GameAssembly.dll, "il2cpp_string_new");
	GameAssembly.oil2cpp_string_new_len = GetProcAddress(GameAssembly.dll, "il2cpp_string_new_len");
	GameAssembly.oil2cpp_string_new_utf16 = GetProcAddress(GameAssembly.dll, "il2cpp_string_new_utf16");
	GameAssembly.oil2cpp_string_new_wrapper = GetProcAddress(GameAssembly.dll, "il2cpp_string_new_wrapper");
	GameAssembly.oil2cpp_thread_attach = GetProcAddress(GameAssembly.dll, "il2cpp_thread_attach");
	GameAssembly.oil2cpp_thread_current = GetProcAddress(GameAssembly.dll, "il2cpp_thread_current");
	GameAssembly.oil2cpp_thread_detach = GetProcAddress(GameAssembly.dll, "il2cpp_thread_detach");
	GameAssembly.oil2cpp_thread_get_all_attached_threads = GetProcAddress(GameAssembly.dll, "il2cpp_thread_get_all_attached_threads");
	GameAssembly.oil2cpp_thread_get_frame_at = GetProcAddress(GameAssembly.dll, "il2cpp_thread_get_frame_at");
	GameAssembly.oil2cpp_thread_get_stack_depth = GetProcAddress(GameAssembly.dll, "il2cpp_thread_get_stack_depth");
	GameAssembly.oil2cpp_thread_get_top_frame = GetProcAddress(GameAssembly.dll, "il2cpp_thread_get_top_frame");
	GameAssembly.oil2cpp_thread_walk_frame_stack = GetProcAddress(GameAssembly.dll, "il2cpp_thread_walk_frame_stack");
	GameAssembly.oil2cpp_type_equals = GetProcAddress(GameAssembly.dll, "il2cpp_type_equals");
	GameAssembly.oil2cpp_type_get_assembly_qualified_name = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_assembly_qualified_name");
	GameAssembly.oil2cpp_type_get_attrs = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_attrs");
	GameAssembly.oil2cpp_type_get_class_or_element_class = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_class_or_element_class");
	GameAssembly.oil2cpp_type_get_name = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_name");
	GameAssembly.oil2cpp_type_get_name_chunked = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_name_chunked");
	GameAssembly.oil2cpp_type_get_object = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_object");
	GameAssembly.oil2cpp_type_get_type = GetProcAddress(GameAssembly.dll, "il2cpp_type_get_type");
	GameAssembly.oil2cpp_type_is_byref = GetProcAddress(GameAssembly.dll, "il2cpp_type_is_byref");
	GameAssembly.oil2cpp_type_is_pointer_type = GetProcAddress(GameAssembly.dll, "il2cpp_type_is_pointer_type");
	GameAssembly.oil2cpp_type_is_static = GetProcAddress(GameAssembly.dll, "il2cpp_type_is_static");
	GameAssembly.oil2cpp_unhandled_exception = GetProcAddress(GameAssembly.dll, "il2cpp_unhandled_exception");
	GameAssembly.oil2cpp_unity_install_unitytls_interface = GetProcAddress(GameAssembly.dll, "il2cpp_unity_install_unitytls_interface");
	GameAssembly.oil2cpp_unity_liveness_calculation_begin = GetProcAddress(GameAssembly.dll, "il2cpp_unity_liveness_calculation_begin");
	GameAssembly.oil2cpp_unity_liveness_calculation_end = GetProcAddress(GameAssembly.dll, "il2cpp_unity_liveness_calculation_end");
	GameAssembly.oil2cpp_unity_liveness_calculation_from_root = GetProcAddress(GameAssembly.dll, "il2cpp_unity_liveness_calculation_from_root");
	GameAssembly.oil2cpp_unity_liveness_calculation_from_statics = GetProcAddress(GameAssembly.dll, "il2cpp_unity_liveness_calculation_from_statics");
	GameAssembly.oil2cpp_value_box = GetProcAddress(GameAssembly.dll, "il2cpp_value_box");
}
#pragma endregion


std::uintptr_t load_metadata_address = 0x00142340;

/* Xor Key Table */
std::uint8_t key_table[] =
{
	0x38, 0x25, 0xAB, 0x73,
	0x3A, 0x2D, 0x9B, 0x6B,
	0x3C, 0x35, 0x8B, 0x63,
	0x3E, 0x3D, 0x7B, 0x5B,
	0x40, 0x45, 0x6B, 0x53,
	0x42, 0x4D, 0x5B, 0x4B,
	0xDC, 0x55, 0x4B, 0x43,
	0xDE, 0x5D, 0x3B, 0x3B,
};

void xor_encrypt(std::uint8_t* data, std::size_t size)
{
	for (std::size_t i = 0; i < size; i++)
	{
		data[i] ^= key_table[i % sizeof(key_table)];
	}
}

/* constexpr fnv1a hashing */
constexpr std::uint64_t fnv1a_64(const char* str, std::size_t count) {
	std::uint64_t hash = 0xcbf29ce484222325;
	for (std::size_t i = 0; i < count; ++i) {
		hash ^= str[i];
		hash *= 0x100000001b3;
	}
	return hash;
}

std::size_t get_length_of_metadata(std::uint8_t* data)
{
	std::size_t size = 0;
	/* End signature: `21 43 6B 53 21 4B 5B 4B BE 53 4B 43 D8 50 3B 3B` (hex) */
	while (data[size] != 0x21 || data[size + 1] != 0x43 || data[size + 2] != 0x6B || data[size + 3] != 0x53 ||
		data[size + 4] != 0x21 || data[size + 5] != 0x4B || data[size + 6] != 0x5B || data[size + 7] != 0x4B ||
		data[size + 8] != 0xBE || data[size + 9] != 0x53 || data[size + 10] != 0x4B || data[size + 11] != 0x43 ||
		data[size + 12] != 0xD8 || data[size + 13] != 0x50 || data[size + 14] != 0x3B || data[size + 15] != 0x3B)
	{
		size++;
	}
	return size + 16;
}

PVOID original_load_metadata = nullptr;
/* Our custom callback for metadata loading */
std::uint8_t* __fastcall hook_load_metadata(const char* filename)
{
	/* Ensure filename is `global-metadata.dat` */
	if (fnv1a_64(filename, strlen(filename)) == fnv1a_64("global-metadata.dat", 19))
	{
		/* Call original */
		std::uint8_t* result = PLH::FnCast(original_load_metadata, hook_load_metadata)(filename);
		/* Get size */
		std::size_t size = get_length_of_metadata(result);
		/* Allocate new memory for copy */
		std::uint8_t* new_result = (std::uint8_t*)VirtualAlloc(nullptr, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		/* Copy data */
		memcpy(new_result, result, size);
		/* Decrypt */
		xor_encrypt((std::uint8_t*)new_result, size);

		/* Return now decrypted metadata */
		return new_result;
	}

	/* Call original */
	return PLH::FnCast(original_load_metadata, hook_load_metadata)(filename);
}

std::uintptr_t game_assembly = NULL;

bool initialize_metadata_decryption_routine(  )
{
	PLH::CapstoneDisassembler dis(PLH::Mode::x64);
	PLH::x64Detour detour((uint64_t)(game_assembly + load_metadata_address), (uint64_t)hook_load_metadata, (uint64_t*)&original_load_metadata, dis);
	detour.hook();

	/* Busy wait to keep thread alive */
	while (true)
	{
		Sleep(1000);
	}
}


BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
		char path[MAX_PATH];
		GetCurrentDirectory(sizeof(path), path);

		// Example: "\\System32\\version.dll"
		strcat_s(path, "\\msvcbin.dll");
		GameAssembly.dll = LoadLibrary(path);
		game_assembly = (std::uintptr_t)GameAssembly.dll;
		setupFunctions();
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)initialize_metadata_decryption_routine, NULL, NULL, NULL);
		break;
	case DLL_PROCESS_DETACH:
		FreeLibrary(GameAssembly.dll);
		break;
	}
	return 1;
}
