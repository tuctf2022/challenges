import struct
import os
import time
import random
import binascii
import re
import string
import argparse

function_start_str = "# FUNCTION START #"
function_end_str = "# FUNCTION END #"

STRING_ENCRYPTION_KEY = 0xB16B00B5

# Global list of all the registers
registers = [ "$v0", "$v1", "$a0", "$a1", "$a2", "$a3", "$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7", "$t8", "$t9", "$k0", "$k1" ]
# List of temporary registers
temp_registers = [ "$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9" ]

# List of instructions to use in random generation
mips_instructions = [
    "add REG, REG, REG",
    "addi REG, REG, NUM",
    "addiu REG, REG, NUM",
    "addu REG, REG, REG",
    "and REG, REG, REG",
    "andi REG, REG, NUM",
    "beq REG, REG, LABEL",
    "bne REG, REG, LABEL",
    "j LABEL",
    "jal LABEL",
    "jr REG",
    "lb REG, NUM(REG)",
    "lbu REG, NUM(REG)",
    "lh REG, NUM(REG)",
    "lhu REG, NUM(REG)",
    "lui REG, NUM",
    "li, REG, NUM",
    "lw REG, NUM(REG)",
    "lwl REG, NUM(REG)",
    "lwr REG, NUM(REG)",
    "nor REG, REG, REG",
    "or REG, REG, REG",
    "ori REG, REG, NUM",
    "sb REG, NUM(REG)",
    "sh REG, NUM(REG)",
    "sll REG, REG, NUM",
    "sllv REG, REG, REG",
    "slt REG, REG, REG",
    "slti REG, REG, NUM",
    "sltiu REG, REG, NUM",
    "sltu REG, REG, REG",
    "sra REG, REG, NUM",
    "srav REG, REG, REG",
    "srl REG, REG, NUM",
    "srlv REG, REG, REG",
    "sub REG, REG, REG",
    "subu REG, REG, REG",
    "sw REG, NUM(REG)",
    "swl REG, NUM(REG)",
    "swr REG, NUM(REG)",
    "syscall",
    "xor REG, REG, REG",
    "xori REG, REG, NUM",
]

# Mips utilities

# Generate random string func
def generate_random_ascii_string( length ):
    return ''.join( random.choice( string.ascii_uppercase + string.ascii_lowercase ) for _ in range( length ) )

def infix_to_postfix( expression ):
    # Shunting-yard algorithm
    # https://en.wikipedia.org/wiki/Shunting-yard_algorithm
    operators = {
        "+": 1,
        "-": 1,
        "*": 2,
        "/": 2,
        "(": 0,
        ")": 0
    }
    output = []
    stack = []
    for token in expression:
        if token in operators:
            if token == "(":
                stack.append( token )
            elif token == ")":
                while stack[-1] != "(":
                    output.append( stack.pop( ) )
                stack.pop( )
            elif len( stack ) == 0:
                stack.append( token )
            else:
                while len( stack ) != 0 and operators[stack[-1]] >= operators[token]:
                    output.append( stack.pop( ) )
                stack.append( token )
        else:
            output.append( token )
    while len( stack ) != 0:
        output.append( stack.pop() )
    return output

def postfix_to_mips( postfix_expression, destination_register ):
    # Our resultant mips code
    mips_code = ""
    # Strip ',' from the destination register
    destination_register = destination_register.replace( ",", "" )
    # Generate 2 random registers that are not the same or the destination register
    random_registers = random.sample( temp_registers, 2 )
    while random_registers[0] == random_registers[1] or random_registers[0] == destination_register or random_registers[1] == destination_register:
        random_registers = random.sample( temp_registers, 2 )
    # Push random registers onto stack
    mips_code += "addi $sp, $sp, -4\n"
    mips_code += "sw " + random_registers[0] + ", 0($sp)\n"
    mips_code += "addi $sp, $sp, -4 \n"
    mips_code += "sw " + random_registers[1] + ", 0($sp)\n"
    # Set both temporary registers to 0 and push them onto the stack
    mips_code += "addi " + random_registers[0] + ", $zero, 0\n"
    mips_code += "addi " + random_registers[1] + ", $zero, 0\n"
    # Push onto stack
    mips_code += "addi $sp, $sp, -4\n"
    mips_code += "sw " + random_registers[0] + ", 0($sp)\n"
    mips_code += "addi $sp, $sp, -4\n"
    mips_code += "sw " + random_registers[1] + ", 0($sp)\n"
    # Build the mips code from the postfix expression
    for token in postfix_expression:
        if token in [ "+", "-", "*", "/" ]:
            # Pop 2 registers off the stack
            mips_code += "lw " + random_registers[0] + ", 0($sp)\n"
            mips_code += "addi $sp, $sp, 4 \n"
            mips_code += "lw " + random_registers[1] + ", 0($sp)\n"
            mips_code += "addi $sp, $sp, 4 \n"
            # Perform the operation
            if token == "+":
                mips_code += "addu " + random_registers[1] + ", " + random_registers[1] + ", " + random_registers[0] + "\n"
            elif token == "-":
                mips_code += "subu " + random_registers[1] + ", " + random_registers[1] + ", " + random_registers[0] + "\n"
            elif token == "*":
                mips_code += "mult " + random_registers[1] + ", " + random_registers[0] + "\n"
                mips_code += "mflo " + random_registers[1] + "\n"
            elif token == "/":
                mips_code += "div " + random_registers[1] + ", " + random_registers[0] + "\n"
                mips_code += "mflo " + random_registers[1] + "\n"
            # Push the result back onto the stack
            mips_code += "addi $sp, $sp, -4 \n"
            mips_code += "sw " + random_registers[1] + ", 0($sp)\n"
        else:
            # Push the token onto the stack
            mips_code += "addi $sp, $sp, -4 \n"
            mips_code += "li " + random_registers[0] + ", " + hex( int( token ) ) + "\n"
            mips_code += "sw " + random_registers[0] + ", 0($sp)\n"
    # Pop the result off the stack
    mips_code += "lw " + destination_register + ", 0($sp)\n"
    mips_code += "addi $sp, $sp, 4\n"
    # Add 8 bytes to the stack pointer to remove the 2 temporary registers
    mips_code += "addi $sp, $sp, 8\n"
    # Pop the random registers off the stack
    mips_code += "lw " + random_registers[1] + ", 0($sp)\n"
    mips_code += "addi $sp, $sp, 4\n"
    mips_code += "lw " + random_registers[0] + ", 0($sp)\n"
    mips_code += "addi $sp, $sp, 4\n"
    return mips_code


# Math expression to MIPS code
def math_to_mips( math_expression, destination_register ):
    # Remove all whitespace
    math_expression = math_expression.replace( " ", "" )
    # Split the math expression into tokens
    tokens = re.split( "(\+|\-|\*|\/|\(|\))", math_expression )
    # Remove empty strings
    tokens = list( filter( None, tokens ) )
    # Convert the tokens to postfix
    postfix = infix_to_postfix( tokens )
    # Convert postfix to mips
    mips = postfix_to_mips( postfix, destination_register )
    return mips

def generate_random_instructions( num_instructions, num_registers ):
    # num_registers must be at least 3
    if num_registers < 3:
        num_registers = 3
    # Select random registers of the correct size
    random_registers = random.sample( registers, num_registers )
    # Generate n random instructions
    instructions = []
    for i in range( 0, num_instructions ):
        # Get random instruction
        instruction = random.choice( mips_instructions )
        # Replace every instance of REG with a random register
        for register in random_registers:
            instruction = instruction.replace( "REG", register, 1 )
        # Replace NUM with random number
        instruction = instruction.replace( "NUM", str( random.randint( 0, 31 ) ) )
        # If there is a label
        if "LABEL" in instruction:
            # Generate a random string of length 4-12
            label = generate_random_ascii_string( random.randint( 4, 12 ) )
            # Replace LABEL with the label
            instruction = instruction.replace( "LABEL", label )
            # Append a ':' to the end of the label
            label += ":\n"
            # place the label randomly into the list of instructions
            instructions.insert( random.randint( 0, len( instructions ) ), label )
        # Add newline to instruction
        instruction += "\n"
        # Add instruction to instructions
        instructions.append( instruction )
        # Randomize the order of registers
        random.shuffle( random_registers )
    # Add a return instruction
    instructions.append( "jr $ra\n" )
    return instructions

# MBA obfuscator

def obfuscate_sub( instruction, depth = 3 ):
    # Our return string
    return_string = ""
    # If the instruction does not contain 'sub'
    if "sub" not in instruction:
        return instruction
        
    # Split the instruction into its parts
    instruction_parts = instruction.split( " " )
    # Get the destination register
    destination_register = instruction_parts[1].strip( "," )
    # Get the first source register, strip the comma
    source_register_1 = instruction_parts[2].strip( "," )
    # Get the second source register, strip the comma
    source_register_2 = instruction_parts[3].strip( "," )

    # If the depth is 0, just do a normal sub
    if depth == 0:
        # If the second source register is a number, use subi
        if source_register_2.isdigit():
            return_string += "subi " + destination_register + ", " + source_register_1 + ", -" + source_register_2 + "\n"
        # Otherwise, use sub
        else:
            return_string += "sub " + destination_register + ", " + source_register_1 + ", " + source_register_2 + "\n"
        return return_string

    # Operation 1: x - y -> x + ~y + 1
    # Operation 2: x - y -> ( x & ~y ) - ( ~x & y )

    # Get a random number between 1 and 2
    operation = random.randint( 1, 2 )

     # Push $t0 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t0, 0($sp)\n"
    # Push $t1 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t1, 0($sp)\n"
    # Store the first source register in $t0
    # If the register is a number use addi, otherwise use add
    if source_register_1.isdigit():
        return_string += "addi $t0, $zero, " + source_register_1 + "\n"
    else:
        return_string += "add $t0, $zero, " + source_register_1 + "\n"
    # Store the second source register in $t1
    # If the register is a number use addi, otherwise use add
    if source_register_2.isdigit():
        return_string += "addi $t1, $zero, " + source_register_2 + "\n"
    else:
        return_string += "add $t1, $zero, " + source_register_2 + "\n"

    # If the operation is 1
    if operation == 1:
        # Perform the operation
        return_string += "nor $t1, $t1, $zero\n"
        add_instruciton = "addi $t1, $t1, 1\n"
        return_string += obfuscate_add( add_instruciton, depth - 1 )
        add_operation = "add " + destination_register + ", $t0, $t1\n"
        return_string += obfuscate_add( add_operation, depth - 1 )
    # If the operation is 2
    elif operation == 2:
        # Perform the operation
        return_string += "nor $t0, $t0, $zero\n"
        return_string += "nor $t1, $t1, $zero\n"
        and_operation = "and $t0, $t0, $t1\n"
        return_string += obfuscate_and( and_operation, depth - 1 )
        and_operation = "and $t1, $t1, $t0\n"
        return_string += obfuscate_and( and_operation, depth - 1 )
        sub_operation = "sub " + destination_register + ", $t0, $t1\n"
        return_string += obfuscate_sub( sub_operation, depth - 1 )

    # Pop $t1 from the stack
    # If destination register is $t1, don't load it
    if destination_register != "$t1":
        return_string += "lw $t1, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # Pop $t0 from the stack
    # If destination register is $t0, don't load it
    if destination_register != "$t0":
        return_string += "lw $t0, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"

    return return_string

def obfuscate_and( instruction, depth = 3 ):
    # Our return string
    return_string = ""
    # If the instruction does not contain 'and'
    if "and" not in instruction:
        return instruction

    # Add debug information
    return_string += "# AND OBFUSCATION START: " + instruction + "\n"
        
    # Split the instruction into its parts
    instruction_parts = instruction.split( " " )
    # Get the destination register
    destination_register = instruction_parts[1].strip( "," )
    # Get the first source register, strip the comma
    source_register_1 = instruction_parts[2].strip( "," )
    # Get the second source register, strip the comma
    source_register_2 = instruction_parts[3].strip( "," )

    # If the depth is 0, just do a normal and
    if depth == 0:
        return_string += "and " + destination_register + ", " + source_register_1 + ", " + source_register_2 + "\n"
        return return_string

    # Operation 1: x & y -> ~( ~x | ~y )
    # Operation 2: x & y -> ( ~x | y ) - ~x

    # Generate a random number between for the operation
    operation = random.randint( 1, 2 )
    # Push $t0 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t0, 0($sp)\n"
    # Push $t1 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t1, 0($sp)\n"
    # Store the first source register in $t0
    # If the register is a number use addi, otherwise use add
    if source_register_1.isdigit():
        return_string += "addi $t0, $zero, " + source_register_1 + "\n"
    else:
        return_string += "add $t0, $zero, " + source_register_1 + "\n"
    # Store the second source register in $t1
    # If the register is a number use addi, otherwise use add
    if source_register_2.isdigit():
        return_string += "addi $t1, $zero, " + source_register_2 + "\n"
    else:
        return_string += "add $t1, $zero, " + source_register_2 + "\n"
    # If the operation is 1
    if operation == 1:
        # NOT $t0, store result in $t0
        return_string += "nor $t0, $t0, $t0\n"
        # NOT $t1, store result in $t1
        return_string += "nor $t1, $t1, $t1\n"
        # OR $t0 and $t1, store result in destination register
        return_string += "or " + destination_register + ", $t0, $t1\n"
        # NOT destination register, store result in destination register
        return_string += "nor " + destination_register + ", " + destination_register + ", " + destination_register + "\n"
    # Otherwise, the operation is 2
    else:
        # NOT $t0, store result in destination register
        return_string += "nor " + destination_register + ", $t0, $t0\n"
        # OR $t0 and $t1, store result in destination register
        return_string += "or " + destination_register + ", " + destination_register + ", $t1\n"
        # NOT $t0, store result in $t0
        return_string += "nor $t0, $t0, $t0\n"
        # SUB destination register and $t0, store result in destination register
        return_string += "sub " + destination_register + ", " + destination_register + ", $t0\n"
    # Pop $t1 from the stack
    # If destination register is $t1, don't load it
    if destination_register != "$t1":
        return_string += "lw $t1, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # Pop $t0 from the stack
    # If destination register is $t0, don't load it
    if destination_register != "$t0":
        return_string += "lw $t0, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # Debug information
    return_string += "# AND OBFUSCATION END: " + instruction + "\n"

    # Return the string
    return return_string

def obfuscate_or( instruction, depth = 3 ):
    # Our return string
    return_string = ""
    # If the instruction does not contain 'or'
    if "or" not in instruction:
        return instruction
        
    # Add debug string to the return string
    return_string += "# START OR: " + instruction + "\n"
    # Split the instruction into its parts
    instruction_parts = instruction.split( " " )
    # Get the destination register
    destination_register = instruction_parts[1].strip( "," )
    # Get the first source register, strip the comma
    source_register_1 = instruction_parts[2].strip( "," )
    # Get the second source register, strip the comma
    source_register_2 = instruction_parts[3].strip( "," )
    # If the depth is 0, just do a normal or
    if depth == 0:
        return_string += "or " + destination_register + ", " + source_register_1 + ", " + source_register_2 + "\n"
        return return_string

    # Operation 1: x | y -> ( x & y ) + ( x ^ y )
    # Operation 2: x | y -> ( x & ~y ) + ( ~x & y )
    
    # Generate a random number between for the operation
    operation = random.randint( 1, 2 )
    # Push $t0 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t0, 0($sp)\n"
    # Push $t1 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t1, 0($sp)\n"
    # Store the first source register in $t0
    # If the register is a number use addi, otherwise use add
    if source_register_1.isdigit():
        return_string += "addi $t0, $zero, " + source_register_1 + "\n"
    else:
        return_string += "add $t0, $zero, " + source_register_1 + "\n"
    # Store the second source register in $t1
    # If the register is a number use addi, otherwise use add
    if source_register_2.isdigit():
        return_string += "addi $t1, $zero, " + source_register_2 + "\n"
    else:
        return_string += "add $t1, $zero, " + source_register_2 + "\n"
    # If the operation is 1
    if operation == 1:
        # AND $t0 and $t1, store result in destination register
        and_operation = "and " + destination_register + ", $t0, $t1\n"
        return_string += and_operation
        # XOR $t0 and $t1, store result in $t0
        xor_instr = "xor $t0, $t0, $t1\n"
        return_string += xor_instr
        # Add $t0 to destination register, store result in destination register
        return_string += "add " + destination_register + ", " + destination_register + ", $t0\n"
    # Otherwise, the operation is 2
    else:
        # Operation 2: x | y -> ( x & ~y ) + ( ~x & y )
        # ~y, store result in $t1
        return_string += "nor $t1, $t1, $t1\n"
        # AND $t0 and $t1, store result in destination register
        and_operation = "and " + destination_register + ", $t0, $t1\n"
        return_string += and_operation
        # ~x, store result in $t0
        return_string += "nor $t0, $t0, $t0\n"
        # ~y again, store result in $t1
        return_string += "nor $t1, $t1, $t1\n"
        # AND $t0 and $t1, store result in $t0
        and_operation = "and $t0, $t0, $t1\n"
        return_string += and_operation
        # Add $t0 to destination register, store result in destination register
        return_string += "add " + destination_register + ", " + destination_register + ", $t0\n"
    # Pop $t1 from the stack
    # If destination register is $t1, don't load it
    if destination_register != "$t1":
        return_string += "lw $t1, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # Pop $t0 from the stack
    # If destination register is $t0, don't load it
    if destination_register != "$t0":
        return_string += "lw $t0, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # End debug string
    return_string += "# END OR: " + instruction + "\n"
    return return_string

def obfuscate_xor( instruction, depth = 3 ):
    # Our return string
    return_string = ""
    # If the instruction does not contain 'xor'
    if "xor" not in instruction:
        return instruction
        
    # Split the instruction into its parts
    instruction_parts = instruction.split( " " )
     # Get the destination register
    destination_register = instruction_parts[1].strip( "," )
    # Get the first source register, strip the comma
    source_register_1 = instruction_parts[2].strip( "," )
    # Get the second source register, strip the comma
    source_register_2 = instruction_parts[3].strip( "," )

    # If the depth is 0, just do a normal xor
    if depth == 0:
        return_string += "xor " + destination_register + ", " + source_register_1 + ", " + source_register_2 + "\n"
        return return_string

    # Operation 1: x ^ y -> ( x | y ) - ( x & y )
    # Operation 2: x ^ y -> ( x & ~y ) | ( ~x & y )
    
    # Generate a random number between for the operation
    operation = random.randint( 2, 2 )
    # Push $t0 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t0, 0($sp)\n"
    # Push $t1 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t1, 0($sp)\n"
    # Store the first source register in $t0
    # If the register is a number use addi, otherwise use add
    if source_register_1.isdigit():
        return_string += "addi $t0, $zero, " + source_register_1 + "\n"
    else:
        return_string += "add $t0, $zero, " + source_register_1 + "\n"
    # Store the second source register in $t1
    # If the register is a number use addi, otherwise use add
    if source_register_2.isdigit():
        return_string += "addi $t1, $zero, " + source_register_2 + "\n"
    else:
        return_string += "add $t1, $zero, " + source_register_2 + "\n"
    return_string += "# Begin XOR obfuscation\n"
    # If the operation is 1
    if operation == 1:
        # OR $t0 and $t1, store result in destination register
        or_instruction = "or " + destination_register + ", $t0, $t1\n"
        return_string += obfuscate_or( or_instruction, depth - 1 )
        # AND $t0 and $t1, store result in $t0
        and_instruction = "and $t0, $t0, $t1\n"
        return_string += obfuscate_and( and_instruction, depth - 1 )
        # Subtract $t0 from destination register, store result in destination register
        return_string += "sub " + destination_register + ", " + destination_register + ", $t0\n"
    # Otherwise, the operation is 2
    else:
        # Operation 2: x ^ y -> ( x & ~y ) | ( ~x & y )
        # ~y, store result in $t1
        return_string += "nor $t1, $t1, $t1\n"
        # AND $t0 and $t1, store result in destination register
        and_operation = "and " + destination_register + ", $t0, $t1\n"
        return_string += obfuscate_and( and_operation, depth - 1 )
        # ~x, store result in $t0
        return_string += "nor $t0, $t0, $t0\n"
        # ~y again, store result in $t1
        return_string += "nor $t1, $t1, $t1\n"
        # AND $t0 and $t1, store result in $t0
        and_operation = "and $t0, $t0, $t1\n"
        return_string += obfuscate_and( and_operation, depth - 1 )
        # OR $t0 and destination register, store result in destination register
        or_operation = "or " + destination_register + ", " + destination_register + ", $t0\n"
        return_string += obfuscate_or( or_operation, depth - 1 )
    return_string += "# End XOR obfuscation\n"
    # Pop $t1 from the stack
    # If destination register is $t1, don't load it
    if destination_register != "$t1":
        return_string += "lw $t1, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # Pop $t0 from the stack
    # If destination register is $t0, don't load it
    if destination_register != "$t0":
        return_string += "lw $t0, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"

    return return_string

def obfuscate_add( instruction, depth = 3 ):
    # Our return string
    return_string = ""
    # If the instruction does not contain 'add'
    if "add" not in instruction:
        return instruction
        
    # Split the instruction into its parts
    instruction_parts = instruction.split( " " )
     # Get the destination register
    destination_register = instruction_parts[1].strip( "," )
    # Get the first source register, strip the comma
    source_register_1 = instruction_parts[2].strip( "," )
    # Get the second source register, strip the comma
    source_register_2 = instruction_parts[3].strip( "," )

    # If the depth is 0, just do a normal add
    if depth == 0:
        # If the second source register is a number, use addi
        if source_register_2.isdigit():
            return_string += "addi " + destination_register + ", " + source_register_1 + ", " + source_register_2 + "\n"
        # Otherwise, use add
        else:
            return_string += "add " + destination_register + ", " + source_register_1 + ", " + source_register_2 + "\n"
        return return_string

    # Operation 1: x + y -> x - ( ~y ) - 1
    # Operation 2: x + y -> ( x | y ) + ( x & y )

    # Generate a random number between for the operation
    operation = random.randint( 2, 2 )
    #print( "Operation: " + str( operation ) )
    # Push $t0 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t0, 0($sp)\n"
    # Push $t1 to the stack
    return_string += "addi $sp, $sp, -4\n"
    return_string += "sw $t1, 0($sp)\n"
    # Store the first source register in $t0
    # If the register is a number use addi, otherwise use add
    if source_register_1.isdigit():
        return_string += "addi $t0, $zero, " + source_register_1 + "\n"
    else:
        return_string += "add $t0, $zero, " + source_register_1 + "\n"
    # Store the second source register in $t1
    # If the register is a number use addi, otherwise use add
    if source_register_2.isdigit():
        return_string += "addi $t1, $zero, " + source_register_2 + "\n"
    else:
        return_string += "add $t1, $zero, " + source_register_2 + "\n"
    # If the operation is 1
    if operation == 1:
        # ~y
        return_string += "nor $t1, $t1, $zero\n"
        # x - ( ~y )
        return_string += "sub " + destination_register + ", $t0, $t1\n"
        # x - ( ~y ) - 1
        return_string += "addi " + destination_register + ", " + destination_register + ", -1\n"
    # Otherwise, the operation is 2
    else:
        # x + y -> ( x | y ) + ( x & y )
        # ( x | y ), store into destination register
        or_instruction = "or " + destination_register + ", $t0, $t1\n"
        return_string += obfuscate_or( or_instruction, depth - 1 )
        # ( x & y ), store into $t0
        and_instruction = "and $t0, $t0, $t1\n"
        return_string += obfuscate_and( and_instruction, depth - 1 )
        # ( x | y ) + ( x & y ), store into destination register
        return_string += "add " + destination_register + ", " + destination_register + ", $t0\n"
    # Pop $t1 from the stack
    # If destination register is $t1, don't load it
    if destination_register != "$t1":
        return_string += "lw $t1, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"
    # Pop $t0 from the stack
    # If destination register is $t0, don't load it
    if destination_register != "$t0":
        return_string += "lw $t0, 0($sp)\n"
    return_string += "addi $sp, $sp, 4\n"

    return return_string

def obfuscate_adds( instructions: list ):
    # Strip all instructions of whitespace
    instructions = [ instruction.strip( ) for instruction in instructions ]
    # Get start of .text
    text_start = instructions.index( ".text" )
    # Loop from start of .text to end of .text, backwards
    for i in range( len( instructions ) - 1, text_start, -1 ):
        # Check if 'add' is the start of the instruction
        if "add" != instructions[i][0:3]:
            continue
        print( "Obfuscating add: " + instructions[i] )
        # Split instruction by spaces, strip commas
        instruction_parts = instructions[i].split( " " )
        instruction_parts = [ part.strip( "," ) for part in instruction_parts ]
        # Get destination register
        destination_register = instruction_parts[1]
        # Check if this is an immediate instruction, contains an 'i'
        is_immediate = "i" in instruction_parts[0]
        # Get source register 1
        source_register_1 = instruction_parts[2]
        # Get source register 2
        source_register_2 = instruction_parts[3]

        # Store original instruction
        original_instruction = instructions[i]

        # If destination register is $sp, skip
        if destination_register == "$sp":
            continue

        # Remove original instruction
        instructions.pop( i )

        # Select 2 random registers that are not the destination register or source registers
        random_registers = random.sample( registers, 2 )
        # If any of the random registers are the destination register or source registers, get new random registers
        while destination_register in random_registers or source_register_1 in random_registers or source_register_2 in random_registers:
            random_registers = random.sample( registers, 2 )

        # Our new instruction to insert
        new_instruction = ""

        # Insert comment of original instruction
        new_instruction += "# " + original_instruction + "\n"

        # Push temporary registers to the stack
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[0] + ", 0($sp)\n" )
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[1] + ", 0($sp)\n" )

        # Set the first temporary register to the first source register
        if source_register_1.isdigit():
            new_instruction += ( "addi " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )

        # Set the second temporary register to the second source register
        if source_register_2.isdigit():
            new_instruction += ( "addi " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )

        # Operation 1: x + y -> x - ( ~y ) - 1
        # Operation 2: x + y -> ( x | y ) + ( x & y )

        # Random number between 1 and 2
        operation = random.randint( 1, 2 )
        # If the operation is 1
        if operation == 1:
            # ~y
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            # x - ( ~y )
            new_instruction += (  "sub " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            # x - ( ~y ) - 1
            new_instruction += (  "addi " + destination_register + ", " + destination_register + ", -1\n" )
        # Otherwise, the operation is 2
        else:
            # x + y -> ( x | y ) + ( x & y )
            # ( x | y ), store into destination register
            new_instruction += (  "or " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            # ( x & y ), store into the first temporary register
            new_instruction += (  "and " + random_registers[0] + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            # ( x | y ) + ( x & y ), store into destination register
            new_instruction += (  "add " + destination_register + ", " + destination_register + ", " + random_registers[0] + "\n" )

        # Pop temporary registers from the stack
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[1] + ", 0($sp)\n" )
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[0] + ", 0($sp)\n" )

        # Split into list of strings by newline
        new_instruction = new_instruction.split( "\n" )
        # Reverse list
        new_instruction.reverse( )
        # Insert each instruction
        for instruction in new_instruction:
            instructions.insert( i, instruction )
        
    return instructions
    
def obfuscate_subs( instructions: list ):
    # Strip all instructions of whitespace
    instructions = [ instruction.strip( ) for instruction in instructions ]
    # Get start of .text
    text_start = instructions.index( ".text" )
    # Loop from start of .text to end of .text, backwards
    for i in range( len( instructions ) - 1, text_start, -1 ):
        # Check if 'add' is the start of the instruction
        if "sub" != instructions[i][0:3]:
            continue
        # Split instruction by spaces, strip commas
        instruction_parts = instructions[i].split( " " )
        instruction_parts = [ part.strip( "," ) for part in instruction_parts ]
        # Get destination register
        destination_register = instruction_parts[1]
        # Check if this is an immediate instruction, contains an 'i'
        is_immediate = "i" in instruction_parts[0]
        # Get source register 1
        source_register_1 = instruction_parts[2]
        # Get source register 2
        source_register_2 = instruction_parts[3]

        # Store original instruction
        original_instruction = instructions[i]

        # Remove original instruction
        instructions.pop( i )

        # Select 2 random registers that are not the destination register or source registers
        random_registers = random.sample( registers, 2 )
        # If any of the random registers are the destination register or source registers, get new random registers
        while destination_register in random_registers or source_register_1 in random_registers or source_register_2 in random_registers:
            random_registers = random.sample( registers, 2 )

        # Our new instruction to insert
        new_instruction = ""

        # If destination register is $sp, skip
        if destination_register == "$sp":
            continue

        # Insert comment of original instruction
        new_instruction += "# " + original_instruction + "\n"

        # Push temporary registers to the stack
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[0] + ", 0($sp)\n" )
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[1] + ", 0($sp)\n" )

        # Set the first temporary register to the first source register
        if source_register_1.isdigit():
            new_instruction += ( "addi " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )

        # Set the second temporary register to the second source register
        if source_register_2.isdigit():
            new_instruction += ( "addi " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )

        # Operation 1: x - y -> x + ~y + 1
        # Operation 2: x - y -> ( x & ~y ) - ( ~x & y )

        # Random number between 1 and 2
        operation = random.randint( 1, 2 )
        # If the operation is 1
        if operation == 1:
            # ~y
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            # x + ~y
            new_instruction += (  "add " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            # x + ~y + 1
            new_instruction += (  "addi " + destination_register + ", " + destination_register + ", 1\n" )
        # Otherwise, the operation is 2
        else:
            # x - y -> ( x & ~y ) - ( ~x & y )
            # ( x & ~y ), store into destination register
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "and " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            # ( ~x & y ), store into the first temporary register
            new_instruction += (  "nor " + random_registers[0] + ", " + random_registers[0] + ", $zero\n" )
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "and " + random_registers[0] + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            # ( x & ~y ) - ( ~x & y ), store into destination register
            new_instruction += (  "sub " + destination_register + ", " + destination_register + ", " + random_registers[0] + "\n" )

        # Pop temporary registers from the stack
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[1] + ", 0($sp)\n" )
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[0] + ", 0($sp)\n" )

        # Split into list of strings by newline
        new_instruction = new_instruction.split( "\n" )
        # Reverse list
        new_instruction.reverse( )
        # Insert each instruction
        for instruction in new_instruction:
            instructions.insert( i, instruction )
        
    return instructions

def obfuscate_ands( instructions: list ):
    # Strip all instructions of whitespace
    instructions = [ instruction.strip( ) for instruction in instructions ]
    # Get start of .text
    text_start = instructions.index( ".text" )
    # Loop from start of .text to end of .text, backwards
    for i in range( len( instructions ) - 1, text_start, -1 ):
        # Check if 'add' is the start of the instruction
        if "and" != instructions[i][0:3]:
            continue
        # Split instruction by spaces, strip commas
        instruction_parts = instructions[i].split( " " )
        instruction_parts = [ part.strip( "," ) for part in instruction_parts ]
        # Get destination register
        destination_register = instruction_parts[1]
        # Check if this is an immediate instruction, contains an 'i'
        is_immediate = "i" in instruction_parts[0]
        # Get source register 1
        source_register_1 = instruction_parts[2]
        # Get source register 2
        source_register_2 = instruction_parts[3]

        # Store original instruction
        original_instruction = instructions[i]

        # If destination register is $sp, skip
        if destination_register == "$sp":
            continue

        # Remove original instruction
        instructions.pop( i )

        # Select 2 random registers that are not the destination register or source registers
        random_registers = random.sample( registers, 2 )
        # If any of the random registers are the destination register or source registers, get new random registers
        while destination_register in random_registers or source_register_1 in random_registers or source_register_2 in random_registers:
            random_registers = random.sample( registers, 2 )

        # Our new instruction to insert
        new_instruction = ""

        # Insert comment of original instruction
        new_instruction += "# " + original_instruction + "\n"

        # Push temporary registers to the stack
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[0] + ", 0($sp)\n" )
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[1] + ", 0($sp)\n" )

        # Set the first temporary register to the first source register
        if source_register_1.isdigit():
            new_instruction += ( "addi " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )

        # Set the second temporary register to the second source register
        if source_register_2.isdigit():
            new_instruction += ( "addi " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )

        # Operation 1: x & y -> ~( ~x | ~y )
        # Operation 2: x & y -> ( ~x | y ) - ~x

        # Random number between 1 and 2
        operation = random.randint( 1, 2 )
        # If the operation is 1
        if operation == 1:
            # ~( ~x | ~y )
            new_instruction += (  "nor " + random_registers[0] + ", " + random_registers[0] + ", $zero\n" )
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "or " + random_registers[0] + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "nor " + destination_register + ", " + random_registers[0] + ", $zero\n" )
        # Otherwise, the operation is 2
        else:
            # ( ~x | y ) - ~x
            new_instruction += (  "nor " + random_registers[0] + ", " + random_registers[0] + ", $zero\n" )
            new_instruction += (  "or " + random_registers[1] + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "sub " + destination_register + ", " + random_registers[1] + ", " + random_registers[0] + "\n" )

        # Pop temporary registers from the stack
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[1] + ", 0($sp)\n" )
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[0] + ", 0($sp)\n" )

        # Split into list of strings by newline
        new_instruction = new_instruction.split( "\n" )
        # Reverse list
        new_instruction.reverse( )
        # Insert each instruction
        for instruction in new_instruction:
            instructions.insert( i, instruction )
        
    return instructions

def obfuscate_ors( instructions: list ):
    # Strip all instructions of whitespace
    instructions = [ instruction.strip( ) for instruction in instructions ]
    # Get start of .text
    text_start = instructions.index( ".text" )
    # Loop from start of .text to end of .text, backwards
    for i in range( len( instructions ) - 1, text_start, -1 ):
        # Check if 'or' is the start of the instruction
        if "or" != instructions[i][0:2]:
            continue
        # Split instruction by spaces, strip commas
        instruction_parts = instructions[i].split( " " )
        instruction_parts = [ part.strip( "," ) for part in instruction_parts ]
        # Get destination register
        destination_register = instruction_parts[1]
        # Check if this is an immediate instruction, contains an 'i'
        is_immediate = "i" in instruction_parts[0]
        # Get source register 1
        source_register_1 = instruction_parts[2]
        # Get source register 2
        source_register_2 = instruction_parts[3]

        # Store original instruction
        original_instruction = instructions[i]

                # If destination register is $sp, skip
        if destination_register == "$sp":
            continue

        # Remove original instruction
        instructions.pop( i )

        # Select 2 random registers that are not the destination register or source registers
        random_registers = random.sample( registers, 2 )
        # If any of the random registers are the destination register or source registers, get new random registers
        while destination_register in random_registers or source_register_1 in random_registers or source_register_2 in random_registers:
            random_registers = random.sample( registers, 2 )

        # Our new instruction to insert
        new_instruction = ""

        # Insert comment of original instruction
        new_instruction += "# " + original_instruction + "\n"

        # Push temporary registers to the stack
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[0] + ", 0($sp)\n" )
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[1] + ", 0($sp)\n" )

        # Set the first temporary register to the first source register
        if source_register_1.isdigit():
            new_instruction += ( "addi " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )

        # Set the second temporary register to the second source register
        if source_register_2.isdigit():
            new_instruction += ( "addi " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )

        # Operation 1: x | y -> ( x & y ) + ( x ^ y )
        # Operation 2: x | y -> ( x & ~y ) + ( ~x & y )
    
        # Random number between 1 and 2
        operation = random.randint( 1, 2 )
        # If the operation is 1
        if operation == 1:
            # ( x & y ) + ( x ^ y )
            new_instruction += (  "and " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "xor " + random_registers[1] + ", " + random_registers[1] + ", " + random_registers[0] + "\n" )
            new_instruction += (  "add " + destination_register + ", " + destination_register + ", " + random_registers[1] + "\n" )
        # Otherwise, the operation is 2
        else:
            # ( x & ~y ) + ( ~x & y )
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "and " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "nor " + random_registers[0] + ", " + random_registers[0] + ", $zero\n" )
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "and " + random_registers[1] + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "add " + destination_register + ", " + destination_register + ", " + random_registers[1] + "\n" )


        # Pop temporary registers from the stack
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[1] + ", 0($sp)\n" )
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[0] + ", 0($sp)\n" )

        # Split into list of strings by newline
        new_instruction = new_instruction.split( "\n" )
        # Reverse list
        new_instruction.reverse( )
        # Insert each instruction
        for instruction in new_instruction:
            instructions.insert( i, instruction )
        
    return instructions


def obfuscate_xors( instructions: list ):
    # Strip all instructions of whitespace
    instructions = [ instruction.strip( ) for instruction in instructions ]
    # Get start of .text
    text_start = instructions.index( ".text" )
    # Loop from start of .text to end of .text, backwards
    for i in range( len( instructions ) - 1, text_start, -1 ):
        # Check if 'or' is the start of the instruction
        if "xor" != instructions[i][0:3]:
            continue
        # Split instruction by spaces, strip commas
        instruction_parts = instructions[i].split( " " )
        instruction_parts = [ part.strip( "," ) for part in instruction_parts ]
        # Get destination register
        destination_register = instruction_parts[1]
        # Check if this is an immediate instruction, contains an 'i'
        is_immediate = "i" in instruction_parts[0]
        # Get source register 1
        source_register_1 = instruction_parts[2]
        # Get source register 2
        source_register_2 = instruction_parts[3]

        # Store original instruction
        original_instruction = instructions[i]

        # If destination register is $sp, skip
        if destination_register == "$sp":
            continue

        # Remove original instruction
        instructions.pop( i )

        # Select 2 random registers that are not the destination register or source registers
        random_registers = random.sample( registers, 2 )
        # If any of the random registers are the destination register or source registers, get new random registers
        while destination_register in random_registers or source_register_1 in random_registers or source_register_2 in random_registers:
            random_registers = random.sample( registers, 2 )

        # Our new instruction to insert
        new_instruction = ""

        # Insert comment of original instruction
        new_instruction += "# " + original_instruction + "\n"

        # Push temporary registers to the stack
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[0] + ", 0($sp)\n" )
        new_instruction += ( "addi $sp, $sp, -4\n" )
        new_instruction += ( "sw " + random_registers[1] + ", 0($sp)\n" )

        # Set the first temporary register to the first source register
        if source_register_1.isdigit():
            new_instruction += ( "addi " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[0] + ", $zero, " + source_register_1 + "\n" )

        # Set the second temporary register to the second source register
        if source_register_2.isdigit():
            new_instruction += ( "addi " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )
        else:
            new_instruction += ( "add " + random_registers[1] + ", $zero, " + source_register_2 + "\n" )

        # Operation 1: x ^ y -> ( x | y ) - ( x & y )
        # Operation 2: x ^ y -> ( x & ~y ) | ( ~x & y )
    
        # Random number between 1 and 2
        operation = random.randint( 1, 2 )
        # If the operation is 1
        if operation == 1:
            # ( x | y ) - ( x & y )
            new_instruction += (  "or " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "and " + random_registers[1] + ", " + random_registers[1] + ", " + random_registers[0] + "\n" )
            new_instruction += (  "sub " + destination_register + ", " + destination_register + ", " + random_registers[1] + "\n" )
        # Otherwise, the operation is 2
        else:
            # ( x & ~y ) | ( ~x & y )
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "and " + destination_register + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "nor " + random_registers[0] + ", " + random_registers[0] + ", $zero\n" )
            new_instruction += (  "nor " + random_registers[1] + ", " + random_registers[1] + ", $zero\n" )
            new_instruction += (  "and " + random_registers[1] + ", " + random_registers[0] + ", " + random_registers[1] + "\n" )
            new_instruction += (  "or " + destination_register + ", " + destination_register + ", " + random_registers[1] + "\n" )


        # Pop temporary registers from the stack
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[1] + ", 0($sp)\n" )
        new_instruction += (  "addi $sp, $sp, 4\n" )
        new_instruction += (  "lw " + random_registers[0] + ", 0($sp)\n" )

        # Split into list of strings by newline
        new_instruction = new_instruction.split( "\n" )
        # Reverse list
        new_instruction.reverse( )
        # Insert each instruction
        for instruction in new_instruction:
            instructions.insert( i, instruction )
        
    return instructions


def obfuscate_arithmetic( instruction ):
    # Our return string
    return_string = ""
    # If this is a label, skip it 
    if ":" in instruction:
        return instruction
    # If the instruction contains 'xor'
    if "xor" in instruction:
        return obfuscate_xor( instruction, 2 )
    # If the instruction contains 'or'
    if "or" in instruction:
        return obfuscate_or( instruction, 5 )

    # If the instruction contains 'add' 
    if "add" in instruction:
        return obfuscate_add( instruction, 7 )
    # If the instruction contains 'and'
    if "and" in instruction:
        return obfuscate_and( instruction, 5 )
    # If the instruction contains 'sub'
    if "sub" in instruction:
        return obfuscate_sub( instruction, 5 )
    # Otherwise, just return the instruction
    else:
        return_string = instruction
    return return_string

def apply_mba( instructions: list, depth: int ):
    # If depth is less than 1, return the instructions
    if depth < 1:
        return instructions

    # Loop for depth
    for i in range( depth ):
        print(len(instructions))
        # Obfuscate adds
        instructions = obfuscate_adds( instructions )
        # Obfuscate subs
        instructions = obfuscate_subs( instructions )
        # Obfuscate ands
        #instructions = obfuscate_ands( instructions )
        # Obfuscate ors
        #instructions = obfuscate_ors( instructions )
        # Obfuscate xors
        #instructions = obfuscate_xors( instructions )
    # return
    return instructions

# Deadcode obfuscation
def find_deadcode_insertion_point( instructions, register ):
    # Deadcode Insert Point definition:
    # Area is suitable for register deadcode insertion if:
    # The area is after the previous register access and before the next register write
    # Example with $a0:
    # mov $t0, $a0; LAST ACCESS, if there is no more access between this and the next write, this is a suitable area
    # add $t0, $t0, $t1; Does not access $a0, so this is a suitable area
    # mov $a0, $t0; NEW WRITE, modifies, $a0, so deadcode can only be inserted before this

    # Find first access of register, access is if the register is not the first part of the instruction
    first_access = -1
    for i in range( 0, len( instructions ) ):
        instruction = instructions[i]
        # If the instruction is a label, skip it
        if ":" in instruction:
            continue
        # If the instruction contains the register
        if register in instruction:
            # Split instruction by spaces, remove commas
            instruction_parts = instruction.split( " " )
            instruction_parts = [x.strip( "," ) for x in instruction_parts]
            # If length of instruction parts is 1, then skip
            if len( instruction_parts ) == 1:
                continue
            # If the register is not the first register of the instruction
            if register != instruction_parts[1]:
                first_access = i
                break
    #print( "First access: " + str( first_access ) )
    # If there is no access, return -1
    if first_access == -1:
        return -1
    # Find the next write of the register
    next_write = -1
    for i in range( first_access, len( instructions ) ):
        instruction = instructions[i]
        # If the instruction is a label, skip it
        if ":" in instruction:
            continue
        # If the instruction contains the register
        if register in instruction:
            # Split instruction by spaces, remove commas
            instruction_parts = instruction.split( " " )
            instruction_parts = [x.strip( "," ) for x in instruction_parts]
            # If length of instruction parts is 1, then skip
            if len( instruction_parts ) == 1:
                continue
            # If we encounter an access, update the first access
            if register == instruction_parts[1]:
                first_access = i
            # If the register is the first register of the instruction
            if register == instruction_parts[1]:
                next_write = i
                break
        # If we hit the last instruction, return end of function
        if i == len( instructions ) - 1:
            next_write = i - 1
            break

    # If there is no next write, return -1
    if next_write == -1:
        return -1
    # Random number between first access and next write
    return random.randint( first_access, next_write )

def insert_deadcode( instructions, register, depth = 1, use_push_and_pop = False ):
    
    # Deadcode operations
    deadcode_operations = [
        "li REG, NUM32",
        "add REG, REG, REG",
        "sub REG, REG, REG",
        "and REG, REG, REG",
        "or REG, REG, REG",
        "xor REG, REG, REG",
        "nor REG, REG, REG",
        "sll REG, REG, NUM",
        "srl REG, REG, NUM",
        "sra REG, REG, NUM",
        "mult REG, REG",
        "div REG, REG",
        "mfhi REG",
        "mflo REG",
        "slt REG, REG, REG",
        "sltu REG, REG, REG",
        "slti REG, REG, NUM",
        "sltiu REG, REG, NUM",
    ]

    # Get start of .text
    text_start = 0
    for i in range( 0, len( instructions ) ):
        if ".text" in instructions[i]:
            text_start = i
            break

    # If using push and pop
    if use_push_and_pop:
        for i in range( 0, depth ):
            # Our insertion string
            insertion_string = ""
            # Pick a random place to insert deadcode
            insertion_point = random.randint( text_start + 1, len( instructions ) - 1 )
            # Pick a random register to use
            random_register = random.choice( registers )
            # Push register onto stack
            insertion_string += "addi $sp, $sp, -4\n"
            insertion_string += "sw " + random_register + ", 0($sp)\n"
            # Pick 3 random operations
            for j in range( 0, random.randint( 1, 10 ) ):
                # Pick a random operation
                random_operation = random.choice( deadcode_operations )
                # Replace REG with our register
                random_operation = random_operation.replace( "REG", random_register )
                # Replace NUM32 with a random 32 bit number
                random_operation = random_operation.replace( "NUM32", str( random.randint( -2147483648, 2147483647 ) ) )
                # Replace NUM with a random number
                random_operation = random_operation.replace( "NUM", str( random.randint( 0, 31 ) ) )
                # Append #DEADCODE to the end of the operation
                random_operation += " #DEADCODE"
                # Add the operation to the insertion string
                insertion_string += random_operation + "\n"
            # Pop register from stack
            insertion_string += "lw " + random_register + ", 0($sp)\n"
            insertion_string += "addi $sp, $sp, 4\n"
            # Split the insertion string into lines
            insertion_string = insertion_string.split( "\n" )
            # Add each line to the instructions
            for line in insertion_string:
                instructions.insert( insertion_point, line )
                insertion_point += 1
        return instructions

    # Find insertion point
    insertion_point = find_deadcode_insertion_point( instructions, register )
    #print( "Inserting deadcode at " + str( insertion_point ) )
    # If insertion point is -1, return instructions
    if insertion_point == -1:
        return instructions

    # Loop for depth
    for i in range( 0, depth ):
        # Generate random number for deadcode operation
        deadcode_operation = random.choice( deadcode_operations )
        # Replace REG with register
        deadcode_operation = deadcode_operation.replace( "REG", register )
        # Replace NUM with random number
        deadcode_operation = deadcode_operation.replace( "NUM", str( random.randint( 0, 31 ) ) )
        # Add '# DEADCODE' to the end of the instruction
        deadcode_operation += " # DEADCODE"
        # Add newline to deadcode operation
        deadcode_operation += "\n"
        # Insert deadcode operation into instructions
        instructions.insert( insertion_point, deadcode_operation )
    return instructions

def scramble_deadcode( instructions ):
    # Get all instructions with '# DEADCODE' in them and their indices
    deadcode_instructions = []
    for i in range( 0, len( instructions ) ):
        instruction = instructions[i]
        if "# DEADCODE" in instruction:
            deadcode_instructions.append( (i, instruction) )
    # Get all consecutive deadcode instruction groups
    deadcode_instruction_groups = []
    current_group = []
    for i in range( 0, len( deadcode_instructions ) ):
        # If the current index is the next index in the list
        if i + 1 < len( deadcode_instructions ) and deadcode_instructions[i][0] + 1 == deadcode_instructions[i + 1][0]:
            # Add the current instruction to the current group
            current_group.append( deadcode_instructions[i] )
        # Otherwise, add the current instruction to the current group and add the group to the list of groups
        else:
            current_group.append( deadcode_instructions[i] )
            deadcode_instruction_groups.append( current_group )
            current_group = []
    
    # For each group, scramble the order of the instructions
    for group in deadcode_instruction_groups:
        random.shuffle( group )
    # Replace the instructions in the original list with the scrambled instructions
    for group in deadcode_instruction_groups:
        for instruction in group:
            instructions[instruction[0]] = instruction[1]
    return instructions

def create_random_function( instructions: str ):
    # Create function start label
    # Get random english word
    name = generate_random_ascii_string( random.randint( 3, 10 ) )
    # Create function start label
    function_start_label = name + ":\n"
    # Create random function
    random_function = []
    # Add function start label
    random_function.append( function_start_label )
    # Add random number of instructions
    rand_instruction = generate_random_instructions( random.randint( 10, 80 ), random.randint( 3, 7 ) )
    # Append random instructions to random function
    random_function.extend( rand_instruction )
    # Convert random function to string
    random_function = "".join( random_function )
    # Find 'j main' instruction and append random function to the end of it
    instructions = instructions.replace( "j main", "j main\n" + random_function )
    return instructions


# Opaque predicate functions
def get_all_labels( instructions ):
    labels = []
    for instruction in instructions:
        if ":" in instruction:
            # Append all data before the colon, strip whitespace
            labels.append( instruction.split( ":" )[0].strip() )
    return labels

def get_all_registers( instructions ):
    # Find all registers that are used in this function
    used_registers = []
    for instruction in instructions:
        # Split instruction by spaces, remove commas
        instruction_parts = instruction.split( " " )
        instruction_parts = [x.strip( "," ) for x in instruction_parts]
        # If length of instruction parts is 1, then skip
        if len( instruction_parts ) == 1:
            continue
        # Loop through all registers, and check if the current instruction uses them
        for register in registers:
            if register in instruction_parts:
                # Append, no duplicates
                if register not in used_registers:
                    used_registers.append( register )
    return used_registers

def insert_opaque_predicate( instructions, labels, used_registers, use_push_and_pop: False ):
    # Branch instructions
    branch_instructions = [
        "beq REG, REG, LABEL", # Branch if both registers are equal
        "bne REG, REG, LABEL", # Branch if both registers are not equal
        "blez REG, LABEL", # Branch if register is less than or equal to zero
        "bgtz REG, LABEL", # Branch if register is greater than zero
        "bltz REG, LABEL", # Branch if register is less than zero
        "bgez REG, LABEL", # Branch if register is greater than or equal to zero
        "bltzal REG, LABEL", # Branch if register is less than zero and link
        "bgezal REG, LABEL", # Branch if register is greater than or equal to zero and link
    ]
    # Get index of first label
    first_label_index = -1
    for i in range( 0, len( instructions ) ):
        instruction = instructions[i]
        if ":" in instruction:
            first_label_index = i + 1
            break
    # Select a random spot to insert the opaque predicate, subtract 2 to account for return
    insertion_point = random.randint( first_label_index, len( instructions ) - 2 )
    # Select a random branch instruction
    branch_instruction = random.choice( branch_instructions )
    
    # Subtract used registers from all registers
    unused_registers = list( set( registers ) - set( used_registers ) )
    # Choose 2 random registers, unique
    random_registers = random.sample( unused_registers, 2 )

    # If we're using push and pop, just select two random registers
    if use_push_and_pop:
        random_registers = random.sample( registers, 2 )

    # Build our branch instruction
    final_instruction_sequence = ""
    # If we're using push and pop, push the registers
    if use_push_and_pop:
        final_instruction_sequence += "addi $sp, $sp, -8\n"
        final_instruction_sequence += "sw " + random_registers[0] + ", 4($sp)\n"
        final_instruction_sequence += "sw " + random_registers[1] + ", 0($sp)\n"

    # Replace first REG with first random register, second REG with second random register
    branch_instruction = branch_instruction.replace( "REG", random_registers[0], 1 )
    branch_instruction = branch_instruction.replace( "REG", random_registers[1], 1 )
    # Choose random label
    label = random.choice( labels )
    branch_instruction = branch_instruction.replace( "LABEL", label )
    # Switch based on the branch instruction
    if "beq" in branch_instruction or "bne" in branch_instruction:
        # Choose another random register
        branch_instruction = branch_instruction.replace( "REG", random_registers[1] )
    # Prepend data to the branch instruction to ensure the branch is not taken depending on the branch instruction
    if "beq" in branch_instruction:
        # Ensure register and register2 are not equal
        branch_instruction = "li " + random_registers[0] + ", 1\n" + "li " + random_registers[1] + ", 2\n" + branch_instruction
    elif "bne" in branch_instruction:
        # Ensure register and register2 are equal
        branch_instruction = "li " + random_registers[0] + ", 1\n" + "li " + random_registers[1] + ", 1\n" + branch_instruction
    elif "blez" in branch_instruction:
        # Ensure register is greater than zero
        branch_instruction = "li " + random_registers[0] + ", 1\n" + branch_instruction
    elif "bgtz" in branch_instruction:
        # Ensure register is less than or equal to zero
        branch_instruction = "li " + random_registers[0] + ", -1\n" + branch_instruction
    elif "bltz" in branch_instruction:
        # Ensure register is greater than or equal to zero
        branch_instruction = "li " + random_registers[0] + ", 0\n" + branch_instruction
    elif "bgez" in branch_instruction:
        # Ensure register is less than zero
        branch_instruction = "li " + random_registers[0] + ", -1\n" + branch_instruction
    elif "bltzal" in branch_instruction:
        # Ensure register is greater than or equal to zero
        branch_instruction = "li " + random_registers[0] + ", 0\n" + branch_instruction
    elif "bgezal" in branch_instruction:
        # Ensure register is less than zero
        branch_instruction = "li " + random_registers[0] + ", -1\n" + branch_instruction
    # Append branch instruction
    final_instruction_sequence += branch_instruction
    # If we're using push and pop, pop the registers
    if use_push_and_pop:
        final_instruction_sequence += "lw " + random_registers[0] + ", 4($sp)\n"
        final_instruction_sequence += "lw " + random_registers[1] + ", 0($sp)\n"
        final_instruction_sequence += "addi $sp, $sp, 8\n"
    # Split instruciton sequence by newlines
    final_instruction_sequence = final_instruction_sequence.split( "\n" )
    # Insert the instruction sequence
    for i in range( 0, len( final_instruction_sequence ) ):
        instructions.insert( insertion_point + i, final_instruction_sequence[i] )
    return instructions

# Exception-based control functions
def create_exception_handler( instructions ):


    # Create segment '.ktext' for exception handler
    exception_handler = ".ktext 0x80000180\n"

    ''' Push $t0
sw $t0, 0($sp)
addiu $sp, $sp, -4
# Push $t1
sw $t1, 0($sp)
addiu $sp, $sp, -4
# Check if $13 is 0x20
li $t0, 0x20
# Move $13 to $t1
mfc0 $t1, $13
beq $t1, $t0, syscall_exception
j branch_exception
syscall_exception:
# xor $v0 with 0x1337
li $t0, 0x1337
xor $v0, $v0, $t0
# syscall
syscall
# Move $8 into $t9
move $t9, $8
# Add 4
addiu $t9, $t9, 4
branch_exception:'''
    # push $t0
    exception_handler += "sw $t0, 0($sp)\n"
    exception_handler += "addiu $sp, $sp, -4\n"
    # push $t1
    exception_handler += "sw $t1, 0($sp)\n"
    exception_handler += "addiu $sp, $sp, -4\n"
    # Check if $13 is 0x20
    exception_handler += "li $t0, 0x20\n"
    # Move $13 to $t1
    exception_handler += "mfc0 $t1, $13\n"
    exception_handler += "beq $t1, $t0, syscall_exception\n"
    exception_handler += "j branch_exception\n"
    exception_handler += "syscall_exception:\n"
    # xor $v0 with 0x1337
    exception_handler += "li $t0, 0x1337\n"
    exception_handler += "xor $v0, $v0, $t0\n"
    # syscall
    exception_handler += "syscall\n"
    # Move $8 into $t9
    exception_handler += "mfc0 $t9, $14\n"
    # Add 4
    exception_handler += "addiu $t9, $t9, 4\n"
    exception_handler += "branch_exception:\n"
    # Exception handler gets target address stored in $t9 and store it into $14 (exception handler address), then eret
    exception_handler += "mtc0 $t9, $14\n"
    # Pop $t8
    exception_handler += "lw $t8, 0($sp)\n"
    exception_handler += "addiu $sp, $sp, 4\n"
    # Pop stack into $t9
    exception_handler += "lw $t9, 0($sp)\n"
    # Increment stack pointer
    exception_handler += "addiu $sp, $sp, 4\n"
    exception_handler += "eret\n"

    # Split exception handler into lines
    exception_handler = exception_handler.split( "\n" )

    # Append exception handler to the end of the instructions
    instructions += exception_handler
    return instructions

def obfuscate_branch_instructions( instructions ):
    # Branch instructions + jump instructions
    branch_instructions = [
        "beq", # Branch if both registers are equal
        "bne", # Branch if both registers are not equal
        "blez", # Branch if register is less than or equal to zero
        "bgtz", # Branch if register is greater than zero
        "bltz", # Branch if register is less than zero
        "bgez", # Branch if register is greater than or equal to zero
        "bltzal", # Branch if register is less than zero and link
        "bgezal", # Branch if register is greater than or equal to zero and link
        "j", # Jump to label
        "jal", # Jump to label and link
        "jr", # Jump to register
        "jalr", # Jump to register and link
    ]
    # Loop through all instructions
    for i in range( 0, len( instructions ) ):
        instruction = instructions[i]
        # Get instruction name
        instruction_name = instruction.split( " " )[0]
        #print( instruction_name )
        # Check if instruction is a branch instruction
        if instruction_name in branch_instructions:
            #print( "Found branch instruction: " + instruction )
            # Split instruction by spaces
            instruction_split = instruction.split( " " )
            # Get length of instruction split
            instruction_split_length = len( instruction_split )
            # Get index of label
            label_index = instruction_split_length - 1
            # Get label
            label = instruction_split[label_index]
            # Check if 'label' contains a '$', if so, it's a register
            if "$" in label:
                # Get register
                register = label
                # Push $t9 onto stack
                new_instruction += (  "sw $t9, 0($sp)" )
                # Decrement stack pointer
                instructions.insert( i + 1, "addiu $sp, $sp, -4" )
                # Push $t8 onto stack
                instructions.insert( i + 2, "sw $t8, 0($sp)" )
                # Decrement stack pointer
                instructions.insert( i + 3, "addiu $sp, $sp, -4" )
                # Set $t8 to zero   
                instructions.insert( i + 4, "li $t8, 0" )
                # Set $t9 to register
                instructions.insert( i + 5, "move $t9, " + register )
                # lw $t8, 0($t8), causes exception
                instructions.insert( i + 6, "lw $t8, 0($t8)" )
            # Otherwise, it's a label
            else:
                # Get label
                label = instruction_split[label_index]
                # Push $t9 onto stack
                new_instruction += (  "sw $t9, 0($sp)" )
                # Decrement stack pointer
                instructions.insert( i + 1, "addiu $sp, $sp, -4" )
                # Push $t8 onto stack
                instructions.insert( i + 2, "sw $t8, 0($sp)" )
                # Decrement stack pointer
                instructions.insert( i + 3, "addiu $sp, $sp, -4" )
                # Set $t8 to zero   
                instructions.insert( i + 4, "li $t8, 0" )
                # Set $t9 to label
                instructions.insert( i + 5, "la $t9, " + label )
                # lw $t8, 0($t8), causes exception
                instructions.insert( i + 6, "lw $t8, 0($t8)" )
            # remove original instruction
            instructions.pop( i )

    return instructions

# Syscall obfuscation
def obfuscate_syscall_instructions( instructions ):
    # Strip all instructions of whitespace
    instructions = [x.strip() for x in instructions]
    # Loop through all instructions
    for i in range( 0, len( instructions ) ):
        syscall_instruction = "syscall"
        # Check if instruction is a syscall instruction
        if syscall_instruction in instructions[i]:
            # Get current $v0 value by searching backwards for 'li $v0,', max 10 instructions
            for j in range( i, i - 10, -1 ):
                if "li $v0," in instructions[j]:
                    # Get $v0 value
                    v0_value = instructions[j].split( " " )[2]
                    # Check if $v0 value is valid
                    if v0_value.isdigit():
                        # Xor value with 0x1337
                        xor_value = int( v0_value ) ^ 0x1337
                        # Replace li $v0, with li $v0, xor_value
                        instructions[j] = "li $v0, " + str( xor_value )
                        break

    return instructions

# String obfuscation
def get_all_strings( instructions ):
    # Return map of variable name and their starting string value
    string_map = {}
    # Find start of .data section
    data_section_start = instructions.index( ".data" )
    # End of .data section is .text
    data_section_end = instructions.index( ".text" )
    # Loop through all instructions
    for i in range( data_section_start, data_section_end ):
        # Check if instruction contains .ascii
        if ".ascii" in instructions[i]:
            # Variable name is start of line until ':'
            variable_name = instructions[i].split( ":" )[0]
            # Strip variable name of whitespace
            variable_name = variable_name.strip()
            # String value is everything after '.ascii(z)', might contain 'z' at the end, account for
            # this by removing it
            string_value = instructions[i].split( ".ascii" )[1].replace( "z", "" ).strip()
            # Remove surrounding quotes
            string_value = string_value.replace( "\"", "" )
            # Replace all escaped characters with their actual value
            string_value = string_value.replace( "\\t", "\t" )
            string_value = string_value.replace( "\\n", "\n" )
            string_value = string_value.replace( "\\r", "\r" )
            string_value = string_value.replace( "\\0", "\0" )
            # Add variable name and string value to map
            string_map[variable_name] = string_value
    return string_map

# Convert string to hex
def string_to_hex( string ):
    # If string length is not divisible by 4, pad with null bytes
    if len( string ) % 4 != 0:
        string += "\x00" * ( 4 - len( string ) % 4 )
    # Get every 4 characters of string
    string_bytes = [string[i:i+4] for i in range( 0, len( string ), 4 )]
    # Convert each 4 character string to bytes
    string_bytes = [x.encode( "utf-8" ) for x in string_bytes]
    # Convert each 4 characters to hex
    string_hex = [hex( struct.unpack( "<I", x )[0] ) for x in string_bytes]
    # If any element in the list is less than 8 character (minus the '0x' prefix), pad with a 0 first then random characters
    #for i in range( 0, len( string_hex ) ):
    #    if len( string_hex[i] ) < 10:
    #        string_hex[i] = "0x0" + ''.join( random.choice( string.hexdigits ) for _ in range( 6 ) )
    # Add null terminator to end of string
    string_hex.append( "0x0" )
    # Xor each hex value with the string encryption key
    string_hex = [hex( int( x, 16 ) ^ STRING_ENCRYPTION_KEY ) for x in string_hex]
    # Print string with each element separated by a comma
    raw_str = ", ".join( string_hex )
    # Return hex strings
    return raw_str

def obfuscate_strings( instructions ):
    # Ensure all instructions are stripped of whitespace
    instructions = [x.strip( ) for x in instructions]
    # Get all strings
    string_map = get_all_strings( instructions )
    # Get data section start
    data_section_start = instructions.index( ".data" )
    # Get data section end
    data_section_end = instructions.index( ".text" )
    # Loop through all instructions
    for i in range( data_section_start, data_section_end ):
        # Check if instruction contains .ascii
        if ".ascii" in instructions[i]:
            # Get variable name
            variable_name = instructions[i].split( ":" )[0]
            # Strip variable name of whitespace
            variable_name = variable_name.strip( )
            # Get string value
            string_value = string_map[variable_name]
            # Convert string to hex
            string_hex = string_to_hex( string_value )
            # Set line value to variable_name: .word string_hex
            #instructions[i] = variable_name + ": .word " + string_hex
    # Get .text section start
    text_section_start = instructions.index( ".text" )
    # .text section end is last line
    text_section_end = len( instructions ) - 1
    # Loop through all instructions backwards
    for i in range( text_section_end, text_section_start, -1 ):
        # Find all instances of 'la {register}, {variable_name}'
        if "la " in instructions[i]:
            # Get register
            register = instructions[i].split( " " )[1]
            # Get variable name
            variable_name = instructions[i].split( " " )[2]
            # Check if variable name is in string map
            if variable_name in string_map:
                # Get string value
                string_value = string_map[variable_name]
                # Convert string to hex
                string_hex = string_to_hex( string_value )
                # Convert string hex to list split by spaces, remove commas
                string_hex = string_hex.split( " " )
                string_hex = [x.replace( ",", "" ) for x in string_hex]
                # Reverse list
                string_hex.reverse( )
                # Insert new instructions
                # select random temporary register to use
                temp_register = random.choice( temp_registers )
                # Our insert string
                insert_string = ""
                # Push temporary register onto stack
                insert_string += "addi $sp, $sp, -4\n"
                insert_string += "sw " + temp_register + ", 0($sp)\n"
                # Allocate space for string on stack
                insert_string += "addi $sp, $sp, -" + str( ( len( string_hex ) + 1 ) * 4 ) + "\n"
                string_stack_instructions = []
                # Loop through all hex values
                for j in range( 0, len( string_hex ) ):
                    string_instruction = ""
                    enc_key = STRING_ENCRYPTION_KEY
                    # Load hex value into temporary register
                    string_instruction += "li " + temp_register + ", " + string_hex[j] + "\n"
                    # Select random register to use that is not the temporary register
                    random_register = random.choice( registers )
                    while random_register == temp_register:
                        random_register = random.choice( registers )
                    # Push random register onto stack
                    string_instruction += "addi $sp, $sp, -4\n"
                    string_instruction += "sw " + random_register + ", 0($sp)\n"
                   
                    # Get random number from 0 to STRING_ENCRYPTION_KEY
                    #rand_int = random.randint( 0, enc_key )
                    # Subtract random number from encryption key
                    #enc_key = STRING_ENCRYPTION_KEY - rand_int
                    # Set the random register to the string encryption key
                    string_instruction += "li " + random_register + ", " + hex( enc_key ) + "\n"
                    # Add rand_int to the random register
                    #string_instruction += "addiu " + random_register + ", " + random_register + ", " + hex( rand_int ) + "\n"
                    # Xor the temporary register with the random register
                    string_instruction += "xor " + temp_register + ", " + temp_register + ", " + random_register + "\n"
                    # Pop random register off stack
                    string_instruction += "lw " + random_register + ", 0($sp)\n"
                    string_instruction += "addi $sp, $sp, 4\n"
                    # Get offset from stack pointer
                    offset = j * 4
                    # Offset = size of string - offset
                    offset = len( string_hex ) * 4 - offset
                    # Store temporary register to stack
                    string_instruction += "sw " + temp_register + ", " + str( offset ) + "($sp)\n"
                    # Add string instruction to list
                    string_stack_instructions.append( string_instruction )
                # Randomize order of string stack instructions
                random.shuffle( string_stack_instructions )
                # Add all string stack instructions to insert string
                for j in range( 0, len( string_stack_instructions ) ):
                    insert_string += string_stack_instructions[j]
                # Set register to point to top of stack + 4
                insert_string += "addi " + register + " $sp, 4\n"
                # Add size of string to stack pointer
                insert_string += "addi $sp, $sp, " + str( ( len( string_hex ) + 1 ) * 4 ) + "\n"
                # Pop temporary register off stack
                insert_string += "lw " + temp_register + ", 0($sp)\n"
                insert_string += "addi $sp, $sp, 4\n"
                # Split string into list of instructions by newlines
                insert_string = insert_string.split( "\n" )
                # Insert new instructions in place of old instruction
                instructions[i:i+1] = insert_string

    # Delete all .ascii instructions
    for i in range( data_section_start, data_section_end ):
        if ".ascii" in instructions[i]:
            instructions[i] = ""

    return instructions

# Constant unfolding obfuscation
def constant_unfolding( instructions, max_iterations ):
    # Ensure all instructions are stripped of whitespace
    instructions = [x.strip( ) for x in instructions]
    # Find all "load immediate" instructions (li), search backwards
    for i in range( len( instructions ) - 1, -1, -1 ):
        if "li " in instructions[i]:
            # Get register
            register = instructions[i].split( " " )[1]
            # Get immediate value
            immediate = instructions[i].split( " " )[2]
            # Get immediate value as int, if it is a hex value, convert to int
            immediate_int = 0
            if "0x" in immediate:
                immediate_int = int( immediate, 16 )
            else:
                immediate_int = int( immediate )
            # Build an arithmetic expression that evaluates to the immediate value
            # We will use this expression to replace the immediate value, example expression: immediate = 3 * 2 + 18 - 2
            current_value = immediate_int
            # Pick random number from 1 to max_iterations
            iterations = max_iterations #random.randint( 1, max_iterations )
            expression = ""
            # Loop through iterations
            for j in range( 0, iterations ):
                # Pick a random operation
                operation = random.choice( ["+", "-", "*"] )
                iter_count = 0
                # Switch on operation
                if operation == "+":
                    # Pick random number from 1 to 10000000, ensure no overflow
                    rand_int = random.randint( 1, 100000 )
                    while current_value + rand_int > 4294967295: # kekw
                        rand_int = random.randint( 1, 100000 )
                        iter_count += 1
                        if iter_count > 100:
                            rand_int = 0
                            break
                    # Update current value
                    current_value += rand_int
                    # Prepend subtraction to expression to get back to original value
                    expression = " - " + str( rand_int ) + ") " + expression
                elif operation == "-":
                    # Pick random number from 1 to 10000000, ensure no overflow
                    rand_int = random.randint( 0, 1000000 )
                    while current_value - rand_int < 0:
                        rand_int = random.randint( 0, 10000 )
                        iter_count += 1
                        if iter_count > 100:
                            rand_int = 0
                            break
                    # Update current value
                    current_value -= rand_int
                    # Add addition to expression to get back to original value
                    expression = " + " + str( rand_int ) + ") " + expression
                elif operation == "*":
                    # Pick random number from 1 to 100, ensure no overflow
                    rand_int = random.randint( 1, 10 )
                    while current_value * rand_int > 4294967295:
                        rand_int = random.randint( 1, 10 )
                        iter_count += 1
                        if iter_count > 100:
                            rand_int = 1
                            break
                    # Ensure no remainder
                    while current_value % rand_int != 0:
                        rand_int = random.randint( 1, 10 )
                    # Update current value
                    current_value *= rand_int
                    # Add division to expression to get back to original value
                    expression = " / " + str( rand_int ) + ") " + expression
            # Prepend current value to expression
            expression = str( current_value ) + " " + expression
            # Prepend 'iterations' number of parentheses
            for j in range( 0, iterations ):
                expression = "(" + expression
            # Eval expression
            expression_result = eval( expression )
            # Ensure expression result is equal to immediate value
            if expression_result != immediate_int:
                print( "Error: expression result does not equal immediate value" )
            # Get mips code of expression
            expression_instructions = math_to_mips( expression, register )
            # Split expression instructions by newlines
            expression_instructions = expression_instructions.split( "\n" )
            # Remove original `li` instruction
            instructions.pop( i )
            # Insert new instructions in place of old instruction
            instructions[i:i] = expression_instructions
            #print(instructions)
    return instructions

# Code chunking functions
def chunk_code( instructions, chunk_size ):
    
    # Ensure all instructions are stripped of whitespace
    instructions = [x.strip( ) for x in instructions]
    # Remove any blank lines or comments
    instructions = [x for x in instructions if x != "" and x[0] != "#"]
    # Get index of .text section
    text_index = instructions.index( ".text" )
    # What is chunking?
    # Chunking is the process of taking a large block of code and breaking it up into smaller blocks, seperated by a jump instruction
    # Once the code is chunked, the chunks are shuffled around the program

    # List of chunks, contains lists of instructions, each list of instruction should start with a label and end with a jump instruction
    chunks = []
    label_name = generate_random_ascii_string( 10 )
    i = text_index + 1
    # Iterate through all instructions
    while i < len( instructions ) - 1:
        # Current chunk length
        chunk_length = 0
        # New chunk
        new_chunk = []
        # Add label to new chunk, random english word
        new_chunk.append( label_name + ":" )
        # Pick random number from 1 to chunk_size
        select_chunk_size = random.randint( 1, chunk_size )
        # Loop for chunk size
        for j in range( 0, min( select_chunk_size, len( instructions ) - i - 1 ) ):
            # If we encounter an unconditional jump, break out of loop
            if "j " in instructions[i] or "jr " in instructions[i]:
                # Append jump instruction to new chunk
                new_chunk.append( instructions[i] )
                # Increment i to skip jump instruction
                i += 1
                break
            # If we encounter a label, increment i and break out of loop
            if ":" in instructions[i]:
                # Append label to new chunk
                new_chunk.append( instructions[i] )
                i += 1
                break
            #print(instructions[i])
            # Append instruction to new chunk
            new_chunk.append( instructions[i] )
            # Increment chunk length
            chunk_length += 1
            # Increment instruction index
            i += 1
        label_name = generate_random_ascii_string( 10 )
        
        # If there is a previous chunk, add jump instruction to new chunk if it does not already have one
        if len( chunks ) > 0:
            # If not unconfitional jmp as last instruction
            if "j " not in new_chunk[-1] and "jr " not in new_chunk[-1]:
                # Get label of previous chunk
                previous_chunk_label = chunks[-1][0]
                # Strip colon from label
                previous_chunk_label = previous_chunk_label[:-1]
                # Append jump instruction to previous chunk
                new_chunk.append( "j " + label_name )
        # Append new chunk to list of chunks
        chunks.append( new_chunk )
        # Debug
        #print( "Chunk length: " + str( chunk_length ) )
    # Remove last instruction of last chunk due to EOF
    chunks[-1].pop( )
    # Return list of chunks
    return chunks

def randomize_existing_labels( instructions:list ):
    # Get all labels after .text section
    # Get start of .text section
    text_index = instructions.index( ".text" )
    # Get all labels after .text section
    labels = [x for x in instructions[text_index:] if ":" in x]
    # Find all references to labels
    label_references = [x for x in instructions if ":" not in x]

    # Rename all labels and their references
    for label in labels:
        # Get label name
        label_name = label[:-1]
        # Generate new random label name
        new_label_name = generate_random_ascii_string( 10 )
        # Replace all instances of 'label_name' with 'new_label_name'
        instructions = [x.replace( label_name, new_label_name ) for x in instructions]
    return instructions

def obfuscate( function_code, labels ):
    # Our return string
    return_string = "\n"
    # Loop through all the lines in the function code
    for line in function_code.split( "\n" ):
        # Skip empty lines
        if line == "":
            continue
        # Skip lines that has a # as the  first non-whitespace character
        if line.lstrip( )[0] == "#":
            continue
        # Strip the line of whitespace
        line = line.strip( )
        # Perform MBA obfuscation on the line
        #line = obfuscate_arithmetic( line )
        #print( line )
        # Add the line to the return string
        return_string += line + "\n"
    # Perform deadcode obfuscation on the function code
    return_string = return_string.split( "\n" )
    #return_string = insert_deadcode( return_string, "$a1", 5 )
    #return_string = insert_deadcode( return_string, "$a0", 5 )
    #return_string = insert_deadcode( return_string, "$t0", 5 )

    # Scramble deadcode
    # return_string = scramble_deadcode( return_string )
    
    # Get all used registers
    used_registers = get_all_registers( return_string )

    # Insert deadcode for all used registers
    #for register in used_registers:
    #    return_string = insert_deadcode( return_string, register, 5 )
    # Insert extra random deadcode
    #return_string = insert_deadcode( return_string, "aaa", 100, True )

    #return_string = obfuscate_branch_instructions( return_string )

    # Insert opaque predicate
    #return_string = insert_opaque_predicate( return_string, labels, used_registers )
    # Loop 10 times
    #for i in range( 0, 40 ):
    #    # Insert opaque predicate
    #    return_string = insert_opaque_predicate( return_string, labels, used_registers )
    # Find all newlines in list elements, and split them into new elements
    return_string = [ x for y in return_string for x in y.split( "\n" ) if x != "" ]


    # add '\n' to each element in the list
    return_string = [x + "\n" for x in return_string]
    # Add exception obfuscation
    # convert return string to string
    return_string = "".join( return_string )
    return return_string

def main( args ):
    
    print( args )
    
    # Get current script directory
    script_dir = os.path.dirname( __file__ )
    mips_file = open( script_dir + "\\" + args.input, "r" )
    mips_code = mips_file.read( )
    mips_file.close( )

    # Check if we should create random function
    if args.random_function:
        # Generate random functions
        for i in range( 0, args.random_function_size ):
            mips_code = create_random_function( mips_code )

    #print(mips_code)
    # Convert mips code to a list by splitting on newlines
    mips_code = mips_code.split( "\n" )

    #mips_code = apply_mba( mips_code, 1 )

    # Randomize labels
    if args.randomize_labels:
        mips_code = randomize_existing_labels( mips_code )

    # Obfuscate all strings
    if args.encrypt_string:
        mips_code = obfuscate_strings( mips_code )

     # Obfuscate syscalls
    if args.obfuscate_syscalls:
        mips_code = obfuscate_syscall_instructions( mips_code )

    # Insert extra random deadcode
    if args.random_deadcode:
        mips_code = insert_deadcode( mips_code, "aaa", 1000, True )

    # Get all labels
    labels = get_all_labels( mips_code )
    # Add opaque predicate to all labels
    #for i in range( 0, 100 ):
    #    mips_code = insert_opaque_predicate( mips_code, labels, temp_registers, True )

    # Perform constant folding obfuscation
    if args.constant_encryption:
        print( "Performing constant folding obfuscation" )
        #print(mips_code)
        # Get size of 'mips_code' before obfuscation
        mips_code_size = len( mips_code )
        mips_code = constant_unfolding( mips_code, args.constant_encryption_size )

        ## Get size of 'mips_code' after obfuscation
        mips_code_size_after = len( mips_code )
        ## Calculate the size difference
        size_difference = mips_code_size_after - mips_code_size
        ## Print the size difference
        print( "Size difference: " + str( size_difference ) )

    # Perform chunking on mips code
    if args.chunk:
        chunks = chunk_code( mips_code, args.chunk_size )
        random.shuffle( chunks )

        # Delete all lines after 'j main' in mips code
        for i in range( 0, len( mips_code ) ):
            if "j main" in mips_code[i]:
                mips_code = mips_code[:i + 1]
                break
        # Print all chunks
        for chunk in chunks:
            # Print each element in chunk on a new line
            for line in chunk:
                # Append to mips code
                mips_code.append( line )

    # Create exception handler
    if args.exception_handler:
        mips_code = create_exception_handler( mips_code )

    #print( '\n'.join( mips_code ) )
    # Remove any line that contains a '#' character
    mips_code = [ x for x in mips_code if "#" not in x ]
    obfuscated_mips_file = open( script_dir + "\\" + args.output, "w" )
    obfuscated_mips_file.write( '\n'.join(mips_code) )
    return

    # Add newlines to each element in the list
    mips_code = [x + "\n" for x in mips_code]
    # Convert mips code to string
    mips_code = "".join( mips_code )
    #print(mips_code)
    # All functions in the mips code start with the string "# FUNCTION START #" and end with "# FUNCTION END #"
    # Get all code between these two strings
    function_code = mips_code.split( function_start_str )[1].split( function_end_str )[0]
    
    # Remove all lines between the function start and end strings
    mips_code = re.sub( r"# FUNCTION START #.*# FUNCTION END #", "", mips_code, flags=re.DOTALL )
    # Strip the mips code of whitespace
    mips_code = mips_code.strip( )
    # Remove all comments
    mips_code = re.sub( r'#.*', '', mips_code )
    # Regex all data before a ':', ignore '#'
    labels = re.findall( r"(?<!#)\b\w+(?=:)", mips_code )


    #print(labels)
    obfuscated_function = obfuscate( function_code, labels )
    # Add the obfuscated function to the mips code
    mips_code += "\n" + obfuscated_function

    # Create exception handler
    mips_code = create_exception_handler( mips_code )

    # Write the obfuscated function to a new mips file
    obfuscated_mips_file = open( "obfuscated_mips_code.asm", "w" )
    obfuscated_mips_file.write( mips_code )

if __name__ == "__main__":
    # Argparse
    parser = argparse.ArgumentParser(
        description = "Obfuscate MIPS assembly code"
     )
    # Arguments: 
    # -i, --input: Input file
    # -o, --output: Output file
    # Optional arguments:
    # -s, --shuffle: Shuffle the code
    # -c, --chunk: Chunk the code
    # --chunk-size: Size of chunks
    # -d, --deadcode: Insert deadcode
    # -r, --random-deadcode: Insert random deadcode
    # -e, --encrypt-string: Encrypt strings
    # --constant-encryption: Encrypt constants
    # --constant-encryption-size: Size of constant encryption
    # --random-function: Insert random function
    # --random-function-size: Size of random function
    # --obfuscate-syscalls: Obfuscate syscalls
    # --exception-handler: Enable exception handler
    # --randomize-labels: Randomize labels

    parser.add_argument( "-i", "--input", help="Input file", required=True )
    parser.add_argument( "-o", "--output", help="Output file", required=True )
    parser.add_argument( "-s", "--shuffle", help="Shuffle the code", action="store_true" )
    parser.add_argument( "-c", "--chunk", help="Chunk the code", action="store_true" )
    parser.add_argument( "--chunk-size", help="Size of chunks", type=int, default=5 )
    parser.add_argument( "-d", "--deadcode", help="Insert deadcode", action="store_true" )
    parser.add_argument( "-r", "--random-deadcode", help="Insert random deadcode", action="store_true" )
    parser.add_argument( "-e", "--encrypt-string", help="Encrypt strings", action="store_true" )
    parser.add_argument( "--constant-encryption", help="Encrypt constants", action="store_true" )
    parser.add_argument( "--constant-encryption-size", help="Size of constant encryption", type=int, default=5 )
    parser.add_argument( "--random-function", help="Insert random function", action="store_true" )
    parser.add_argument( "--random-function-size", help="Size of random function", type=int, default=5 )
    parser.add_argument( "--obfuscate-syscalls", help="Obfuscate syscalls", action="store_true" )
    parser.add_argument( "--exception-handler", help="Enable exception handler", action="store_true" )
    parser.add_argument( "--randomize-labels", help="Randomize labels", action="store_true" )

    print("a")

    # Get arguments
    args = parser.parse_args( )
    main( args )