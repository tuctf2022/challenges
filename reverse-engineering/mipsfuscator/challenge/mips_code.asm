# Mips program

# Our .data section
.data
# word input buffer pointer
input: .space 64

intro: .asciiz "\nWelcome to the laundry day!\nThis is the first mipsfuscator challenge!\n"
prompt: .asciiz "Enter the password: "
success: .asciiz "\nAccess granted!\nGood job, try the next challenge!\n"
failure: .asciiz "\nAccess denied!\n"

password: .asciiz "TUCTF{1_h0p3_y0u_d1dn7_br34kp01n7_m3_37291}\n"
.text
j main
strcmp:
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    addi $sp, $sp, -4
    sw $t1, 0($sp)
    lb $t0, 0($a0)
    lb $t1, 0($a1)
    bne $t0, $t1, strcmp_L2
    addi $a1, $a1, 1
    addi $a0, $a0, 1
strcmp_L1:
    beq $t0, $zero, strcmp_L3
    lb $t0, 0($a0)
    addi $a0, $a0, 1
    lb $t1, 0($a1)
    addi $a1, $a1, 1
    beq $t0, $t1, strcmp_L1
strcmp_L2:
    li $v0, 0
    lb $t1, 0($sp)
    addi $sp, $sp, 4
    lb $t0, 0($sp)
    addi $sp, $sp, 4
    jr $ra
strcmp_L3:
    li $v0, 1
    lb $t1, 0($sp)
    addi $sp, $sp, 4
    lb $t0, 0($sp)
    addi $sp, $sp, 4
    jr $ra

main:
    li $v0, 4
    la $a0, intro
    syscall
    li $v0, 4
    la $a0, prompt
    syscall
    li $v0, 8
    la $a0, input
    li $a1, 64
    syscall
    la $a0, input
    la $a1, password
    jal strcmp
    move $a0, $v0
    li $v0, 4
    beq $a0, $zero, main_failure
    la $a0, success
    syscall
    j exit
main_failure:
    la $a0, failure
    syscall
exit:
    li $v0, 10
    syscall
    li $v0, 0