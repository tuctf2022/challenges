# MIPSfuscator: A MIPS Obfuscator
## Goals: Apply MBA ruling, opaque predicates, and other techniques to obfuscate MIPS code.

# Laundry Day
## Description: Can you help me fold my laundry
## Flag: TUCTF{1_h0p3_y0u_d1dn7_br34kp01n7_m3_37291}

# Needle in the Stack
## Description: Can you find the needle in the (hay)stack?
## Flag: TUCTF{w45_7h47_50_h4rd_70_f1nd?_593046}