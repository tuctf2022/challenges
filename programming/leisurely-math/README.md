# Leisurely Math
## Description
Too slow for rapid arithmetic? Want to take things a little more leisurely? Try this challenge!

## Flag 
TUCTF{7h4nk5_f0r_74k1n6_7h1n65_4_l177l3_5l0w_4268285}