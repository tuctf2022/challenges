import sys
import os
import random
# PIL
from PIL import Image

def load_bmp_data( filepath: str ):
    # Open the file in binary mode
    with open( filepath, 'rb' ) as f:
        # Read the header
        header = f.read( 54 )
        # Read the data
        data = f.read( )
    # Return the data
    return header, data

def save_bmp_data( filepath: str, header: bytes, data: bytes ):
    # Open the file in binary mode
    with open( filepath, 'wb' ) as f:
        # Write the header
        f.write( header )
        # Write the data
        f.write( data )

def randomize_grayscale( data: bytes ):
    # Color threshold for randomization, 128 is the middle of the range
    threshold = 137
    # Convert the data to a list
    data = list( data )
    # Randomize the grayscale
    for i in range( 0, len( data ) - 3, 3 ):
        # Skip all black pixels
        if data[i] == 0 and data[i+1] == 0 and data[i+2] == 0:
            continue
        # Randomize If the rgb values are above the threshold
        if data[i] > threshold and data[i+1] > threshold and data[i+2] > threshold:
            data[i] = random.randint( threshold, 255 )
            data[i+1] = random.randint( threshold, 255 )
            data[i+2] = random.randint( threshold, 255 )
    # Return the data
    return bytes( data )

def create_list_of_answers( filename: str, data: bytes ):
    list_of_answers = []
    # iterate all bytes in data
    for i in range( 0, len( data ) - 3, 3 ):
        # Append each byte to a list
        list_of_answers.append( data[i] )
    # write the list to a file
    with open( filename, 'w' ) as f:
        for i in list_of_answers:
            f.write( str( i ) + "\n" )

def main( ):
    # Get path of the current file
    path = os.path.dirname( os.path.realpath( __file__ ) )

    filepath = path + "\\tuctf_flag.bmp"

    # Open the file and convert to grayscale using PIL
    img = Image.open( filepath ).convert( 'L' )
    # Save the image
    img.save( path + "\\tuctf_flag.bmp" )

    # Load the bmp data
    header, data = load_bmp_data( filepath )
    # Randomize the grayscale
    data = randomize_grayscale( data )
    # Create list of answers
    create_list_of_answers( path + "\\answers.txt", data )
    # Strip the file extension
    filepath = filepath[ : filepath.rfind( '.' ) ]
    # Save the bmp data
    save_bmp_data(  (filepath + '_randomized.bmp' ), header, data )

    img = Image.open( filepath + '_randomized.bmp' ).convert( 'L' )
    # Save the image
    img.save( path + "\\tuctf_flag_randomized.bmp" )

if __name__ == '__main__':
    main( )