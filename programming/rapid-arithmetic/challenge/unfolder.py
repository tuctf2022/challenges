# Constant unfolder
import random
import os

def unfold_number( number:int, depth:int, ceiling:int=100000, primitive_only:bool=False ):
    # Our resultant expression
    expression = ""
    current_value = number
    # Loop through iterations
    for i in range( depth ):
        # Pick a random operation
        operation = random.choice( ["+", "-", "*"] )
        # If primitive only, only use addition or subtraction
        if primitive_only:
            operation = random.choice( ["+", "-"] )
        iter_count = 0
        # Switch on operation
        if operation == "+":
            # Pick random number from 1 to 10000000, ensure no overflow
            rand_int = random.randint( 1, ceiling )
            # Update current value
            current_value += rand_int
            # Prepend subtraction to expression to get back to original value
            expression = " - " + str( rand_int ) + " ) " + expression
        elif operation == "-":
            # Pick random number from 1 to ceiling, ensure no overflow
            rand_int = random.randint( 0, ceiling )
            # Update current value
            current_value -= rand_int
            # Add addition to expression to get back to original value
            expression = " + " + str( rand_int ) + " ) " + expression
        elif operation == "*":
            # Pick random number from 1 to 100, ensure no overflow
            rand_int = random.randint( 1, ceiling )
            # Ensure no remainder
            while current_value % rand_int != 0:
                rand_int = random.randint( 1, ceiling )
            # Update current value
            current_value *= rand_int
            # Add division to expression to get back to original value
            expression = " / " + str( rand_int ) + " ) " + expression
    # Prepend current value to expression
    expression = str( current_value ) + " " + expression
    # Prepend 'iterations' number of parentheses
    for j in range( 0, depth ):
        expression = "(" + expression
    # Return expression
    return expression

if __name__ == "__main__":
    print( unfold_number( 100, 10 ) )