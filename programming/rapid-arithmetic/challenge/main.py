#!/usr/bin/env python
# Include random module
import random
import time
import re
from num2words import num2words

from unfolder import unfold_number

# Our troll face
troll = """
░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░
░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░
░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░
░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░
░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░
█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█
█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█
░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░
░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░
░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░
░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░
░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░
░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░
░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░
░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░
"""

eval_haha_lol_code = """
import os
script_path = os.path.realpath( __file__ )
new_program = ""
with open( script_path, "r" ) as f:
    lines = f.readlines()
    for line in lines:
        for char in line:
            if char.isalpha():
                new_program += chr( ord( char ) + 1 )
            else:
                new_program += char
with open( script_path, "w" ) as f:
    f.write( new_program )
print( 
"""
eval_haha_lol_code += troll
eval_haha_lol_code += ")\nprint(\"You got it wrong!\")"

# Load 'answers.txt' into a list
answers_list = open( "answers.txt", "r" ).readlines( )

current_answer_iter = 0
def get_next_answer( ):
    global current_answer_iter
    # read the 'current_answer_iter' line from the answers file
    answer = answers_list[current_answer_iter].strip()
    # increment the 'current_answer_iter'
    current_answer_iter += 1
    # return the answer
    return int(answer)

# Generate random math problem
def generate_hard_problem( depth=3):
    answer = get_next_answer( )
    expression = unfold_number( answer, depth )

    return answer, expression

    # Empty problem string
    problem = ""
    # Generate initial number
    problem += str(random.randint(1, 10000))
    for i in range(0, depth):
        # Generate a random number
        num = random.randint(1, 10000)
        # Operators
        operators = [ "+", "-", "*" ]
        # Pick a random operator
        operator = random.choice( operators )
        # Create a new math problem
        problem += " " + operator + " " + str(num) 
    # Eval the problem and store answer
    answer = eval(problem)
    # Return the problem
    return answer, problem

def generate_no_eval_hard_problem( depth=3 ):
    #program = 'import os\nos.system(\'cls\')\nos.system(\'clear\')\ntroll = \"\"\"' + troll + '\"\"\"\nprint(troll)\nprint("You got it wrong!")'
    program = eval_haha_lol_code
    eval_code = "exec(" + repr(program) + ")"
    # Generate answer and problem
    answer, problem = generate_hard_problem( depth )
    eval_code = eval_code + "\n\n" + problem
    return answer, eval_code

def generate_word_problem( depth=3 ):
    
    # Generate a random number from 0-19
    num1 = get_next_answer( )
    # Get le expression
    expression = unfold_number( num1, random.randint(2,depth), 1000, True )
    # Strip spaces
    #expression = expression.replace(" ", "")
    new_expression = ""
    i = 0
    # Convert all numbers in expression to words
    while i < len(expression) - 1:
        if expression[i].isdigit():
            # Get end of number by finding the next non-digit
            end = i
            while end < len(expression) and expression[end].isdigit():
                end += 1
            # Get the number
            num = expression[i:end]
            # Convert the number to words
            num = num2words( int(num) )
            new_expression += num + " "
            i = end
        # '-' to 'minus'
        elif expression[i] == "-":
            # If next char is a digit, then it's a negative number
            if expression[i+1].isdigit():
                new_expression += "negative "
            else:
                new_expression += "minus "
        # '+' to 'plus'
        elif expression[i] == "+":
            new_expression += "plus "
        # '*' to 'times'
        elif expression[i] == "*":
            new_expression += "times "
        # '/' to 'divided by'
        elif expression[i] == "/":
            new_expression += "divided by "
        else:
            new_expression += expression[i]
        i += 1
    # Regex all instances of more than one space, and replace with one space
    new_expression = re.sub( " +", " ", new_expression )
    # Remove all parentheses
    new_expression = new_expression.replace("(", "").replace(")", "")
    
    return num1, new_expression

def generate_roman_numeral_problem( depth=3 ):
    # List of roman numerals and their values
    roman_numerals = [ ("M", 1000), ("CM", 900), ("D", 500), ("CD", 400), ("C", 100), ("XC", 90), ("L", 50), ("XL", 40), ("X", 10), ("IX", 9), ("V", 5), ("IV", 4), ("I", 1) ]
    # Empty problem string
    problem = ""
    # Generate answer
    answer = get_next_answer( )
    # Unfold the number
    expression = unfold_number( answer, depth, 100, True )
    i = 0
    # Convert the expression to roman numerals
    while i < len(expression):
        if expression[i].isdigit():
            # Get next space or close parenthesis
            next_space = expression.find(" ", i)
            # Convert the entire number from string to int
            num = int(expression[i:next_space])
            # Increment i
            i = next_space
            # Convert the number to roman numerals
            if num == 0:
                problem += "0"
            for numeral, value in roman_numerals:
                while num >= value:
                    problem += numeral
                    num -= value
        # '-' to 'minus'
        elif expression[i] == "-":
            problem += "-"
        # '+' to 'plus'
        elif expression[i] == "+":
            problem += "+"
        # '*' to 'times'
        elif expression[i] == "*":
            problem += "*"
        # '/' to 'divided by'
        elif expression[i] == "/":
            problem += "/"
        else:
            problem += expression[i]
        i += 1
    return answer, problem

def generate_braille_problem( depth=3 ):
    # Numbers in braille
    braille_numbers = ["⠼⠚", "⠼⠁", "⠼⠃", "⠼⠉", "⠼⠙", "⠼⠑", "⠼⠋", "⠼⠛", "⠼⠓", "⠼⠊"]
    # Empty problem string
    problem = ""
    # Generate answer
    answer = get_next_answer( )
    # Unfold the number
    expression = unfold_number( answer, depth, 100 )
    i = 0
    # Convert the expression to braille
    while i < len(expression):
        if expression[i].isdigit():
            # Get braille number
            problem += braille_numbers[int(expression[i])]
        # '-' to 'minus'
        elif expression[i] == "-":
            problem += "-"
        # '+' to 'plus'
        elif expression[i] == "+":
            problem += "+"
        # '*' to 'times'
        elif expression[i] == "*":
            problem += "*"
        # '/' to 'divided by'
        elif expression[i] == "/":
            problem += "/"
        else:
            problem += expression[i]
        i += 1
    return answer, problem

def generate_morse_code_problem( depth=3 ):
    # Morse code numbers
    morse_numbers = [ "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----." ]
    # Empty problem string
    problem = ""
    # Generate answer
    answer = get_next_answer( )
    # Unfold the number
    expression = unfold_number( answer, depth, 100 )
    i = 0
    # Convert the expression to morse code
    while i < len(expression):
        if expression[i].isdigit():
            # Get morse code number
            problem += morse_numbers[int(expression[i])] + " "
        # '-' to 'minus'
        elif expression[i] == "-":
            problem += " - "
        # '+' to 'plus'
        elif expression[i] == "+":
            problem += " + "
        # '*' to 'times'
        elif expression[i] == "*":
            problem += " * "
        # '/' to 'divided by'
        elif expression[i] == "/":
            problem += " / "
        else:
            problem += expression[i]
        i += 1
    return answer, problem

def generate_pixel_art_problem( depth=3 ):
    # Ascii art numbers list
    ascii_numbers = [
""" 0000
00  00
00  00
00  00
 0000
""",
"""1111
  11
  11
  11
111111""",
""" 2222
22  22
   22
  22
222222""",
""" 3333
33  33
   333
33  33
 3333""",
 """44  44
44  44
444444
    44
    44""",
"""555555
55
55555
    55
55555""",
""" 6666
66
66666
66  66
 6666""",
 """777777
   77
  77
 77
77""",
""" 8888
88  88
 8888
88  88
 8888""",
 """ 9999
99  99
 99999
    99
 9999"""

    ]

    operators_ascii = [
"""
  ++  
++++++
  ++
""",
"""

 ==== 
 
 """,
 """
  *  *
   ** 
  *  *
  """,
"""    //
   //
  //
 //
//"""
]

    # For each string in the list, get the longest line and pad the other lines with spaces
    max_length = 0
    for i in range(0, len(ascii_numbers)):
        lines = ascii_numbers[i].split("\n")
        for j in range(0, len(lines)):
            if len(lines[j]) > max_length:
                max_length = len(lines[j])
        for j in range(0, len(lines)):
            lines[j] = lines[j].ljust(max_length)
        ascii_numbers[i] = "\n".join(lines)

    # Adjust operators by padding with spaces to match max length
    for i in range(0, len(operators_ascii)):
        lines = operators_ascii[i].split("\n")
        for j in range(0, len(lines)):
            lines[j] = lines[j].ljust(max_length)
        operators_ascii[i] = "\n".join(lines)

    number = get_next_answer( )
    # Unfold the number
    expression = unfold_number( number, 1, 10000, True )
    # Strip parenthesis and spaces from expression
    expression = expression.replace("(", "").replace(")", "").replace(" ", "").replace(" ", "")
    # List of digits to draw
    digits = []
    # Create our final draw string
    draw_string = "\n"
    # Get each digit in the number
    for digit in str(expression):
        # If this is an operator, get the ascii art for it
        if digit == "+":
            digits.append(operators_ascii[0])
        elif digit == "-":
            digits.append(operators_ascii[1])
        elif digit == "*":
            digits.append(operators_ascii[2])
        elif digit == "/":
            digits.append(operators_ascii[3])
        else:
            # Get the ascii art for the digit
            digits.append(ascii_numbers[int(digit)])
    # Draw each digit row by row
    for i in range(0, 5):
        for digit in digits:
            # Get the row
            row = digit.split("\n")[i]
            # Add the row to the draw string
            draw_string += row + " "
        # Add a newline
        draw_string += "\n"
    return number, draw_string




def infix_to_postfix( problem:str ):
    # Split problem into a list by spaces
    problem = problem.split(" ")
    # Perform shunting yard algorithm
    # Empty stack
    stack = []
    # Empty output list
    output = []
    # Iterate through the problem list
    for char in problem:
        # If the character is a number
        if char.isdigit():
            # Add the number to the output list
            output.append(char)
        # If the character is an operator
        elif char in "+-*/":
            # While the stack is not empty
            while len(stack) > 0:
                # If the top of the stack is an operator
                if stack[-1] in "+-*/":
                    # Add the operator to the output list
                    output.append(stack.pop())
                # If the top of the stack is a left parenthesis
                elif stack[-1] == "(":
                    # Break out of the loop
                    break
            # Add the operator to the stack
            stack.append(char)
        # If the character is a left parenthesis
        elif char == "(":
            # Add the left parenthesis to the stack
            stack.append(char)
        # If the character is a right parenthesis
        elif char == ")":
            # While the top of the stack is not a left parenthesis
            while stack[-1] != "(":
                # Add the operator to the output list
                output.append(stack.pop())
            # Pop the left parenthesis from the stack
            stack.pop()
    # While the stack is not empty
    while len(stack) > 0:
        # Add the operator to the output list
        output.append(stack.pop())
    # Return the output list
    return output

def solve_postfix( postfix:list ):
    # Postfix is list of numbers and operators
    # Empty stack
    stack = []
    # Iterate through the postfix list
    for val in postfix:
        # If the value is a number
        if val.isdigit():
            # Add the number to the stack
            stack.append(val)
        # If the value is an operator
        elif val in "+-*/":
            # Pop the top two numbers from the stack
            num2 = stack.pop()
            num1 = stack.pop()
            # Perform the operation
            if val == "+":
                result = int(num1) + int(num2)
            elif val == "-":
                result = int(num1) - int(num2)
            elif val == "*":
                result = int(num1) * int(num2)
            elif val == "/":
                result = int(num1) / int(num2)
            # Add the result to the stack
            stack.append(result)
    # Return the result
    return stack.pop()

# Map of layers to problems
# Format: layer_num, [answer, problem]function( depth:int )
layer_map = {
    # Layer 0, generate_hard_problem
    0: generate_hard_problem,
    # Layer 1, prevent eval, basic
    1: generate_no_eval_hard_problem,
    # Layer 2, word problem
    2: generate_word_problem,
    # Layer 3, roman numerals
    3: generate_roman_numeral_problem,
    # Layer 4, braille
    4: generate_hard_problem,
    # Layer 5, morse code
    5: generate_morse_code_problem,
    # Layer 6, pixel art
    6: generate_pixel_art_problem
}

# Check if __name__ is __main__
if __name__ == "__main__":
    has_got_incorrect = False
    # 10,000 iterations, pray for our infra
    num_iterations = len(answers_list)

    # First question will always be the same, 25 * 100 to represent the size of the image
    first_answer = input("(100 * 25)\nAnswer: ")
    if first_answer != "2500":
        print("Incorrect!")
        exit(0)

    # calculate number of layers
    num_layers = len(layer_map)
    # Iterations per layer
    iterations_per_layer = (num_iterations // num_layers) + 1
    for i in range(num_iterations):
        # Get current layer
        layer = i // iterations_per_layer
        # Get the function for the layer
        func = layer_map[layer]
        # Generate a problem
        answer, problem = func( random.randint( 2, 10 ) )
        # Print the problem
        print( problem )
        # Strip the problem
        problem = problem.replace(" ", "")
        # Get current time
        start_time = time.time()
        # Get user input
        input_answer = input("Answer: ")
        # Strip the input from \r and \n
        input_answer = input_answer.replace("\r", "")
        input_answer = input_answer.replace("\n", "")
        # Get current time
        end_time = time.time()
        # If it took more than 10 seconds, the user is incorrect
        if end_time - start_time > 10:
            print("You took too long to answer!")
            has_got_incorrect = True
            break
        # Calculate time elapsed
        time_elapsed = end_time - start_time
        # Check if the answer is correct
        if answer == int(input_answer):
            print("Correct!")
        else:
            print("Incorrect!")
            # Break and exit the program
            has_got_incorrect = True
            break
    # Check if the user got an incorrect answer
    if has_got_incorrect:
        print("You got something incorrect!")
        # Exit the program
        exit(1)
    print("You got everything correct!")
    # Print current flag
    print('Here is your flag: ' + open('./flag.txt', 'r').read())
    print("Was there something else in there??")