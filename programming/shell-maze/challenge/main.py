#!/usr/bin/env python
import time
import random

PATH_CHAR = 'O'
WALL_CHAR = '#'
PLAYER_CHAT = 'X'

maze_size_x = 20
maze_size_y = 10

current_pos_x = 0
current_pos_y = 0

def is_valid_move( x, y ):
    # Check if the position is out of bounds
    if x < 0 or x >= maze_size_x or y < 0 or y >= maze_size_y:
        return False
    # Check if the position is a wall
    if maze[y][x] == WALL_CHAR:
        return False
    # Return true if the position is valid
    return True

def generate_maze( ):
    # Starting position will always be in the top left corner (0, 0)
    # Ending position will always be in the bottom right corner (maze_size_x - 1, maze_size_y - 1)
    current_x = 0
    current_y = 0
    # Create a 2D array of maze_size_x rows and maze_size_y columns
    maze = [[WALL_CHAR for x in range(maze_size_x)] for y in range(maze_size_y)]
    # Set the starting position to the path character
    maze[current_y][current_x] = PATH_CHAR
    # Loop until we reach the end of the maze
    while current_x != maze_size_x - 1 or current_y != maze_size_y - 1:
        # Check how much space we have to move in each direction
        space_left = current_x
        space_right = maze_size_x - current_x - 1
        # Generate a random number to move left or right
        move = random.randint(-space_left, space_right)
        # Generate a random number to move down
        move_down = random.randint(1, 3)
        # Ensure we don't go too far down
        if current_y + move_down > maze_size_y - 1:
            move_down = maze_size_y - current_y - 1
        # Fill in the path between the current position and the new position, accounting for negative moves
        for i in range(abs(move)):
            # Move right or left depending on change
            if move > 0:
                current_x += 1
            else:
                current_x -= 1
            maze[current_y][current_x] = PATH_CHAR
        # Fill in the downward path
        for i in range(move_down):
            # Move down
            current_y += 1
            maze[current_y][current_x] = PATH_CHAR

    # Return the maze
    return maze

def print_maze(maze):
    # Loop through each row in the maze by max y
    for y in range(maze_size_y):
        # Loop through each column in the maze by max x
        for x in range(maze_size_x):
            # Check if the current position is the player's position
            if x == current_pos_x and y == current_pos_y:
                # Print the player's character
                print(PLAYER_CHAT, end='')
            else:
                # Print the current position
                print(maze[y][x], end='')
        # Print a new line
        print()


if __name__ == "__main__":
    # Print introduction
    print("Welcome to Shell Maze 1!")
    print("Please navigate this maze for me!")
    print("Controls: `<` to move left, '>' to move right, and 'V' to move down.")

    # Loop 50 times
    for i in range(50):
        # Increment maze_size_y
        maze_size_y += 1
        # Reset player position
        current_pos_x = 0
        current_pos_y = 0
        # Generate the maze 
        maze = generate_maze()
        # Print the maze
        print_maze(maze)
        # Start a timing benchmark
        start_time = time.time()
        # Loop until the player reaches the end of the maze
        while current_pos_x != maze_size_x - 1 or current_pos_y != maze_size_y - 1:
            # Get the player's input
            move = input("Move: ")
            # Check if the player wants to move left
            if move == '<':
                # Check if the move is valid
                if is_valid_move(current_pos_x - 1, current_pos_y):
                    # Move left
                    current_pos_x -= 1
                else:
                    # Print an error
                    print("Invalid move!")
                    # Return
                    exit(1)
            # Check if the player wants to move right
            elif move == '>':
                # Check if the move is valid
                if is_valid_move(current_pos_x + 1, current_pos_y):
                    # Move right
                    current_pos_x += 1
                else:
                    # Print an error
                    print("Invalid move!")
                    # Return
                    exit(1)
            # Check if the player wants to move down
            elif move == 'V':
                # Check if the move is valid
                if is_valid_move(current_pos_x, current_pos_y + 1):
                    # Move down
                    current_pos_y += 1
                else:
                    # Print an error
                    print("Invalid move!")
                    # Return
                    exit(1)
            
            # Clear the screen
            print(chr(27) + "[2J")  
            # Print the maze
            print_maze(maze)
        print("\n\nLoading next level...\n")
        # Get the time it took to complete the maze
        end_time = time.time()


    # Print the flag
    print("Congrats! Here's your flag: " + open("flag.txt", "r").read())