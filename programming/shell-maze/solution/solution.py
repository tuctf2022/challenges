# import socket
import socket

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('xxxx', 50003))
    # Receive the first message
    data = s.recv(1024)
    # Convert data to string
    data = data.decode('utf-8')
    # Skip the first three lines
    data = data.split('\n')[3:]
    # The maze is the rest of the data
    data = '\n'.join(data)
    print(data)
    # Player = X
    # Path = O
    # Wall = #
    # End = last O
    
    # Loop 100 times
    for i in range(100):
        maze = data.split('\n')
        # Remove last line
        maze.pop()
        print(maze)
        # Set our starting position
        current_position_x = 0
        current_position_y = 0
        last_move = ''
        # Calculate a path to the end
        while current_position_x != 19 and current_position_y != 10 + i:
            # Check if the position to the right is a path and we did not just move left
            if maze[current_position_y][current_position_x + 1] == 'O' and last_move != '<':
                # Move right
                current_position_x += 1
                last_move = '>'
                # Send the move with a newline
                s.sendall('>'.encode('utf-8') + b'\n\r')
                # Receive the response
                data = s.recv(1024)
                # Convert data to string
                data = data.decode('utf-8')
                continue
            # Check if the position to the left is a path and we did not just move right
            elif maze[current_position_y][current_position_x - 1] == 'O' and last_move != '>':
                # Move left
                current_position_x -= 1
                last_move = '<'
                # Send the move with a newline
                s.sendall('<'.encode('utf-8') + b'\n\r')
                # Receive the response
                data = s.recv(1024)
                # Convert data to string
                data = data.decode('utf-8')
                continue
            # Check if the position below is a path
            elif maze[current_position_y + 1][current_position_x] == 'O':
                # Move down
                current_position_y += 1
                last_move = 'v'
                # Send the move with a newline
                s.sendall('V'.encode('utf-8') + b'\n\r')
                # Receive the response
                data = s.recv(1024)
                # Convert data to string
                data = data.decode('utf-8')
                continue
            print('Found no move!')
            exit()
        print('Finished maze')
        maze = data.split('\n')

