# Shell-Maze
## Difficulty: Easy

## Description
I've got lost in this maze. Can you help me find my way out?

## Flag
TUCTF{1_4m_4_7ru3_n37_7r4v3l3r_357269}