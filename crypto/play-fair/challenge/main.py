#!/usr/bin/env python

def get_part_one( ):
    print(" ")
    print("BEGIN")
    print("DC GR CW GN ND SY CN OP IP CK UP KA IR EF YG UP CW")
    print(" ")
    #Get solution to part 1
    inp = input("Enter Solution With No Spaces: ")
    #Print hint/fail case
    print(" ")
    if inp == "CAESEREIGHTXHEMONOALPHABETICXIPHER":
        print("Good job! Try reverse shifting the entire monoalphabetic cipher :)\n")
    else:
        print("That's a negative Ghost Rider :(")
    main()


def get_part_two( ):
    print("Here's a monoalphabetic cipher, something is off about it...")
    print(" ")
	
    print(")*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!\"#$%&'(")
    print("6e?FP|x3+<%&NrZU,iVK2Xb`}<@vLEa)wt~\"Hy7[mqsp#ko5AJh,gc^n_zMCG'!fl-8{(BQ4uj=:Y;D/]9O.RWT$1I\S*d")
    print(" ")
    print("It also came with this super, securely-encrypted flag")
    print(" ")
    print("ohyg`( (S@B@. ~:(~V{to'\9.|'&aS)xHq{")
    main()

def exit( ): 
    print(" ")
    print("Goodbye!")
    quit()

def main( ):
    # Intro
    print("Welcome to the play-fair challenge!")
    print(" ")
    print("Can you reverse our series of encryption methods to figure out the flag?")
    print(" ")
    # Display 2 options
    print("1. Get part one")
    print("2. Get part two")
    print("3. Exit")
    print(" ")
    # Get the user's choice
    choice = input("Enter your choice: ")
    # If the user wants part one
    if choice == "1":
        get_part_one( )
    # If the user wants part two
    elif choice == "2":
        get_part_two( )
    # Exit 
    elif choice == "3":
        exit()    

    # If the user entered an invalid choice
    else:
        print("Invalid choice!")
    

if __name__ == "__main__":
    main( )
