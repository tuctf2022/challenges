# Solution

Based on the name of the challenge, "Play Fair", the player should be able to deduce that they need to use the given key to create a playfair cipher and decrypt the 
encrypted text. 

This yields a hint on the next step of the challenge, involving decrypting a monoalphabetic cipher to make it actually usable in decrypting the encrypted flag. 

Doing this will get the player close to the solution, however there is still one more step. 

The player should recognize some patterns in the encrypted flag and try different variations of the caesar cipher until the flag is finally revealed.

## Flag
```
TUCTF{s0_m@ny_c!7h3r5}
```
