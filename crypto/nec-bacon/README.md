# Crypto - Never Ending Crypto: Bacon

## Difficulty: Intermediate -> Hard

## Description

Might as well solve a never ending stream of crypto challenges while you get that bacon nice and crispy.

<details>
    <summary>Spoiler warning</summary>

    Flag: `TUCTF{b4c0n_b4c0n_b4c0n_b4c0n_b4c0n_43v3r}`
</details>

## Deployment

Ports: 8888

Environment variables:

* TIMEOUT: the time out in seconds for each decryption attempt (default: 3)
* CHOICE_TIMEOUT: the time out in seconds for giving text or choosing level (default: 60)

Example usage:

```bash
docker run -d -p 8888:8888 -e "TIMEOUT=5" --name bacon bacon:tuctf2022
```
