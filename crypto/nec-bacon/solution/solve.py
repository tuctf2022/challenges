#!/usr/bin/env python3

import sys, string

from pwn import *

from bacon import Bacon


def level_zero_decrypt(enc):
    return Bacon.from_bacon_string(enc).decipher()


def level_one_decrypt(enc):
    return Bacon.from_bacon_string(enc).flip().decipher()


def level_two_decrypt(enc):
    revkey = 20
    cipher = Bacon.from_bacon_string(enc).decipher()
    return "".join(
        [chr(((ord(c) - ord("A")) + revkey) % 26 + ord("A")) for c in cipher]
    )


def level_three_decrypt(enc):
    k = "BACON"

    cipher = Bacon.from_bacon_string(enc).decipher()

    # normal alphabet
    na = string.ascii_uppercase
    # keyed alphabet
    ka = "".join(OrderedDict.fromkeys(k + na))
    assert len(ka) == len(na)

    keymap = dict(zip(list(ka), list(na)))
    keymap[" "] = " "

    return "".join([keymap[c] for c in cipher])


def level_four_decrypt(enc):
    return Bacon.from_bacon_string(enc).flip_chars().decipher()


def level_five_decrypt(enc):
    k = "TOFU"

    cipher = Bacon.from_bacon_string(enc).decipher()
    ki = [(26 - (ord(c) - ord("A"))) for c in k]

    o = ""
    for i in range(len(cipher)):
        o += chr(((ord(cipher[i]) - ord("A")) + ki[i % len(ki)]) % 26 + ord("A"))

    return o


def level_six_decrypt(enc):
    cipher = Bacon.from_bacon_string(enc).decipher()
    l = len(cipher)
    ll = l // 3
    lm = l % 3
    if lm == 0:
        o = ""
        for i in range(ll):
            o += cipher[i::ll]
        return o

    ll += 1
    need = ll * 3
    fill = 3 - (need - l)
    mat = [""] * ll
    ci = 0
    col = 0
    row = 0
    for i in range(need):
        col = i % ll
        if col == 0 and i != 0:
            row += 1
        if row < fill:
            mat[col] += cipher[ci]
            ci += 1
        else:
            if col == ll - 1:
                continue
            else:
                mat[col] += cipher[ci]
                ci += 1
        if ci == l:
            break
    o = "".join(mat)
    return o


def level_seven_decrypt(enc):
    cipher = Bacon.from_bacon_string(enc).flip().decipher()
    revkey = 26 - (2999 % 26)
    return "".join(
        [chr(((ord(c) - ord("A")) + revkey) % 26 + ord("A")) for c in cipher]
    )


def level_eight_decrypt(enc):
    cipher = Bacon.from_bacon_string(enc).flip_chars().decipher()

    k = "TASTY"
    ki = [(26 - (ord(c) - ord("A"))) for c in k]

    o = ""
    for i in range(len(cipher)):
        o += chr(((ord(cipher[i]) - ord("A")) + ki[i % len(ki)]) % 26 + ord("A"))

    return o


def level_nine_decrypt(enc):
    k = "CRISPY"

    cipher = Bacon.from_bacon_string(enc).flip_chars().flip().decipher()

    l = len(cipher)
    ll = l // 5
    lm = l % 5
    if lm == 0:
        o = ""
        for i in range(ll):
            o += cipher[i::ll]
    else:
        ll += 1
        need = ll * 5
        fill = 5 - (need - l)
        mat = [""] * ll
        ci = 0
        col = 0
        row = 0
        for i in range(need):
            col = i % ll
            if col == 0 and i != 0:
                row += 1
            if row < fill:
                mat[col] += cipher[ci]
                ci += 1
            else:
                if col == ll - 1:
                    continue
                else:
                    mat[col] += cipher[ci]
                    ci += 1
            if ci == l:
                break
        o = "".join(mat)

    # normal alphabet
    na = string.ascii_uppercase
    # keyed alphabet
    ka = "".join(OrderedDict.fromkeys(k + na))
    assert len(ka) == len(na)

    keymap = dict(zip(list(ka), list(na)))
    keymap[" "] = " "

    return "".join([keymap[c] for c in o])


# TODO Change address
r = remote("bacon", "8888")


# Level 0

r.recvuntil("What level? ".encode("utf-8"))
r.send("0\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_zero_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 1

r.recvuntil("What level? ".encode("utf-8"))
r.send("1\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_one_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 2

r.recvuntil("What level? ".encode("utf-8"))
r.send("2\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_two_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 3

r.recvuntil("What level? ".encode("utf-8"))
r.send("3\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_three_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 4

r.recvuntil("What level? ".encode("utf-8"))
r.send("4\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_four_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 5

r.recvuntil("What level? ".encode("utf-8"))
r.send("5\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_five_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 6

r.recvuntil("What level? ".encode("utf-8"))
r.send("6\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_six_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 7

r.recvuntil("What level? ".encode("utf-8"))
r.send("7\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_seven_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 8

r.recvuntil("What level? ".encode("utf-8"))
r.send("8\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_eight_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))


# Level 9

r.recvuntil("What level? ".encode("utf-8"))
r.send("9\n".encode("utf-8"))

r.recvuntil("Give me text:\n".encode("utf-8"))
r.send("text\n".encode("utf-8"))

for i in range(50):
    r.recvuntil("Decrypt ".encode("utf-8"))
    m = r.recvuntil("\n".encode("utf-8")).decode("utf-8")[:-1]
    d = level_nine_decrypt(m)
    r.send(f"{d}\n".encode("utf-8"))

r.recvuntil("Here's your flag:\n".encode("utf-8"))

flag = r.recvuntil("}\n".encode("utf-8")).decode("utf-8")[:-1]

FLAG = "TUCTF{b4c0n_b4c0n_b4c0n_b4c0n_b4c0n_43v3r}"

r.close()

if flag == FLAG:
    print("gucchi")
    sys.exit(0)
print("no flag")
sys.exit(1)
