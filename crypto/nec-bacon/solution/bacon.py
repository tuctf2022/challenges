ORD_A = ord("A")


class Bacon(object):
    def __init__(self, bacon):
        self.bacon = bacon

    def __str__(self):
        return "".join(self.bacon)

    @classmethod
    def encipher(cls, plaintext):
        return cls([Bacon.to_bacon(c) for c in plaintext.upper()])

    @classmethod
    def from_bacon_string(cls, ciphertext):
        ciphertext = ciphertext.upper()
        return cls([ciphertext[i : i + 5] for i in range(0, len(ciphertext), 5)])

    def decipher(self):
        return "".join([Bacon.from_bacon(b) for b in self.bacon])

    def flip(self):
        self.bacon = [Bacon.flip_bacon(b) for b in self.bacon]
        return self

    def flip_chars(self):
        self.bacon = [Bacon.flip_char(b) for b in self.bacon]
        return self

    @staticmethod
    def from_bacon(b):
        i = int("".join(map(lambda c: "0" if c == "A" else "1", b)), 2)
        return chr(i + ORD_A)

    @staticmethod
    def to_bacon(c):
        i = format(ord(c) - ORD_A, "b").zfill(5)
        return "".join(map(lambda c: "A" if c == "0" else "B", i))

    @staticmethod
    def flip_bacon(b):
        return "".join(map(lambda c: "B" if c == "A" else "A", b))

    @staticmethod
    def flip_char(b):
        return b[::-1]


if __name__ == "__main__":
    import sys

    enc = Bacon.encipher(sys.argv[1])
    print(enc)
    # print(enc.flip())
    print(enc.flip_chars())
    # print(Bacon.from_bacon_string(enc).decipher())
