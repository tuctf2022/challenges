# Never Ending Crypto: Bacon -- TUCTF2022

The challenge prompts you for input and then gives you the ciphertext. The intended solution is to craft input in such a way that reveals what the cipher is doing.

Solve script: `solve.py`

There's also a Dockerfile that can be used, so you don't have to set up pwntools.

<details>
    <summary>Spoiler warning</summary>

    Flag: `TUCTF{b4c0n_b4c0n_b4c0n_b4c0n_b4c0n_43v3r}`
</details>

## The Ciphers

0. The Unique Baconian Cipher
1. "Flips" the Baconian Cipher. A->B and B->A
2. Caesar with key 6 -> Bacon
3. Keyed Caesar cipher with key `BACON` -> Bacon
4. Reverses each character of the Bacon cipher. Example (with spaces for visibility): test -> BAABB AABAA BAABA BAABB -> BBAAB AABAA ABAAB BBAAB
5. Vigenere with key `TOFU` -> Bacon
6. Matrix cipher with key of 3
7. Caesar with key 2999 then "flip" the Bacon (like Level 1)
8. Vigenere Cipher with key `TASTY` then reverse characters (like level 4)
9. The BACON level: Keyed Caesar with key `CRYSPY` -> Matrix with key of 5 -> "flip" -> reverse characters
