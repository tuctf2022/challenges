#!/usr/bin/env python

import socket, sys, random, string, os

from enum import Enum

from collections import OrderedDict

from select import select

from abc import ABC, abstractmethod

from _thread import *

from bacon import Bacon


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class Answer_Result(Enum):
    INCORRECT = 0
    SLOW = 1
    CORRECT = 2


NUMBER_OF_ROUNDS = 50

VIGENERE_KEY = "TOFU"

VIGENERETWO_KEY = "TASTY"

KEYED_CAESAR_KEY = "BACON"
KEYED_CAESAR2_KEY = "CRISPY"

TIMEOUT = int(os.getenv("TIMEOUT", 3))
CHOICE_TIMEOUT = int(os.getenv("CHOICE_TIMEOUT", 60))


INTRO = """
Welcome to the CRISPER. Crypto never ends, but at least you can have bacon this time.

You can complete these levels in any order, but I recommend you do them in order, as they get harder as they go.


"""


END = """
Congratulations on BACON.

Here's your flag:\n"""

FLAG = "TUCTF{b4c0n_b4c0n_b4c0n_b4c0n_b4c0n_43v3r}"

with open("wordlist.txt", "r") as f:
    WORDS = [line.rstrip("\n").encode("utf-8") for line in f.readlines()]


GOOD = [
    "Nice job!",
    "Way to go!",
    "Yeah! You do that Crypto!",
    "Ayyyyy",
    "Whoop whoop!",
    "That was adequate.",
]

BAD = ["No.", "Wrong.", "You can do better.", "Inadequate.", "Try again."]


# b = Bacon object
# s = str
class Ciphers:
    @staticmethod
    def Bacon(s):
        return Bacon.encipher(s)

    @staticmethod
    def Caesar(s, k):
        o = ""
        k = k % 26
        for c in s:
            c = chr(((ord(c) - ord("A")) + k) % 26 + ord("A"))
            o += c
        return o

    @staticmethod
    def KeyedCaesar(s, k):
        # normal alphabet
        na = string.ascii_uppercase
        # keyed alphabet
        ka = "".join(OrderedDict.fromkeys(k + na))
        assert len(ka) == len(na)

        keymap = dict(zip(list(na), list(ka)))
        keymap[" "] = " "

        return "".join([keymap[c] for c in s])

    @staticmethod
    def Rot13(s):
        return Ciphers.Caesar(s, 13)

    @staticmethod
    def Matrix(s):
        o = ""
        for i in range(3):
            o += s[i::3]
        return o

    @staticmethod
    def FiveMatrix(s):
        o = ""
        for i in range(5):
            o += s[i::5]
        return o

    @staticmethod
    def Vigenere(s, k):
        ki = [(ord(c) - ord("A")) for c in k]
        o = ""
        for i in range(len(s)):
            o += chr(((ord(s[i]) - ord("A")) + ki[i % len(ki)]) % 26 + ord("A"))
        return o


class Part:

    response = None

    def __init__(self, prompt, answer, prev_part=None):
        self.prompt = prompt
        self.answer = answer
        self.prev_part = prev_part

    def getResponse(self):
        return self.response

    def send_prompt(self):
        sys.stdout.write(self.prompt)
        sys.stdout.flush()

    def send_good_response(self):
        global GOOD
        r = "\n" + random.choice(GOOD) + "\n\n"
        sys.stdout.write(r)
        sys.stdout.flush()

    def send_bad_response(self):
        r = "\n" + random.choice(BAD) + "\n\n"
        sys.stdout.write(r)
        sys.stdout.flush()

    def check_answer(self):
        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()
            return Answer_Result.SLOW

        data = data.rstrip("\n").upper()

        a = data
        self.response = data
        if self.prev_part is not None:
            if a == self.answer and a != self.prev_part.getResponse():
                return Answer_Result.CORRECT
            return Answer_Result.INCORRECT
        if a == self.answer:
            return Answer_Result.CORRECT
        else:
            return Answer_Result.INCORRECT


def check_word(word):
    for c in word:
        o = ord(c)
        if o < ord("A") or o > ord("Z"):
            return False
    return True


def timeoutfunc():
    sys.stdout.write("Too slow!\n\n")
    sys.stdout.flush()


class Level(ABC):
    parts = []
    name = ""
    intro = ""
    status = f"{bcolors.WARNING}Incomplete{bcolors.ENDC}"
    number = 0

    @abstractmethod
    def cipher(self, word):
        return word

    def print_intro(self):
        sys.stdout.write(f"\nLevel {self.number}: {self.name}\n\n")
        sys.stdout.write(f"{self.intro}\n")
        sys.stdout.flush()

    def gen_parts(self):
        global NUMBER_OF_ROUNDS
        global WORDS
        self.parts = []
        for i in range(NUMBER_OF_ROUNDS):
            word = random.choice(WORDS).decode("utf-8").upper()
            while not check_word(word) or len(word) < 7:
                word = random.choice(WORDS).decode("utf-8").upper()
            ciphertext = self.cipher(word)
            self.parts.append(Part("Decrypt {}\n".format(ciphertext), word))

    def solved(self):
        self.status = f"{bcolors.OKGREEN}Solved{bcolors.ENDC}"

    def run_level(self):
        i = 0
        sys.stdout.write("Give me text:\n")
        sys.stdout.flush()
        rlist, _, _ = select([sys.stdin], [], [], CHOICE_TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()
            sys.exit(0)

        data = data.rstrip("\n").upper()

        sys.stdout.write("{} encrypted is {}\n\n".format(data, self.cipher(data)))
        sys.stdout.flush()
        while i < len(self.parts):
            part = self.parts[i]

            part.send_prompt()

            res = part.check_answer()

            if res == Answer_Result.CORRECT:
                part.send_good_response()
                i += 1
            elif res == Answer_Result.INCORRECT:
                part.send_bad_response()
            elif res == Answer_Result.SLOW:
                return False
        self.solved()
        sys.stdout.write("Congratulations! You beat Level {}!\n\n".format(self.number))
        sys.stdout.flush()
        return True


# Bacon
class Level0(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(word))

    def __init__(self):
        self.number = 0
        self.name = "Cookin"


class Level1(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(word).flip())

    def __init__(self):
        self.number = 1
        self.name = "Flippin"


class Level2(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(Ciphers.Caesar(word, 6)))

    def __init__(self):
        self.number = 2
        self.name = "Kevin"


class Level3(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(Ciphers.KeyedCaesar(word, KEYED_CAESAR_KEY)))

    def __init__(self):
        self.number = 3
        self.name = "Vibin"


class Level4(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(word).flip_chars())

    def __init__(self):
        self.number = 4
        self.name = "Revin"


class Level5(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(Ciphers.Vigenere(word, VIGENERE_KEY)))

    def __init__(self):
        self.number = 5
        self.name = "Vegan"


class Level6(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(Ciphers.Matrix(word)))

    def __init__(self):
        self.number = 6
        self.name = "Turnin"


class Level7(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(Ciphers.Caesar(word, 2999)).flip())

    def __init__(self):
        self.number = 7
        self.name = "Fryin"
        self.intro = "Now that we've introduced some of the ciphers, let's really start cooking.\n"


class Level8(Level):
    def cipher(self, word):
        return str(Ciphers.Bacon(Ciphers.Vigenere(word, VIGENERETWO_KEY)).flip_chars())

    def __init__(self):
        self.number = 8
        self.name = "Crispin"


class Level9(Level):
    def cipher(self, word):
        return str(
            Ciphers.Bacon(
                Ciphers.FiveMatrix(Ciphers.KeyedCaesar(word, KEYED_CAESAR2_KEY))
            )
            .flip()
            .flip_chars()
        )

    def __init__(self):
        self.number = 9
        self.name = "BACON"
        self.intro = "ARE YOU READY FOR SOME BACON?!\n"


Levels = [
    Level0,
    Level1,
    Level2,
    Level3,
    Level4,
    Level5,
    Level6,
    Level7,
    Level8,
    Level9,
]


cont = True
while cont:
    try:
        sys.stdout.write(INTRO)
        sys.stdout.flush()

        level_tups = list(enumerate(map(lambda l: l(), Levels)))
        num_levels = len(level_tups)

        num_solves = 0

        while num_solves != num_levels:
            for i, level in level_tups:
                sys.stdout.write(
                    "{:<20s}\t\t{:>12s}\n".format(f"{i}: {level.name}", level.status)
                )

            sys.stdout.write("\nWhat level? ")
            sys.stdout.flush()

            rlist, _, _ = select([sys.stdin], [], [], CHOICE_TIMEOUT)
            if rlist:
                data = sys.stdin.readline()
            else:
                timeoutfunc()
                sys.exit(0)

            try:
                i = int(data.rstrip())
                if i < 0 or i >= num_levels:
                    sys.stdout.write("\nNot a level\n\n")
                    sys.stdout.flush()
                    continue
                l = level_tups[i][1]
                if l.status == "Solved":
                    sys.stdout.write("\nAlready solved\n\n")
                    sys.stdout.flush()
                    continue
                sys.stdout.write("\n\n")
                l.print_intro()
                l.gen_parts()
                solved = l.run_level()
                if solved:
                    num_solves += 1
            except ValueError:
                sys.stdout.write("\nNot a number\n\n")
                sys.stdout.flush()
            except EOFError:
                cont = False
                break

        if cont:
            sys.stdout.write(END + FLAG + "\n\n")
            sys.stdout.flush()
            cont = False
    except KeyboardInterrupt:
        sys.stdout.write("\n")
        sys.stdout.flush()
        sys.exit(0)
