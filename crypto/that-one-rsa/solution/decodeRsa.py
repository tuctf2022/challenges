from Crypto.Util.number import *
from decimal import *

Ns = [
#n1
#n2
#n3
#n4
#n5...
]

Cs = [
#c1
#c2
#c3
#c4
#c5...
]

e = 5

n_max_idx = max(range(len(Ns)), key=Ns.__getitem__)

n_max = Ns[n_max_idx]
c_max = Cs[n_max_idx]

# print(f"Max is at pos {n_max_idx}: \n{n_max}\n\n{c_max}")

sum = c_max

done = False

iter = 0
while not done:
   sum += n_max
   iter+=1
   if iter % 1000000 == 0:
      print(iter)
   # print(sum)
   # print("\n\n")
   for i in range(len(Cs)):
      done = True
      if (sum - Cs[i]) % Ns[i] != 0:
         done = False

getcontext().prec = len(str(sum))

dc = Decimal(sum)

one_fifth = Decimal(1)/Decimal(5)

print(m := dc**one_fifth)

print(long_to_bytes(int(m)))