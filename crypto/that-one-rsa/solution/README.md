# Solution

This challenge is easily solvable using the Chinese Remainder Theorem. If you have access to at least `e` pairs of N/C values for the same message `m`, it forms a system of equations that has one solution, with that solution being the original message `m`.  Since `e` is so small in this challenge (5), we can easily request `e` pairs of N/C values. One possible solution to this system is as follows:

1. Find the highest `N` value and its associated `C` value. These will become `N_max` and `C_max`
2. Add `N_max` and `C_max` into an accumulator, `sum`
3. For each `C_i` in the list of `C` values:
   - Subtract `C_i` from the `sum`
   - Check if that difference is divisible by the associated `N_i`
   - If this condition holds true for every `C_i`, `N_i` pair in the list of `N` values, then `sum` is equal to `m^e`
   - If this condition is false for any `C_i`, `N_i` pair, add `N_max` to `sum` and repeat
4. Once `m^e` has been found, taking the `e`th root (in this case the 5th root) will yield the original plaintext
5. The flag was encoded using Python's `Crypto.Util.number.bytes_to_long()` function, so reversing it yields the flag


## Flag
```
TUCTF{0bl1g4t0ry_RSA_chall_l0l}
```
