# Crypto - That One RSA Challenge

## Difficulty: Easy

## Description
Every CTF needs an RSA challenge, right? This one is pretty easy, but it's a good warmup for the harder ones.