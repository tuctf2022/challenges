#!/usr/bin/env python
from Crypto.Util.number import *
import re

flag = "TUCTF{0bl1g4t0ry_RSA_chall_l0l}"

usedPQ = []

NUM = 5

def invalidN(n):
   m = bytes_to_long(bytes(flag, 'UTF-8'))
   me = pow(m, NUM)
   return me // n < 2_000_000

def getUniquePrime(n):
   while (gen := getPrime(n)) in usedPQ:
      gen = getPrime(n)
   return gen

class RSA():
   def __init__(self):
      self.e = NUM   

      self.p = getUniquePrime(605)
      self.q = getUniquePrime(605)

      self.n = self.p * self.q
      self.phi = (self.p - 1) * (self.q - 1)

      try:
         self.d = pow(self.e, -1, self.phi)
      except:
         self.__init__()
      
      if(GCD(self.e, self.phi) != 1) or invalidN(self.n):
         self.__init__()


   def encrypt(self, m: int):
      return pow(m, self.e, self.n)

   def decrypt(self, c: int):
      return pow(c, self.d, self.n)

def main_old():
   rsas = []
   for i in range(NUM):
      rsas.append(RSA())
   
   Ns = []
   Cs = []
   for r in rsas:
      m = bytes_to_long(bytes(flag, 'UTF-8'))
      c = r.encrypt(m)
      Ns.append(r.n)
      Cs.append(c)
      print(f"m: {m}\nn: {r.n}\ne: {r.e}\nc: {c}\niters: {pow(m, r.e) // r.n}\n\n")
   
   # print(f"[{','.join(str(n) for n in Ns)}]")
   # print(f"[{','.join(str(c) for c in Cs)}]")
   # enc = rsa.encrypt(bytes_to_long(bytes(flag, "UTF-8")))
   # print(r.encrypt(bytes_to_long(bytes(flag, 'UTF-8'))))
   # print(f"{enc}\n")
   # print(f"{long_to_bytes(rsa.decrypt(enc)).decode('UTF-8')}")


def main():
   print("That one RSA Challenge:")
   while(True):
      opt = re.sub("[^0-9]", "", input("\nOptions:\n1) Generate RSA Message\n2) Convert message to text\n3) Exit\n"))
      if opt == "":
         opt = 99
      opt = int(opt)
      if opt == 3:
         exit(0)
      elif opt == 1:
         print("\nGenerating RSA...\n")
         r = RSA()
         m = bytes_to_long(bytes(flag, 'UTF-8'))
         c = r.encrypt(m)
         print(f"n:{r.n}\n\ne:{r.e}\n\nc:{c}")
      elif opt == 2:
         m = input("Enter your number:\n")
         print(long_to_bytes(int(m)))
      elif opt == 4:
         print("A more sinister fourth option....\n\n")
         print("""
                                 .-.
                _.--\"\"\"\".o/         .-.-._
             __'   .\"\"\"; {        _J ,__  `.
            ; o\.-.`._.'J;       ; /  `- /  ;
            `--i`". `\" .';       `._ __.'   |
                \  `\"\"\"   \         `;      :
                 `."-.     ;     ____/     /
                   `-.`     `-.-'    `"-..'
     ___              `;__.-'"           `.
  .-{_  `--._         /.-"                 `-.
 /    ""T    ""---...'  _.-""   \"""-.         `.
;       /                 __.-"".    `.         `,             _..
 \     /            __.-""       '.    \          `.,__      .'L' }
  `---"`-.__    __."    .-.       j     `.         :   `.  .' ,' /
            ""\"\"       /   \     :        `.       |     F' \   ;
                      ;     `-._,L_,-""-.   `-,    ;     `   ; /
                       `.       7        `-._  `.__/_        \/
                         \     _;            \  _.'  `-.     /
                          `---" `.___,,      ;""        \  .'
                                    _/       ;           `"
                                 .-"     _,-'
                                {       "";
                                 ;-.____.'`.
                                  `.  \ '.  :
                                    \  : : /
                                     `':/ `
         """         )
         #author: fsc
      else:
         print("Invalid Option")
         continue

if __name__ == "__main__":
	main()