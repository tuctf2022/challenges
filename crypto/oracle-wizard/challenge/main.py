#!/usr/bin/env python
import string
import sys
import os

# Single byte encryption algorithm
def e( a, b ):
    return hex( a ^ b ) # TODO: Make this good lol

# Enter the message to encrypt: TUCTF{cu7_m3_r16h7_d0wn_7h3_m1ddl3_42568}
# Enter the key to encrypt with: z00m_z00m_z00m
# Encrypted message: 2c6d712b712a3b5f1e7b2c5271e71f93b331e1e2c2c717971793b3b6d2c336932697f1b5b3e6a14364939b1727a5468671e3d423c4075622166327634a2d648483e65636c6fcb2533cb23e1e67c676529177f53d343c4c4ddc699f5cec038b7409d6a8659cbf8ee243d914a6513c51c323496992c50713d790

# Interlace two lists of same length
def interlace( a, b ):
    # Create a new list to store the interlaced lists
    interlaced = [ ]
    # Iterate through the lists
    for i in range( 0, len( a ) ):
        # Append the first list's element
        interlaced.append( a[ i ] )
        # Append the second list's element
        interlaced.append( b[ i ] )
    # Return the interlaced list
    return interlaced

# Perform opposite of interlace
def deinterlace( a ):
    # Create a new list to store the deinterlaced list
    deinterlaced_a = [ ]
    deinterlaced_b = [ ]
    # Iterate through the list
    for i in range( 0, len( a ) ):
        # If the index is even
        if i % 2 == 0:
            # Append the element to the list
            deinterlaced_a.append( a[ i ] )
        else:
            # Append the element to the list
            deinterlaced_b.append( a[ i ] )
    # Return both lists
    return deinterlaced_a, deinterlaced_b

# Our encryption routine
def encrypt( message: string, key: string ):
    # Get the length of the message
    length = len( message )
    # If message is greater than 128 characters, return an error
    if length > 128:
        return 'Message too long!'
    # If the length of the message is less than 128 characters
    if length < 128:
        # Pad the message with As
        message += 'A' * ( 128 - length )
    # Convert message and key to list of hex values
    message = [ hex( ord( c ) ) for c in message ]
    key = [ hex( ord( c ) ) for c in key ]
    # Inverse the key and message lists
    message.reverse( )
    key.reverse( )

    # Split message in half
    message1 = message[ :64 ]
    message2 = message[ 64: ]

    # Preserve original key
    preserved_key = key

    # Create a new list to store the encrypted message
    encrypted = [ ]
    # Iterate through the first half of the message
    for i in range( 0, 64 ):
        # Get the current character
        c = message1[ i ]
        # Get the current key, respecting the key's length
        k = key[ i % len( key ) ]
        # Encrypt the character
        encrypted.append( e( int( c, 16 ), int( k, 16 ) ) )
        # Update the key by XORing it with the current character and adding the current iteration, capped at 255
        key[ i % len( key ) ] = hex( int( k, 16 ) ^ int( c, 16 ) + i % 255 )
    key = preserved_key
    # Iterate through the second half of the message
    for i in range( 0, 64 ):
        # Get the current character
        c = message2[ i ]
        # Get the current key
        k = key[ i % len( key ) ]
        # Encrypt the character
        encrypted.append( e( int( c, 16 ), int( k, 16 ) ) )
        # Update the key by XORing it with the current character and doing other stuff
        key[ i % len( key ) ] = hex( int( k, 16 ) ^ int( c, 16 ) + i % 255 )
    # Interlace the two halves of the encrypted message
    encrypted = interlace( encrypted[ :64 ], encrypted[ 64: ] )

    # Convert the encrypted message to a single hex string without 0x and spaces
    encrypted = ''.join( encrypted ).replace( '0x', '' ).replace( ' ', '' )
    # Return the encrypted message
    return encrypted

# Our decryption routine
def decrypt( encrypted, key ):
    # De interlace the encrypted message
    first_half, second_half = deinterlace( encrypted )
    # Iterate through the first half of the encrypted message
    for i in range( 0, 64 ):
        # Get the current character
        c = first_half[ i ]
        # Get the current key
        k = key[ i % len( key ) ]
        # Decrypt the character
        first_half[ i ] = hex( int( c, 16 ) ^ int( k, 16 ) )
        # Update the key
        key[ i % len( key ) ] = hex( int( k, 16 ) ^ int( c, 16 ) + i % 255 )
    # Iterate through the second half of the encrypted message
    for i in range( 0, 64 ):
        # Get the current character
        c = second_half[ i ]
        # Get the current key
        k = key[ i % len( key ) ]
        # Decrypt the character
        second_half[ i ] = hex( int( c, 16 ) ^ int( k, 16 ) )
        # Update the key
        key[ i % len( key ) ] = hex( int( k, 16 ) ^ int( c, 16 ) + i % 255 )
    # Reverse the first half of the encrypted message
    first_half.reverse( )
    # Reverse the second half of the encrypted message
    second_half.reverse( )
    # Join the two halves of the encrypted message
    encrypted = first_half + second_half
    # Convert the encrypted message to a string
    encrypted = ''.join( encrypted )
    # Return the encrypted message
    return encrypted

# Our main subroutine
def main():
    # Our intro message
    print( 'Welcome to `known-encryption`!' )
    print( 'Good luck cracking my secret encryption technique!' )
    # Print our options
    print( '1. Encrypt a message' )
    print( '2. Get message' )
    print( '3. Decrypt a message' )
    print( '4. Exit' )
    # Get the user's choice
    choice = input( 'Enter your choice: ' )
    # If the user wants to encrypt a message
    if choice == '1':
        # Get the message to encrypt
        message = input( 'Enter the message to encrypt: ' )
        # Get the key to encrypt with
        key = input( 'Enter the key to encrypt with: ' )
        # Encrypt the message
        encrypted = encrypt( message, key )
        # Print the encrypted message
        print( 'Encrypted message: ' + encrypted )
    # If the user wants to get the message
    elif choice == '2':
        # Print the message
        print( 'Message: ' + "2c6d712b712a3b5f1e7b2c5271e71f93b331e1e2c2c717971793b3b6d2c336932697f1b5b3e6a14364939b1727a5468671e3d423c4075622166327634a2d648483e65636c6fcb2533cb23e1e67c676529177f53d343c4c4ddc699f5cec038b7409d6a8659cbf8ee243d914a6513c51c323496992c50713d790" )
    # If the user wants to decrypt a message
    elif choice == '3':
        # Get the encrypted message
        encrypted = input( 'Enter the encrypted message: ' )
        # Get the key to decrypt with
        key = input( 'Enter the key to decrypt with: ' )
        # Decrypt the message
        decrypted = decrypt( encrypted, key )
        # Print the decrypted message
        print( 'Decrypted message: ' + decrypted )
    # If the user wants to exit
    elif choice == '4':
        # Exit the program
        sys.exit( 0 )
    
    # If the user entered an invalid choice
    else:
        # Print an error message
        print( 'Invalid choice!' )
    # Wait for the user to press enter
    input( 'Press enter to continue...' )
    # Clear the screen
    os.system( 'clear' )
    # Call main again
    main( )

if __name__ == '__main__':
    # Call main
    main( )