# Solution

Oracle Wizard must be solved only using observed changes in ciphertext and messages from the server.
Key elements: 'Message is too long' notification, showing oyu the max size of a message is 128 and alluding to the use of padding.
The basic xor cipher is simply to find by using same key and message.

## Password
z00m_z00m_z00m

## Flag
TUCTF{cu7_m3_r16h7_d0wn_7h3_m1ddl3_42568}