# More_Effort

## Description
I have designed a revised RSA algorithm which I believe it’s more secure. I am confident you cannot compromise my message with the p value. Meanwhile, it’s time-consuming to complete the encryption. You may need more effort to brute force it as it takes much longer to complete the encryption. 