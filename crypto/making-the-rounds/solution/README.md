# Solution

Based on the name of the challenge and the description which mentions "something about it seems incomplete", the player should be able to determine that the challenge involves something regarding the 'rounds' which the AES implementation takes.

The player must then modify or write an AES implementation and view the ciphertext after each "round" to find when the ciphertext is readable.
The plaintext is a gif file with the header signature modified to be  "JIF" instead of "GIF". The player must fix this signature and then convert the braille text in the image to ASCII to get the flag.

## Flag
```
TUCTF{7h3_r0und5_4r3_4_l177l3_5h0r7_2750472}
```