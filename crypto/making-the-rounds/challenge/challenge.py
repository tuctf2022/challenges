import os

# Import AES class from aes.py
from aes import AES, encrypt, decrypt

# 16-byte key
key = os.urandom(16)
# 15-byte iv
iv = os.urandom(16)

# Flag
#flag = b"TUCTF{7h3_r0und5_4r3_4_l177l3_5h0r7_2750472}"

# Open flag.jif and read data
with open("flag.jif", "rb") as f:
    data = f.read()
    # Encrypt flag
    ciphertext = AES(key).encrypt_cbc(data, iv)

    # Decrypt ciphertext
    plaintext = AES(key).decrypt_cbc(ciphertext, iv)

    # Print data
    print("Key: " + key.hex())
    print("IV: " + iv.hex())
    print("Ciphertext: " + ciphertext.hex())
    # write  plaintext to flag.dec.gif
    with open("flag.dec.jif", "wb") as f:
        f.write(plaintext)