#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 27 22:40:31 2022

@author: ens-admin-wep6483
"""

import base64


flag = 'TUCTF{thisisawelcomemessagefromdatasecurityandcryptographyclass}'

# Double Rail Fence
n_batch = int(len(flag) /4)
rf_cipher = ''
for i in range(4):
    for j in range(n_batch):
        rf_cipher += flag[j*4+i]
        

# encoding: ascii -> base64-> hex
rf_cipher_bytes = rf_cipher.encode('ascii')
base64_cipher = base64.b64encode(rf_cipher_bytes)
enc_str = "0x" + ''.join('{:02x}'.format(x) for x in base64_cipher)

f = open("TUCTF02.enc", "w")
f.write(enc_str)
f.close()
    