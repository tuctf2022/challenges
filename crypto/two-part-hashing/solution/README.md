# Solution

## Part 1: PDF
The PDF is encrypted using a 4-byte xor key.
The PDF is a downloaded wikipedia page on cryptographic hashing methods. 
The metadata contains a netcat address and a fake flag.
There is a secret metadata element that says to look closer at the text.
Upon inspection you may find the python code to the fletcher-16 cipher.

## Part 2: Server Oracle
The server oracle gives you a hashed flag and an oracle to help you hash things. You are quick to notice that it only hashes data two characters at a time.
Using this and the hash method found in the PDF, you can reconstruct the flag.