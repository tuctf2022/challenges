import time
import os

from sympy import re

hidden_flag = "fda9da9708c1d7a2d4a038cbdfaa3acc059cd29dd09fcb95049d4ed69f6ba36e9c6a53d5"

# Fletcher-16 checksum
def fletcher16( data ):
    sum1 = 0
    sum2 = 0
    for c in data:
        sum1 = ( sum1 + ord( c ) ) % 255
        sum2 = ( sum2 + sum1 ) % 255
    return ( sum2 << 8 ) | sum1
    

# Partition string
def data_partition( data ):
    # Split data every 4 characters
    data = [ data[i:i+4] for i in range( 0, len( data ), 4 ) ]
    return data

# Alphanumeric characters
alphanumeric = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{}_"

# Get all collisions for a given 4 byte string
def get_collisions( data ):

    # List of all collisions
    collisions = []
    # Try each character
    for c in alphanumeric:
        # Try each character
        for c2 in alphanumeric:
            # Generate hash
            hash = "%04x" % fletcher16( c + c2 )
            # Check if hash matches
            if hash == data:
                # Add to collisions list
                collisions.append( c + c2 )
    return collisions

# Partition data
data = data_partition( hidden_flag )

# Crack each partition
cracked = ""
for d in data:
    # Get collisions
    collisions = get_collisions( d )
    # Add to cracked string
    cracked += collisions[0]

print( "Cracked: " + cracked )