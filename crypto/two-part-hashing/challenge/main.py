#!/usr/bin/env python
import time
from hasher import hash

# Get flag from 'flag.txt'
flag = hash( open( 'flag.txt', 'r' ).read( ).strip( ) )

# Main function
def main( ):
    # We give the user a oracle/blackbox to hash anything they want
    print( '[ + ] Welcome to the hash oracle!' )
    print( '[ + ] Enter anything you want to hash, and I\'ll hash it for you!\n' )
    # Get user option 
    # 1 = Hash
    # 2 = Get hidden flag
    # 3 = Exit
    print( '[ + ] 1. Hash' )
    print( '[ + ] 2. Get hidden flag' )
    print( '[ + ] 3. Exit' )
    option = input( '[ + ] Enter your option: ' )
    # Hash option
    if option == '1':
        # Get user data
        data = input( '[ + ] Enter data to hash: ' )
        print( '[ + ] Hashing...' )
        # Sleep 1 second to simulate a slow hash
        time.sleep( 1 )
        # Hash data
        hashed_data = hash( data )
        # Print hash
        print( '[ + ] Hashed data: %s' % hashed_data )
    # Get hidden flag option
    elif option == '2':
       # Print hidden flag
         print( '[ + ] Hidden flag: %s\n' % flag )
    # Exit option
    elif option == '3':
        # Exit
        exit( )
    # Invalid option
    else:
        # Print error
        print( '[ - ] Invalid option!' )


if __name__ == '__main__':
    main( )