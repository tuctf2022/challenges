import time
import os

# Fletcher-16 checksum
def fletcher16( data ):
    sum1 = 0
    sum2 = 0
    for c in data:
        sum1 = ( sum1 + ord( c ) ) % 255
        sum2 = ( sum2 + sum1 ) % 255
    return ( sum2 << 8 ) | sum1


# Partition string
def data_partition( data ):
    # Split data every 2 characters
    data = [ data[i:i+2] for i in range( 0, len( data ), 2 ) ]
    if len( data[-1] ) < 2:
        # Pad if needed
        data[-1] += 'X' * ( 2 - len( data[-1] ) )
    return data

def hash( data ):
    # Partition data
    data = data_partition( data )

    # Hash each partition
    hash = ""
    for d in data:
        # fletcher hash
        hash += "%04x" % fletcher16( d )

    return hash

print(hash("TUCTF{5m4ll_5um_h45h1n6_f7w_475928}"))