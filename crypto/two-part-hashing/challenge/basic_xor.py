import os

# 4-byte xor table, this is random
xor_table = [0x48, 0x30, 0xAB, 0x24]

# xor file
def xor_file( filename, out ):
    # Open file
    f = open( filename, "rb" )
    data = f.read()
    f.close()

    # New file data
    new_data = []

    # xor binary data
    for i in range( len( data ) ):
        new_data.append( data[i] ^ xor_table[i % 4] )

    # Convert to bytes
    new_data = bytes( new_data )
    # Write file
    f = open( out, "wb" )
    f.write( new_data )
    f.close()

# Xor john.pdf
xor_file( "crypto.pdf", "crypto.enc.pdf" )