import string
# GS-8 coded Braille text: ⠱⠁ ⠹⠣ ⠱⠹ ⠱⠁ ⠹⠣ ⠹⠳ ⠱⠁ ⠹⠣ ⠹⠪ ⠱⠁ ⠹⠣ ⠱⠩ ⠹⠣ ⠱⠩ ⠹⠱ ⠻⠁ ⠫⠩ ⠫⠡ ⠻⠻ ⠹⠣ ⠱⠩ ⠹⠱ ⠻⠁ ⠻⠪ ⠻⠁ ⠻⠱ ⠫⠡ ⠻⠻ ⠫⠡ ⠫⠩
# GS-8 to hex numbers: 5a 42 54 5a 42 48 5a 42 49 5a 42 53 42 53 45 7a 63 61 77 42 53 45 7a 79 7a 75 61 77 61 63
# Must convert hex numbers to ordinary letters.ZBTZBHZBIZBSBSEzcawBSEzyzuawac
upperFlag = string.ascii_uppercase[:26]
lowerFlag = string.ascii_lowercase[:26]
MIN_LETTER = ord("a")
MIN_CAPLETTER = ord("A")

def unmix(oneLetter,num):
    
    if(oneLetter.isupper()):
        word = ord(oneLetter)-MIN_CAPLETTER
        shift = ord(num)-MIN_CAPLETTER
        return upperFlag[(word - shift)%len(upperFlag)]
    if(oneLetter.islower()):
        word = ord(oneLetter) + MIN_LETTER
        shift = ord(num) + MIN_LETTER
        return lowerFlag[(word - shift)%len(upperFlag)]

def depuzzle(parts):

    for letter in parts:
        if(parts == "CTF"):
            char = "_"

            return char
        elif (letter.isupper()):
            upperOne = f"{upperFlag.index(parts[:1]):05b}"
            upperTwo = f"{upperFlag.index(parts[1:2]):05b}"
            upperThree = f"{upperFlag.index(parts[2:]):05b}"
            binary = upperOne + upperTwo + upperThree
            flagLetter = chr(int(binary,2))

            return flagLetter
        elif(letter.islower()):
            lowerOne = f"{lowerFlag.index(parts[:1]):01x}"
            lowerTwo = f"{lowerFlag.index(parts[1:]):01x}"
            hex = lowerOne + lowerTwo
            flagLetter = chr(int(hex,16))
            return flagLetter


hiddenMessage = "ZBTZBHZBIZBSBSEzcawBSEzyzuawac"
for x in lowerFlag:
    shiftBack = ""
    shiftAt = x
    flag = ""

    for num, letter in enumerate(hiddenMessage):    
        shiftBack += unmix(letter, shiftAt)
        
    num = 0
    for i in range(len(shiftBack)):
        
        if(num>=len(shiftBack)):
            break
        if(shiftBack[num].isupper()):
                three = (shiftBack[num]+shiftBack[num+1]+shiftBack[num+2])
                flag += depuzzle(three)
                num +=3
              
        elif(shiftBack[num].islower()):
                two = (shiftBack[num] + shiftBack[num+1])
                flag += depuzzle(two)
                num += 2   
    print("TUCTF{"+flag+"}")




    





