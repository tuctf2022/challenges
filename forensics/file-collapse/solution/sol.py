from Crypto.Cipher import AES

# Identifiers for KEY: C1 00 00 00 00 00 00 00
# Identifiers for IV: 71 00 00 00 00 00 00 00

key = b'\x30\x26\xCA\xB7\x05\x3A\x7B\xB6\x32\xA7\x9A\x4E\x38\xA1\x4C\x36'
iv = b'\x35\xF3\x81\xBB\x68\xAD\x58\xC5\x73\x36\x99\x60\x64\xE5\x57\x32'
def decrypt_file( file:str ):
    with open( file, 'rb' ) as f:
        # Read file
        data = f.read()
        # Create AES object
        aes = AES.new( key, AES.MODE_CBC, iv )
        # Encrypt data
        decrypted = aes.decrypt( data )
        # Write encrypted data to file
        print(decrypted)

decrypt_file('flag.txt')