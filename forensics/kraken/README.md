# Kraken - Forensics

## Difficulty: Medium

## Description

Davy Jones has been arrested for pirating the 7 seas! We have his computer but we need you to find the password for the zip file in his confidential folder!

- Requires `kraken.zip`

## Hints

(They dont know where to start)

- Extract the SAM and system registry hives! What do these hold?

(They dont know what to do with the hashes)

- The SAM hive holds two hashes for the same password for each user, what does this mean?

(They cracked the LM hash, but the password doesnt work for the zip file)

- What are the limitations of LM hashing? Try checking the password against the NT hash.
