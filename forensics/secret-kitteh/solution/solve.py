import subprocess

# Dictionary file to use for the bruteforce
dictionary_file = "rockyou.txt"

# Path to the 7zip executable
sevenzip_executable = "7z.exe"

archive_file = "extracted_kitteh.7z"

with open(dictionary_file, "r") as f:
    lines = f.readlines()

# Iterate over each line (password) in the dictionary
for line in lines:
    password = line.strip()
    command = [
        sevenzip_executable,
        "x",
        "-p{}".format(password),
        archive_file,
        "-aoa"
    ]
    output = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Message from 7zip if the password is correct
    if "Everything is Ok" in output.stdout.decode("utf-8"):
        print("The password is: {}".format(password))
        break