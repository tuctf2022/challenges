# Solution
The kitteh has a zip file appended to the end of it D:
Extracting the zip file using `binwalk -e secret_kitteh.jpg` gives us a password protected zip.

Writing a script to bruteforce the password, we get the flag.
The password is `password`

```python