# file and 7zip stuff
import os
import py7zr
import shutil

image_file = "secret_kitteh.jpg"
# Open the image file in current script directory
script_dir = os.path.dirname(__file__)
flag_file = os.path.join(script_dir, "flag.txt")

# Create new 7zip file
with py7zr.SevenZipFile(os.path.join(script_dir, "secret_kitteh.7z"), "w", password="password") as archive:
    # Add the image file to the 7zip file
    archive.writeall(flag_file, "flag")

# Open the zip file as binary
zip_data = open(os.path.join(script_dir, "secret_kitteh.7z"), "rb").read()


image_path = os.path.join(script_dir, image_file)
image_data = open(image_path, "rb").read()
# Append the zip data to the image data
image_data += zip_data

# Write the image data to a new file
new_image_file = "secret_kitteh_.jpg"
new_image_path = os.path.join(script_dir, new_image_file)
open(new_image_path, "wb").write(image_data)