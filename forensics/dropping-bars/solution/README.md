### Solution Description

This challenge provided a .wav file of a remix of "Never Gonna Give You Up" by Rick Astley, and the lyrics to that song. The description mentioned finding the difference between the remix and the original.

Listening to the audio file results in a new sounding chorus at 0:43 and 1:15. To find the difference between the two, the user needed to somehow subtract the original audio from the remix. One such way is as follows:
- Download a WAV of the original audio from YouTube or elsewhere 
- Invert the original track
- Convert both the remix and the original to mono audio
- Combine the inverted original and the remix to one track, which will delete all sections that are the same in both tracks

Doing this results in two isolated areas of noise at 0:43 and 1:25. Viewing these sounds with a spectrogram viewer results in two QR codes.

Scanning the QR codes shows two strings: `L` followed by a series of numbers, and `C` followed by a series of numbers. Looking at the metadata for the remix shows the author as "Cy Ottendorf" - a reference to the Ottendorf cipher, or book cipher. This works by using a known text as a dictionary and referencing certain pages, lines, or characters from it. Given `L` and `C`, one can reasonably assume you need to look at the lines and characters of the lyrics, also uploaded to the challenge, to find the flag.