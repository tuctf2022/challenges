import PIL
from PIL import Image

flagPath = ""

flag = Image.open(flagPath)

colors = {
   (255, 127, 39, 255): 0,
   (34, 177, 76, 255): 1,
   (163, 73, 164, 255): 2,
   (237, 28, 36, 255): 3,
   (255, 201, 14, 255): 4,
   (63, 72, 204, 255): 5
}

## Depending on how you isolate the flag, you may have to use RGBA vs RGB
# colors = {
#    (255, 127, 39): 0,
#    (34, 177, 76): 1,
#    (163, 73, 164): 2,
#    (237, 28, 36): 3,
#    (255, 201, 14): 4,
#    (63, 72, 204): 5
# }

out = []


for x in range(flag.width):
   for y in range(flag.height):
      if flag.getpixel((x, y)) in colors:
         out.append(colors[flag.getpixel((x, y))])


longstr = "".join(map(str,out))

splitstr = [longstr[i:i+3] for i in range(0, len(longstr), 3)]

sol = [chr(int(x, 6)) for x in splitstr]

print("".join(sol))
