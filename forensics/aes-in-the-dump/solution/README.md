# Solution
The memory dump contains two parts of a flag, which are used as a key and IV for the AES CBC encryption of the PDF.
The PDF contains the flag.

## Key
TUCTF{.^_^n0t_th

## IV
3_r34l_fl4gxxx}