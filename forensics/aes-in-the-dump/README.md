# AES in the Dump
## Description
Someone encrypted my top secret file! I managed to get a copy of the memory dump, but I don't know how to decrypt it. Can you help me?

## Flag
TUCTF{4DM1N_P455W0RD_D0N7_L34K_384956}