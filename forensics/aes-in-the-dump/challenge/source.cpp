#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
#include <algorithm>

/* Include OpenSSL AES */
#include <openssl/aes.h>

static char iv[AES_BLOCK_SIZE];
static const unsigned char key[] = "TUCTF{.^_^n0t_th";

void encrypt(unsigned char* buf, size_t length, const AES_KEY* const enc_key, const unsigned char* iv)
{
	unsigned char local_vector[AES_BLOCK_SIZE];
	memcpy(local_vector, iv, AES_BLOCK_SIZE);

	AES_cbc_encrypt(buf, buf, length, enc_key, local_vector, AES_ENCRYPT);
}

void decrypt(unsigned char* buf, size_t length, const AES_KEY* const dec_key, const unsigned char* iv)
{
	unsigned char local_vector[AES_BLOCK_SIZE];
	memcpy(local_vector, iv, AES_BLOCK_SIZE);

	AES_cbc_encrypt(buf, buf, length, dec_key, local_vector, AES_DECRYPT);
}

void test()
{
	AES_KEY enc_key, dec_key;
	AES_set_encrypt_key(key, 128, &enc_key);
	AES_set_decrypt_key(key, 128, &dec_key);

	std::string str = "Hello World!";
	std::vector<unsigned char> buf(str.begin(), str.end());
	buf.resize(AES_BLOCK_SIZE * ((buf.size() / AES_BLOCK_SIZE) + 1), 0);

	encrypt(buf.data(), buf.size(), &enc_key, (const unsigned char*)iv);
	decrypt(buf.data(), buf.size(), &dec_key, (const unsigned char*)iv);

	std::cout << buf.data() << std::endl;
}

int main(int argc, char** argv)
{
	printf("Welcome to the RAT Admin panel!\n");
	
	strcpy(iv, "3_r34l_fl4g!");

	/* Randomize the iv */
	srand((385678));
	for (int i = 12; i < 15; i++)
		/* Random ascii number (1-9) */
		iv[i] = (rand() % 9) + 48;
		
		
	iv[15] = '}';
	
	
	/* Open './Super secret data.pdf' and read all binary data */
	FILE* f = fopen("./Super secret data.pdf", "rb");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return 0;
	}
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);  /* same as rewind(f); */
	
	MessageBoxA(NULL, "Press OK to continue", "RAT Admin Panel", MB_OK);

	unsigned char* string = (unsigned char*)malloc(fsize + 1);
	fread(string, fsize, 1, f);
	fclose(f);
	
	printf("Encrypting data using AES CBC!\n");
	AES_KEY enc_key;
	AES_set_encrypt_key(key, 128, &enc_key);
	
	std::vector<unsigned char> buf(string, string + fsize);
	buf.resize(AES_BLOCK_SIZE * ((buf.size() / AES_BLOCK_SIZE) + 1), 0);

	encrypt(buf.data(), buf.size(), &enc_key, (const unsigned char*)iv);
	
	/* Write encrypted data back to './Super secret data.pdf' and replace existing */
	f = fopen("./Super secret data.pdf", "wb");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return 0;
	}
	fwrite(buf.data(), buf.size(), 1, f);
	fclose(f);
	
	/* Rename file extension to '.ctf' */
	std::string filename = "./Super secret data.pdf";
	std::string new_filename = filename.substr(0, filename.find_last_of(".")) + ".ctf";
	rename(filename.c_str(), new_filename.c_str());

	return 0;
}