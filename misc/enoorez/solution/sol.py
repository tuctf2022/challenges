f = open('enoorez.dat', 'r')

data = f.readlines()

def wbin_to_bin(data):
    word = ''
    ret = ''
    i = 0
    for character in data:
        word += character
        if word == 'orez':
            ret += '0'
            word = '' 
        if word == 'eno':
            ret += '1'
            word = ''
    return ret  

def bin_to_ascii(msg):
    byts = [] 
    i = 0
    byt = ''
    for c in msg:
        if i == 8:
            byts.append(byt)
            byt = ''
            i = 0
        byt +=c
        i+=1
    byts.append(byt)
    s = ''
    for b in byts:
        s += chr(int(b, 2))
    return s

def parse_num(data:str):
    if data[0] == 'o':
        return '0',data[4:]
    else:
        return '1',data[3:]

def stm_decode(data:str):
    decoded = ''
    state = ''
    while(len(data) > 0):
        c, data = parse_num(data)
        decoded += c
    return decoded

print(stm_decode(bin_to_ascii(wbin_to_bin(data[0]))))