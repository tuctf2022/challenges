import random
import secret

def dec_to_bin(num):
    if num > 0:
        r = num % 2
        num = num // 2
        return f'{r}' + f'{dec_to_bin(num)}'
    return 0

def txt_to_wbin(msg):
    c = [dec_to_bin(ord(x))[::-1].rjust(8, '0') for x in msg]
    s = ''
    k = ''
    for i in c:
        s += i
    for j in s:
        if j == '1':
            k += 'eno'
        if j == '0':
            k += 'orez'
    return k


def obfuscate(msg):
    newmsg = ''
    bad = ['z', 'o', 'e']
    for i in range(0, len(msg)):
        c = chr(random.randint(1, 32))
        if msg[i] not in bad:
            newmsg += c
            continue
        newmsg += msg[i]
    return newmsg
open('enoorez.dat', 'w').write(txt_to_wbin(obfuscate(txt_to_wbin(secret.flag))))
