# Solution

Slow password is much like a blind SQL injection, or like picking a lock.
There is a noticable delay in the password check algorithm if you get a character right.
While the first portion of the password is a mix of upper and lowercase, as the password progresses it should be noticed that each section is either all numbers, all lowercase, or all uppercase and begins to spell out words.

## Password
`TfbGMJsEaNTY_468260_cbmabfu_LOL_UMAD?_IT_KEEPS_GOING`