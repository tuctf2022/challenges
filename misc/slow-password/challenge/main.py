#!/usr/bin/env python
import time

admin_password = open( "./password", "r" ).read( ).strip( )

flag = open( "./flag.txt", "r" ).read( ).strip( )

def check_password( password ):
    for i in range( len( password ) ):
        # Check if i is length of admin_password
        if i == len( admin_password ) - 1:
            return True
        if password[ i ] != admin_password[ i ]:
            return False
        time.sleep( 1 )
    return False

def main( ):
    print( "<:: Admin Panel Login ::>" )
    print( "------------------------" )
    password = input( "Password: " )
    if check_password( password ):
        print( "Welcome, admin! Have a flag:" )
        print( flag )
    else:
        print( "Incorrect password!" )

if __name__ == "__main__":
    main( )