#!/usr/bin/env python
import sys
import random


# Current directory string
current_dir = "/home/hacker"

directories = ['secret', 'data']

### Helper functions ###

def write_text_backwards( text ):
    for char in text[::-1]: 
        sys.stdout.write( chr( ord( char ) + cipher_shift ) )
        sys.stdout.flush( )
    # Print new line
    print( )
    # Shift current directory
    print_dir = "".join( [ chr( ord( char ) + cipher_shift ) for char in current_dir ] )
    # Invserse the current directory
    print_dir = print_dir[::-1]
    # Print current directory
    print( print_dir, end = "" )

def read_input( ):
    # get user input
    user_input = input( "> " )
    # Inverse the input
    user_input = user_input[::-1]
    # Shift the input
    user_input = "".join( [ chr( ord( char ) - cipher_shift ) for char in user_input ] )
    print( user_input)
    # return the input
    return user_input

def generate_cipher_shift( ):
    # Generate a random cipher in range of 1-18
    global cipher_shift
    #cipher_shift = 0
    cipher_shift = random.randint( 8, 16 )

### Shell methods ###
def do_help( ):
    write_text_backwards( " Available commands:" )
    write_text_backwards( " help - show this help" )
    write_text_backwards( " exit - exit the shell" )
    write_text_backwards( " cd - change directory" )
    write_text_backwards( " ls - list files in directory" )
    write_text_backwards( " cat - print file contents" )

def do_exit( ):
    write_text_backwards( "Exiting..." )
    sys.exit( 0 )

def do_cd( args ):
    # check if we have arguments
    if len( args ) == 0:
        write_text_backwards( "Usage: cd <directory>" )
        return
    # check if we have a valid directory
    if args[0] not in directories:
        write_text_backwards( "Invalid directory" )
        return
    # change directory
    global current_dir
    current_dir = args[0]
    write_text_backwards( "Changed directory to " + current_dir )

def do_ls( args ):
    # Check if current directory is default
    if current_dir == "/home/hacker":
        write_text_backwards( "\nsecret, data\n" )
        return
    # check if we have a valid directory
    if current_dir not in directories:
        write_text_backwards( "Invalid directory" )
        return
    # If we are in the secret directory
    if current_dir == "secret":
        # If args contains -a or --all
        if "-a" in args or "--all" or "-la" in args:
            write_text_backwards( "\n. .. .flag.txt" )
        write_text_backwards( "\nREADME.txt" )
    # If we are in the data directory
    elif current_dir == "data":
        if "-a" in args or "--all" in args:
            write_text_backwards( "." )
            write_text_backwards( ".." )
        write_text_backwards( "" )

def do_cat( args ):
    # Check if we have arguments
    if len( args ) == 0:
        write_text_backwards( "Usage: cat <file>" )
        return
    # Check if we have a valid file
    if args[0] not in [ "README.txt", ".flag.txt" ]:
        write_text_backwards( "Invalid file" )
        return
    # Check if we are in the secret directory
    if current_dir == "secret":
        # If we have a valid file
        if args[0] == "README.txt":
            write_text_backwards( "Bazinga" )
        elif args[0] == ".flag.txt":
            write_text_backwards( "TUCTF{my_5up3r_dup3r_53cr37_1337_5h3ll}" )
    # Print file not found
    else:
        write_text_backwards( "File not found" )

def main( ):
    # Generate a random cipher shift
    generate_cipher_shift( )
    # Print welcome message with cipher shift'
    print( "Welcome to my shell, version " + str( cipher_shift ) )
    write_text_backwards( "Type help to see available commands" )
    # Main loop
    while True:
        # Read user input
        user_input = read_input( )
        # Split the input
        user_input = user_input.split( " " )
        # Get command
        command = user_input[0]
        # Get arguments
        args = user_input[1:]
        # Execute command
        if command == "help":
            do_help( )
        elif command == "exit":
            do_exit( )
        elif command == "cd":
            do_cd( args )
        elif command == "ls":
            do_ls( args )
        elif command == "cat":
            do_cat( args )
        else:
            write_text_backwards( "Invalid command" )

if __name__ == "__main__":
    main( )